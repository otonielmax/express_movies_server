# Pull base image.
FROM node:10-alpine
ARG NPM_TOKEN

# mkdir no es recursivo por defecto (poner -p)  
RUN \
  mkdir -p /var/webserver

# Copy files
WORKDIR /var/webserver
COPY . .

# Install app
RUN \
  cd /var/webserver && \
  npm install

EXPOSE 80
ENTRYPOINT ["node", "./src/app"]