module.exports = {
    name: 'REPELIS.BIZ',
    host: 'http://localhost:5000',
    type: 'movies',
    domains: ['localhost'],
    fbPixel: {
        default: '123123',
        140: 'hola',
    },
    // analytics: 'UA-130039796-1',
    // adsense: 'xx',
    // facebook: 'Cliver.TV.Cliver',
    // twitter: 'Repelis_',
    // dmca_id: '589a4b3f-dc63-4b35-9bc3-562af2d58089',
    language: {
        // Idiomas disponibles
        available: [
            {
                // en-US
                language_id: 102,
                language_locale_id: 126,
                show_locale: true,
                default: true,
            },
            // {
            //     // es-ES
            //     language_id: 131,
            //     language_locale_id: 140,
            //     show_locale: true,
            //     default: true,
            // }
            // {
            //     // es-MX
            //     language_id: 131,
            //     language_locale_id: 144,
            //     show_locale: true,
            // },
            // {
            //     // fr-FR
            //     language_id: 170,
            //     language_locale_id: 184,
            //     default: true
            // },
            // {
            //     // it-IT
            //     language_id: 250,
            //     language_locale_id: 252,
            // },
            // {
            //     // pt-PT
            //     language_id: 387,
            //     language_locale_id: 391,
            //     default: true
            // }
        ],
        // Sistema de lenguajes activo
        multiLanguage: true,
    },
    cloudflare: {
        email: '',
        apiKey: '',
        zoneId: '',
        dnsRecords: [{
            name: '',
            type: 'CNAME',
            content: '',
            proxied: true
        }]
    },
    playerConfig: {
        waitText1: 'Streaming Gratuito en HD',
        waitText2: 'Por favor espere:',
        waitTime: {
            ad: 2,
            youtube: 2
        },
        adLink: [
            'http://trakingtraffic.com/tracking202/redirect/dl.php?t202id=3504',
            'https://www.google.com?mt={{title}}'
        ],
        adLinkTargetBlank: true,
        adCountries: ['AR']
    },
    featured: {
        genre: 'animation',
    },
    routes: [{
        name: 'ads',
        controller: 'ads',
        no_language_prefix: true,
        values: {
            default: '/ads.txt',
        }
    }, {
        name: 'dmca',
        controller: 'dmca',
        no_language_prefix: true,
        values: {
            default: '/dmca.html',
        }
    }, {
        name: 'tos',
        controller: 'tos',
        no_language_prefix: true,
        values: {
            default: '/tos.html',
        }
    }, {
        name: 'genre',
        controller: 'genre',
        values: {
            default: '/categoria/:hash/',
        },
        paginationValues: {
            default: '/categoria/:hash/pagina/:page/',
        },
        titles: {
            140: 'Ver Peliculas Online de {{genre.name}} Gratis - REPELIS',
            default: 'Ver Peliculas Completas de {{genre.name}} Online Gratis - REPELIS',
        },
        descriptions: {
            140: 'Ver películas online gratis de {{genre.name}} con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver películas gratis.',
            default: 'Películas completas de {{genre.name}} con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de películas completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'home',
        controller: 'home',
        values: {
            default: '/',
        },
        paginationValues: {
            default: '/pagina/:page/',
        },
        titles: {
            default: 'REPELIS | Peliculas Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver películas online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'Películas completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de películas completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'home_alt',
        controller: 'home',
        pathParameters: {
            alternative_site: [
                'cinecalidad', 'cliver-tv',
                'g-nula', 'inkapelis',
                'miradetodo', 'netflix',
                'nexflix', 'pelis24',
                'pelispedia', 'pelisplus',
                'repelis', 'repelisplus',
                'somosmovies', 'tvpelis',
                'yaske'
            ]
        },
        values: {
            default: '/s/:alternative_site',
        },
        titles: {
            default: '{{alternative_site}} | Peliculas Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            default: '{{alternative_site}} - Películas completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de películas completas, estrenos y trailers de cine.',
        },
        // SERIES
    }, {
        name: 'serie',
        controller: 'serie',
        amp: true,
        values: {
            140: '/series/castellano/ver-online/:hash/',
            default: '/series/latino/serie-completa/:hash/',
        },
        versionValues: {
            140: '/series/castellano/ver-online/:hash/v:version/',
            default: '/series/latino/serie-completa/:hash/v:version/'
        },
        titles: {
            140: 'Ver Online {{serie.title}} Castellano - REPELIS',
            default: 'Ver Serie {{serie.title}} Español latino Online Gratis - REPELIS',
        },
        descriptions: {
            default: 'Ver serie {{serie.title}} - {{serie.overview}}',
        }
    }, 
    {
        name: 'season',
        controller: 'season',
        amp: true,
        values: {
            140: '/series/castellano/ver-online/:hash/s:season_number/',
            default: '/series/latino/serie-completa/:hash/s:season_number/',
        },
        versionValues: {
            140: '/series/castellano/ver-online/:hash/s:season_number/v:version/',
            default: '/series/latino/serie-completa/:hash/s:season_number/v:version/',
        },
        titles: {
            140: 'Ver Online {{serie.title}} temporada {{season_number}} Castellano - REPELIS',
            default: 'Ver Serie {{serie.title}} temporada {{season_number}} Español latino Online Gratis - REPELIS',
        },
        descriptions: {
            default: 'Ver serie {{serie.title}} temporada {{season_number}} - {{serie.overview}}',
        }
    },
    {
        name: 'episode',
        controller: 'episode',
        amp: true,
        values: {
            140: '/series/castellano/ver-online/:hash/s:season_number/e:episode_number/',
            default: '/series/latino/serie-completa/:hash/s:season_number/e:episode_number/',
        },
        versionValues: {
            140: '/series/castellano/ver-online/:hash/s:season_number/e:episode_number/v:version/',
            default: '/series/latino/serie-completa/:hash/s:season_number/e:episode_number/v:version/',
        },
        titles: {
            140: 'Ver Online {{serie.title}} temporada {{season_number}} episodio {{episode_number}} Castellano - REPELIS',
            default: 'Ver Serie {{serie.title}} {{season_number}} episodio {{episode_number}} Español latino Online Gratis - REPELIS',
        },
        descriptions: {
            default: 'Ver serie {{serie.title}} {{season_number}} episodio {{episode_number}} - {{serie.overview}}',
        }
    }, {
        name: 'series_popular',
        controller: 'series-popular',
        values: {
            default: '/series/popular',
        },
        paginationValues: {
            default: '/series/popular/pagina/:page/',
        },
        titles: {
            default: 'REPELIS | Series Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver series online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'series completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de series completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'series_top_rated',
        controller: 'series-top-rated',
        values: {
            default: '/series/top-rated',
        },
        paginationValues: {
            default: '/series/top-rated/pagina/:page/',
        },
        titles: {
            default: 'REPELIS | Series Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver series online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'series completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de series completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'series_on_air',
        controller: 'series-on-air',
        values: {
            default: '/series/on-air',
        },
        paginationValues: {
            default: '/series/on-air/pagina/:page/',
        },
        titles: {
            default: 'REPELIS | Series Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver series online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'series completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de series completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'series_airing_today',
        controller: 'series-airing-today',
        values: {
            default: '/series/airing-today',
        },
        paginationValues: {
            default: '/series/airing-today/pagina/:page/',
        },
        titles: {
            default: 'REPELIS | Series Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver series online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'series completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de series completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'movies_upcoming',
        controller: 'movies-upcoming',
        values: {
            default: '/movies/upcoming/',
        },
        paginationValues: {
            default: '/movies/upcoming/pagina/:page/',
        },
        titles: {
            default: 'ESTRENOS - REPELIS | Peliculas Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver películas online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'Películas completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de películas completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'movies_now_playing',
        controller: 'movies-now-playing',
        values: {
            default: '/movies/now-playing/',
        },
        paginationValues: {
            default: '/movies/now-playing/pagina/:page/',
        },
        titles: {
            default: 'CARTELERA - REPELIS | Peliculas Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver películas online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'Películas completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de películas completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'movies_top_rated',
        controller: 'movies-top-rated',
        values: {
            default: '/movies/top-rated/',
        },
        paginationValues: {
            default: '/movies/top-rated/:page/',
        },
        titles: {
            default: 'REPELIS | Series Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver series online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'series completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de series completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'movie',
        controller: 'movie',
        amp: true,
        values: {
            140: '/castellano/ver-online/:release_year/:hash/',
            default: '/latino/pelicula-completa/:release_year/:hash/',
        },
        titles: {
            140: 'Ver Online {{movie.title}} ({{movie.release_year}}) Castellano - REPELIS',
            default: 'Ver Película {{movie.title}} ({{movie.release_year}}) Español latino Online Gratis - REPELIS',
        },
        descriptions: {
            default: 'Ver película {{movie.title}} ({{movie.release_year}}) - {{movie.overview}}',
        }
    }, {
        name: 'search',
        controller: 'search',
        values: {
            default: '/busqueda/',
        },
        paginationValues: {
            default: '/busqueda/pagina/:page/',
        },
        titles: {
            default: '{{search_query}} REPELIS | Peliculas Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            140: 'Ver películas online gratis con audio latino, español, subtitulado y sin cortes. Los útlimos estrenos de cine calidad HD solo en RePelis el mejor sitio para ver pelculas gratis.',
            default: 'Películas completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de películas completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'person',
        controller: 'person',
        values: {
            default: '/persona/:hash/',
        },
        paginationValues: {
            default: '/persona/:hash/pagina/:page/',
        },
        titles: {
            default: 'REPELIS | Peliculas Completas Gratis en Castellano y Latino',
        },
        descriptions: {
            default: 'Películas completas con audio latino, español, subtituladas, sin cortes, calidad HD en Repelis, el mejor sitio de películas completas, estrenos y trailers de cine.',
        },
    }, {
        name: 'sitemap',
        controller: 'sitemap',
        no_language_prefix: true,
        values: {
            default: '/sitemap.xml',
        },
        paginationValues: {
            default: '/sitemap_:page.xml',
        },
    }, {
        name: 'robots',
        controller: 'robots',
        no_language_prefix: true,
        values: {
            default: '/robots.txt',
        }
    }, {
        name: 'rss',
        controller: 'rss',
        no_language_prefix: true,
        values: {
            default: '/rss/',
        },
    }, {
        name: 'rss2',
        controller: 'rss',
        no_language_prefix: true,
        values: {
            default: '/rss/:type/',
        },
    },
    {
        name: 'login',
        controller: 'login',
        method: 'POST',
        no_language_prefix: true,
        values: {
            default: '/ajax/login/',
        },
    }, {
        name: 'signup',
        controller: 'signup',
        method: 'POST',
        no_language_prefix: true,
        values: {
            default: '/ajax/signup/',
        },
    },
    // PROXY DE IMAGENES
    {
        name: 'image_proxy',
        controller: 'tmdb-image',
        no_language_prefix: true,
        values: {
            default: '/movies/static/img/:size/:path',
        },
    }],
    headerLinks: {
        default: [{
            display_name: 'Inicio',
            link: '/'
        }, {
            display_name: 'Google',
            link: 'https://www.google.com',
        }]
    },
    footerLinks: {
        default: [{
            display_name: 'Acción',
            link: '/categoria/accion/'
        }, {
            display_name: 'Terror',
            link: '/categoria/terror/'
        }, {
            display_name: 'DMCA',
            link: '/dmca.html'
        }, {
            display_name: 'ADS',
            link: '/ads.txt'
        }, {
            display_name: 'TOS',
            link: '/tos.html'
        }]
    },
    sidebarLinks: {
        default: [{
            display_name: 'Link1',
            link: '/'
        }, {
            display_name: 'Link2',
            link: '/'
        }]
    },
    id: 1,
    paths: {
        website: '../../movies-websites/src/clivertv.org',
        static: '../../movies-websites/src/clivertv.org/static',
        partials: '../../movies-websites/src/clivertv.org/templates',
        translations: '../../movies-websites/src/clivertv.org/translations',
    },
    home: {
        limit: {
            featured: 32,
            page: 20,
        }
    },
    genre: {
        limit: {
            featured: 32,
            page: 20,
        },
    },
    search: {
        limit: {
            page: 20
        },
    },
    keywords: []
};