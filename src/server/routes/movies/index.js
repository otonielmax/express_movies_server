'use strict';

module.exports = {
    'episode': require('./episode'),
    'genre': require('./genre'),
    'home': require('./home'),
    'movie': require('./movie'),
    'movies-now-playing': require('./movies-now-playing'),
    'movies-top-rated': require('./movies-top-rated'),
    'movies-upcoming': require('./movies-upcoming'),
    'person': require('./person'),
    'rss': require('./rss'),
    'search': require('./search'),
    'season': require('./season'),
    'serie': require('./serie'),
    'series-airing-today': require('./series-airing-today'),
    'series-on-air': require('./series-on-air'),
    'series-popular': require('./series-popular'),
    'series-top-rated': require('./series-top-rated'),
    'tmdb-image': require('./tmdb-image'),
};