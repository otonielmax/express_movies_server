'use strict';
const async = require('async');
const moment = require('moment');
const RSS = require('rss');
const squel = require('squel');

const { logger } = require('../../../../logger');
const dbService = require('../../../../services/db');
const linkHelper = require('../../../../helpers/link');
const languageHelper = require('../../../../helpers/language');
const cacheService = require('../../../../services/cache');
const utils = require('../../../../helpers/utils');
const getMovieList = require('../../../managers/movies/movie/getMovieList');

module.exports = (req, res, next) => {
    const { transactionId } = req;
    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const website = req.website;
    const websiteId = website.id;
    // Multi idioma
    const multiLanguage = req.query.multi_lang && req.query.multi_lang == 0 ? false : true;
    // Limite de resultados
    let limit = 15;
    // Tipo de RSS
    const rssType = req.params.type || 'last';
    if (rssType !== 'last' &&
        rssType !== 'top' && 
        rssType !== 'popular' &&
        rssType !== 'now_playing' &&
        rssType !== 'upcoming') {
        return next();
    }
    // Formato de RSS
    const rssFormat = req.query.format || 'xml';
    if (rssFormat !== 'json' && rssFormat !== 'xml') {
        return next();
    }

    if (req.query.limit && !isNaN(req.query.limit) && req.query.limit <= 40) {
        limit = parseInt(req.query.limit);
    }

    const cacheOptions = {
        localExpireTime: 0,
        storageExpireTime: 60 * 60,
        stringify: true,
    };

    const key = `${websiteId}_RSS_${rssType}_${rssFormat}_${multiLanguage}_${limit}`;
    const composedFn = utils.compose(getRss, website, defaultLanguage, rssType, rssFormat, multiLanguage, limit);
    cacheService.getMaster(key, cacheOptions, composedFn, (err, rss) => {
        if (err) {
            logger.error('[routes/rss] Error generating RSS', {
                transactionId,
                rssType,
                multiLanguage,
                err
            });
            return res.sendError(err);
        }

        if (rssFormat === 'xml') {
            res.type('application/xml');
        }

        return res.send(rss);
    });
};

const getRss = (website, defaultLanguage, rssType, rssFormat, multiLanguage, limit, callback) => {
    async.waterfall([
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        (dbConnection, cb) => {
            let galleryType = 'popular';
            const query = squel.select()
                .field('id')
                .from('movie')
                .limit(limit);

            if (rssType === 'last') {
                const date = moment().subtract(4, 'days').format();

                query
                    .where('created_at >= ?', date)
                    .where('gallery_popular_position IS NOT NULL')
                    .order('created_at', 'DESC');
            } else if (rssType === 'popular') {
                query
                    .where('gallery_popular_position IS NOT NULL')
                    .order('gallery_popular_position', 'ASC');
            } else if (rssType === 'top') {
                galleryType = 'top_rated';

                query
                    .where('gallery_top_rated_position IS NOT NULL')
                    .order('gallery_top_rated_position', 'ASC');
            } else if (rssType === 'now_playing') {
                galleryType = 'now_playing';

                query
                    .where('gallery_now_playing_position IS NOT NULL')
                    .order('gallery_now_playing_position', 'ASC');
            } else if (rssType === 'upcoming') {
                galleryType = 'upcoming';

                query
                    .where('gallery_upcoming_position IS NOT NULL')
                    .order('gallery_upcoming_position', 'ASC');
            }

            const parsedQuery = query.toParam();

            dbConnection.query(parsedQuery.text, parsedQuery.values, (err, movies) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                const movieIds = movies.map((m) => m.id);

                return cb(null, dbConnection, movieIds, galleryType);
            });
        },
        (dbConnection, movieIds, galleryType, cb) => {
            if (!movieIds.length) {
                return cb(null, dbConnection, []);
            }

            const movieLangs = [];
            const availableLangs = multiLanguage ? website.language.available : [defaultLanguage];
            async.mapSeries(availableLangs, (language, cb) => {
                const currentLang = {
                    ...language,
                    movies: [],
                };
                const params = {
                    movieIds,
                    galleryType
                };
    
                getMovieList(dbConnection, website, language, defaultLanguage, params, (err, movies) => {
                    if (err) {
                        return cb(err);
                    }

                    currentLang.movies = movies;

                    movieLangs.push(currentLang);

                    return cb();
                });
            }, (err) => {
                return cb(err, dbConnection, movieLangs);
            });

        },
        (dbConnection, movieLangs, cb) => {
            const localeId = defaultLanguage.language_locale_id;
            const linkPrefix = linkHelper.getLanguagePrefix(website, defaultLanguage);
            const homeRoute = website.routes.find((r) => r.name === 'home');
            const homeLink = website.host + linkPrefix + (homeRoute.values[localeId] || homeRoute.values.default);

            const items = [];

            // Esto se una chapuza que se usa para Zapier. @see WEB-28
            const wordpressCategories = {
                170: 155, // FR
                131: 156, // ES
                102: 157, // EN
                387: 204, // PT
                250: 205 // IT
            };

            const movieRoute = website.routes.find((r) => r.name === 'movie');

            for (const language of movieLangs) {
                const movieLink = website.host + linkPrefix + (movieRoute.values[language.language_locale_id] || movieRoute.values.default);
                const movieDescription = movieRoute.descriptions[language.language_locale_id] || movieRoute.descriptions.default;
                const maxPopularity = Math.max(...language.movies.map((m) => m.popularity)) || 1500;
                for (const movie of language.movies) {
                    let link = movieLink.replace(':hash', movie.hash).replace(':release_year', movie.release_year);

                    let description = movieDescription.replace('{{movie.title}}', movie.title);
                    description = description.replace('{{movie.release_year}}', movie.release_year);
                    description = description.replace('{{movie.overview}}', movie.overview);
                    // Acortar descripcion
                    if (description.length > 140) {
                        description = description.substr(0, 137) + '...';
                    }

                    items.push({
                        title: movie.title,
                        url: link,
                        description: description,
                        content: movie.overview,
                        websiteId: website.id,
                        languageId: language.language_id,
                        languageName: languageHelper.getLanguage(language.language_id).name,
                        wordpressCategoryId: wordpressCategories[language.language_id] || wordpressCategories[102],
                        movieId: movie.id,
                        popularity: movie.popularity,
                        parsedPopularity: Number((movie.popularity / maxPopularity).toFixed(1)), // Poner un numero de popularidad de 0 a 1
                        date: moment(movie.created_at).toDate(),
                        posterPath: movie.poster_path ? 'https://image.tmdb.org/t/p/original' + movie.poster_path : 'https://puu.sh/CnVFX/df8abfe5b6.png'
                    });
                }
            }

            if (rssFormat === 'xml') {
                const feed = new RSS({
                    title: homeRoute.titles[localeId] || homeRoute.titles.default,
                    description: homeRoute.descriptions[localeId] || homeRoute.descriptions.default,
                    site_url: homeLink,
                });
                for (const item of items) {
                    feed.item({
                        title: item.title,
                        url: item.url,
                        description: item.description,
                        custom_elements: [{
                            'content:encoded': `<![CDATA[ ${item.content} ]]>`,
                        }, {
                            websiteId: item.websiteId,
                        }, {
                            languageId: item.languageId,
                        }, {
                            languageName: item.languageName,
                        }, {
                            wordpressCategoryId: item.wordpressCategoryId,
                        }, {
                            movieId: item.movieId,
                        }, {
                            popularity: item.popularity,
                        }, {
                            parsedPopularity: item.parsedPopularity
                        }],
                        date: item.date,
                        enclosure: {
                            url: item.posterPath
                        }
                    });
                }
                const rss = feed.xml({
                    indent: true
                });
                return cb(null, dbConnection, rss);
            }

            return cb(null, dbConnection, items);
        },
    ], (err, dbConnection, rss) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, rss);
    });
};