'use strict';
const async = require('async');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const dbService = require('../../../../services/db');
const languageHelper = require('../../../../helpers/language');
const hashHelper = require('../../../../helpers/hash');
const adsHelper = require('../../../../helpers/ads');
const utils = require('../../../../helpers/utils');
const getPersonLink = require('../../../managers/movies/person/getLink');
const getFeaturedSeries = require('../../../managers/series/serie/getFeatured');
const getAllGenres = require('../../../managers/series/genre/getAll');
const getSerie = require('../../../managers/series/serie/get');
const getSeason = require('../../../managers/series/season/get');
const getEpisodeLink = require('../../../managers/series/episode/getLink');
const shouldRedirect = require('../../../managers/series/version/shouldRedirect');

/**
 * Se utiliza para manejar la ruta de visualizar detalles de un episodio
 */
module.exports = (req, res, next) => {
    const { transactionId } = req;

    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;

    // Informacion del website
    const website = req.website;

    // Informacion de la serie
    const seasonNumber = Number(req.params.season_number || 1);
    const episodeNumber = Number(req.params.episode_number || 1);

    const cacheOptions = {
        localExpireTime: 30,
        storageExpireTime: 60 * 60,
        stringify: true,
    };

    async.waterfall([
        // Obtener la serie
        (cb) => {
            getSerie(website, currentLanguage, defaultLanguage, req.params.hash, (err, serie) => {
                if (err) {
                    if (!err.redirect) {
                        logger.error(`[routes/episode] Error obtaining current serie (${req.params.hash})`, {
                            transactionId,
                            err
                        });
                    }

                    return cb(err);
                }

                res.data.context.serie = serie;

                return cb(null, serie);
            });
        },
        // Obtener la season
        (serie, cb) => {
            getSeason(website, currentLanguage, defaultLanguage, serie, seasonNumber, (err, season) => {
                if (err) {
                    logger.error(`[routes/episode] Error obtaining current season (${req.params.hash} - season ${seasonNumber})`, {
                        transactionId,
                        err
                    });

                    return cb(err);
                }

                res.data.context.season = season;

                return cb(null, serie, season);
            });
        },
        // Obtener el episodio
        (serie, season, cb) => {
            const key = `${website.id}_${currentLanguage.language_locale_id}_SERIE_${req.params.hash}_S_${seasonNumber}_E_${episodeNumber}`.toUpperCase();
            const composedFn = utils.compose(getEpisode, website, currentLanguage, defaultLanguage, serie, season, episodeNumber);
            
            cacheService.getMaster(key, cacheOptions, composedFn, (err, episode) => {
                if (err) {
                    if (!err.redirect) {
                        logger.error(`[routes/episode] Error obtaining current episode (${episodeNumber})`, {
                            transactionId,
                            err
                        });
                    }

                    return cb(err);
                }

                // Control de version
                if (shouldRedirect(episode.version, req.params.version)) {
                    const newLink = website.host + getEpisodeLink(website, currentLanguage, {
                        ...serie,
                        ...season,
                        ...episode
                    });

                    return cb({
                        redirect: true,
                        redirect_to: newLink,
                    });
                }

                res.data.context.episode = episode;

                // Langs alternativos para el meta
                if (episode.alternate_lang) {
                    res.data.context.site.alternate_lang = episode.alternate_lang;
                }

                return cb(null);
            });
        },
        // Obtener el listado de series destacadas
        (cb) => {
            getFeaturedSeries(website, currentLanguage, defaultLanguage, (err, series) => {
                if (err) {
                    logger.error('[routes/serie] Error obtaining featured series', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_series = series;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/serie] Error obtaining genre list', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        },
    ], (err) => {
        if (err) {
            if (err.redirect && err.redirect_to) {
                return res.redirect(301, err.redirect_to);
            } else {
                return res.sendError(err);
            }
        }

        res.data.route = 'episode';
        res.data.path = req.amp ? '/amp/series/episode' : '/series/episode';

        // Reemplazar valores del titulo
        res.data.context.site.title = res.data.context.site.title.replace(/{{serie.title}}/g, res.data.context.serie.title);
        res.data.context.site.title = res.data.context.site.title.replace(/{{season_number}}/g, res.data.context.season.season_number);
        res.data.context.site.title = res.data.context.site.title.replace(/{{episode_number}}/g, res.data.context.episode.episode_number);

        // Reemplazar valores del description
        res.data.context.site.description = res.data.context.site.description.replace(/{{serie.title}}/g, res.data.context.serie.title);
        res.data.context.site.description = res.data.context.site.description.replace(/{{season_number}}/g, res.data.context.season.season_number);
        res.data.context.site.description = res.data.context.site.description.replace(/{{episode_number}}/g, res.data.context.episode.episode_number);
        res.data.context.site.description = res.data.context.site.description.replace(/{{serie.overview}}/g, res.data.context.serie.overview);

        // Truncar longitud de descripcion
        if (res.data.context.site.description.length > 250) {
            res.data.context.site.description = res.data.context.site.description.substr(0, 250) + '...';
        }

        const playerConfig = Object.assign({}, website.playerConfig);
        
        // Definir si puede o no mostrar anuncios y eliminar el config de la respuesta
        playerConfig.showPlayer = adsHelper.showAds(playerConfig.adCountries, req.location.country_code, req.isCrawler);
        delete playerConfig.adCountries;

        // Reemplazar valores del titulo en los enlaces de anuncios
        if (playerConfig.adLink && playerConfig.adLink.length) {
            playerConfig.adLink = playerConfig.adLink.map((link) => {
                return link.replace(/{title}/g, encodeURIComponent(res.data.context.serie.title));
            });
        }

        // Asignar configuracion del player
        res.data.context.playerConfig = playerConfig;

        return next();
    });
};

const getEpisode = (website, currentLanguage, defaultLanguage, serie, season, episodeNumber, callback) => {
    async.waterfall([
        // Obtener conexion a la base de datos
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Obtener datos basicos del episodio
        (dbConnection, cb) => {
            const query = `
                SELECT
                    e.id,
                    IFNULL(sl_1.title, 
                        IFNULL(sl_2.title, 
                            IFNULL(sl_3.title,
                                e.title
                            )
                        )
                    ) title,
                    IFNULL(sl_1.overview,
                        IFNULL(sl_2.overview,
                            IFNULL(sl_3.overview,
                                e.overview
                            )
                        )
                    ) overview,
                    e.episode_number,
                    e.still_path,
                    e.air_date,
                    e.vote_count,
                    e.vote_average,
                    sv.version
                FROM
                    serie s
                JOIN
                    episode e ON e.serie_id = s.id
                LEFT JOIN
                    serie_language sl_1 ON (s.id=sl_1.serie_id AND sl_1.language_locale_id=? AND sl_1.episode_id = e.id)
                LEFT JOIN
                    serie_language sl_2 ON (s.id=sl_2.serie_id AND sl_2.language_id=? AND sl_1.episode_id = e.id)
                LEFT JOIN
                    serie_language sl_3 ON (s.id=sl_3.serie_id AND sl_3.language_id=? AND sl_1.episode_id = e.id)
                LEFT JOIN
                    serie_version sv ON (sv.episode_id=e.id AND sv.website_id=?)
                WHERE
                    e.serie_id = ? AND
                    e.season_id = ? AND
                    e.episode_number = ?
                GROUP BY
                    e.id
            `;

            const params = [
                currentLanguage.language_locale_id,
                currentLanguage.language_id,
                defaultLanguage.language_id,
                website.id,
                serie.id,
                season.id,
                episodeNumber
            ];

            dbConnection.query(query, params, (err, episode) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!episode[0]) {
                    return cb(true, dbConnection);
                }

                return cb(null, dbConnection, episode[0]);
            });
        },
        // Obtener listado de hashes del episode
        (dbConnection, episode, cb) => {
            const query = `
                SELECT
                    sh.hash,
                    l1.id language_id,
                    l2.id language_locale_id
                FROM
                    serie_hash sh
                JOIN
                    language l1 ON sh.language_id = l1.id
                LEFT JOIN
                    language l2 ON sh.language_locale_id = l2.id
                WHERE
                    sh.serie_id = ?
            `;

            dbConnection.query(query, [serie.id], (err, hashes) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                // Obtengo el hash mas optimo para el lenguaje del usuario
                let preferredHash = hashHelper.getBestHash(hashes, currentLanguage, defaultLanguage);

                if (website.language.multiLanguage) {
                    // Si el sitio es multilenguaje, el hash tiene que coincidir con el optimo
                    if (preferredHash.hash !== serie.hash) {
                        const newLink = website.host + getEpisodeLink(website, currentLanguage, {
                            ...serie,
                            ...season,
                            ...episode,
                            hash: preferredHash.hash,
                        });

                        return cb({
                            redirect: true,
                            redirect_to: newLink,
                        }, dbConnection);
                    }
                }

                let alternateLangs = [];

                for (let language of website.language.available) {
                    let bestHash = hashHelper.getBestHash(hashes, language, defaultLanguage);
                    const langId = language.show_locale ? language.language_locale_id : language.language_id;

                    const hash = {
                        default: language.default ? true : false,
                        hreflang: languageHelper.getLanguage(langId).iso,
                        href: website.host + getEpisodeLink(website, language, {
                            ...serie,
                            ...season,
                            ...episode,
                            hash: bestHash.hash,
                        }),
                        language_name: languageHelper.getLanguage(langId).name,
                    };

                    alternateLangs.push(hash);
                }

                // Ordeno primero el default
                alternateLangs = alternateLangs.sort((a, b) => {
                    return b.default - a.default;
                });

                episode.alternate_lang = alternateLangs;

                return cb(null, dbConnection, episode);
            });
        },
        // Obtener videos del episodio
        (dbConnection, episode, cb) => {
            const query = `
                SELECT
                    l.name language_name,
                    l.iso language_iso,
                    vm.name,
                    vm.site,
                    vm.key,
                    vm.size,
                    vm.type
                FROM
                    video_media vm
                JOIN
                    serie_video sv ON sv.video_media_id=vm.id
                JOIN
                    language l ON vm.language_id=l.id
                WHERE
                    sv.serie_id = ? AND
                    sv.season_id = ? AND
                    sv.episode_id = ?
            `;

            dbConnection.query(query, [serie.id, season.id, episodeNumber], (err, videos) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!videos.length) {
                    videos.push({
                        language_name: 'English',
                        iso: 'en',
                        name: 'Trailer',
                        site: 'YouTube',
                        key: 'uYSo9vMrHjw',
                        size: 1080,
                        type: 'Trailer',
                    });
                }

                episode.videos = videos;

                return cb(null, dbConnection, episode);
            });
        },
        // Obtener listado de actores del episodio
        (dbConnection, episode, cb) => {
            const query = `
                SELECT
                    p.id,
                    p.hash,
                    sp.\`character\`,
                    p.name,
                    p.profile_path,
                    p.gender_id,
                    sp.\`order\`,
                    sp.guest
                FROM
                    serie_performer sp
                JOIN
                    person p ON sp.person_id = p.id
                WHERE
                    sp.episode_id=?
                GROUP BY
                    p.id
                ORDER BY
                    sp.\`order\` ASC
            `;

            dbConnection.query(query, [episode.id], (err, results) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                for (const performer of results) {
                    performer.link = getPersonLink(website, currentLanguage, performer);
                }

                const performers = results.filter((p) => p.guest === 0);
                const guests = results.filter((p) => p.guest === 1);

                episode.performers = performers;
                episode.guests = guests;

                return cb(null, dbConnection, episode);
            });
        },
        // Obtener crew del episodio
        (dbConnection, episode, cb) => {
            const query = `
                SELECT
                    p.id,
                    p.name,
                    p.hash,
                    p.profile_path,
                    p.gender_id,
                    sc.job,
                    sc.department
                FROM
                    serie_crew sc
                JOIN
                    person p ON sc.person_id = p.id
                WHERE
                    sc.episode_id=?
                GROUP BY
                    p.id,
                    sc.department
            `;

            dbConnection.query(query, [episode.id], (err, crew) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                for (const c of crew) {
                    c.link = getPersonLink(website, currentLanguage, c);
                }

                episode.crew = crew.filter((c) => c.job !== 'Director');
                episode.directors = crew.filter((c) => c.job === 'Director');

                return cb(null, dbConnection, episode);
            });
        },
        // Obtener videos del episodio
        (dbConnection, episode, cb) => {
            const query = `
                SELECT
                    l.name language_name,
                    l.iso language_iso,
                    vm.name,
                    vm.site,
                    vm.key,
                    vm.size,
                    vm.type
                FROM
                    serie_video sv
                JOIN
                    video_media vm ON sv.video_media_id=vm.id
                JOIN
                    language l ON vm.language_id=l.id
                WHERE
                    sv.episode_id = ?
            `;

            dbConnection.query(query, [episode.id], (err, videos) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                let episodeVideos = videos;
                if (!episodeVideos.length) {
                    episodeVideos = season.videos;
                }

                episode.videos = episodeVideos;

                return cb(null, dbConnection, episode);
            });
        }
    ], (err, dbConnection, episode) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, episode);
    });
};