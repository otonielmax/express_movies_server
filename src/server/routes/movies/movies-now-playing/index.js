'use strict';
const async = require('async');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const linkHelper = require('../../../../helpers/link');
const paginationHelper = require('../../../../helpers/pagination');
const utils = require('../../../../helpers/utils');
const getAllGenres = require('../../../managers/movies/genre/getAll');
const getPage = require('../../../managers/movies/movie/getPage');
const getTotal = require('../../../managers/movies/movie/getTotal');
const getFeaturedMovies = require('../../../managers/movies/movie/getFeatured');
const getFeaturedGenre = require('../../../managers/movies/genre/getFeaturedGenre');

/**
 * Ruta utilizada para manejar el "Now Playing" de la aplicacion 
 */
module.exports = (req, res, next) => {
    const { transactionId } = req;
    // Informacion de paginacion
    const pageId = Number(req.params.page || 1);
    const pageLimit = req.website.home.limit.page;
    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;
    const currentLocaleId = currentLanguage.language_locale_id;
    // Informacion del website
    const website = req.website;
    const websiteId = website.id;

    const cacheOptions = {
        localExpireTime: 60,
        storageExpireTime: 60 * 60,
        stringify: true,
    };

    // Realizo todas las peticiones en paralelos
    async.waterfall([
        // Obtener las películas de la página actual
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_MOVIES_NOW_PLAYING_PAGE_${pageId}`;
            const composedFn = utils.compose(getPage, website, currentLanguage, defaultLanguage, {
                pageId,
                pageLimit,
                galleryType: 'now_playing',
            });

            cacheService.getMaster(key, cacheOptions, composedFn, (err, movies) => {
                if (err) {
                    logger.error(`[routes/movies-now-playing] Error obtaining current page movies (${pageId})`, {
                        transactionId,
                        pageId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.page_movies = movies;

                return cb(null);
            });
        },
        // Obtiene la información de la paginación
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_MOVIES_NOW_PLAYING_PAGINATION`;
            const composedFn = utils.compose(getTotal, {
                galleryType: 'now_playing',
            });

            cacheService.getMaster(key, cacheOptions, composedFn, (err, items) => {
                if (err) {
                    logger.error('[routes/movies-now-playing] Error obtaining pagination info', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                const pagination = paginationHelper.get(website, 'movies_now_playing', currentLanguage, pageId, items, pageLimit);
                res.data.context.pagination = pagination;

                return cb(null);
            });
        },
        // Obtener el listado de películas destacadas
        (cb) => {
            getFeaturedMovies(website, currentLanguage, defaultLanguage, (err, movies) => {
                if (err) {
                    logger.error('[routes/movies-now-playing] Error obtaining featured movies', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_movies = movies;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/movies-now-playing] Error obtaining genre list', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        },
        // Obtener el genero destacado de la pagina con sus peliculas
        (cb) => {
            const hash = req.website.featured.genre;
            getFeaturedGenre(website, currentLanguage, defaultLanguage, hash, (err, result) => {
                if (err) {
                    logger.error('[routes/movies-now-playing] Error featured genre', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_genre = result;
                return cb(null);
            });
        },
    ], (err) => {
        if (err) {
            return res.sendError(err);
        }

        // Defino lenguajes alternativos
        const routeName = 'movies_now_playing';
        const alternateLang = linkHelper.getAlternateLang(website, routeName, pageId);
        for (const lang of alternateLang) {
            lang.href = lang.href.replace(':page', pageId);
        }
        res.data.context.site.alternate_lang = alternateLang;

        // Defino datos de la ruta
        res.data.route = 'movies-now-playing';
        res.data.path = '/movies/now_playing';

        return next();
    });
};