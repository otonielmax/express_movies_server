'use strict';
const async = require('async');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const languageHelper = require('../../../../helpers/language');
const adsHelper = require('../../../../helpers/ads');
const dbService = require('../../../../services/db');
const utils = require('../../../../helpers/utils');
const getMovieLink = require('../../../managers/movies/movie/getLink');
const getGenreLink = require('../../../managers/movies/genre/getLink');
const getPersonLink = require('../../../managers/movies/person/getLink');
const getAllGenres = require('../../../managers/movies/genre/getAll');
const getFeaturedMovies = require('../../../managers/movies/movie/getFeatured');
const getMovieList = require('../../../managers/movies/movie/getMovieList');
const getFeaturedGenre = require('../../../managers/movies/genre/getFeaturedGenre');

/**
 * Se utiliza para manejar la ruta de visualizar detalles de una película
 */
module.exports = (req, res, next) => {
    const { transactionId } = req;
    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;
    const currentLocaleId = currentLanguage.language_locale_id;
    // Informacion del website
    const website = req.website;
    const websiteId = website.id;

    const cacheOptions = {
        localExpireTime: 30,
        storageExpireTime: 60 * 60,
        stringify: true,
    };

    // Realizo todas las peticiones en paralelos
    async.waterfall([
        // Obtener la película actual
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_MOVIE_${req.params.hash}`.toUpperCase();

            const composedFn = utils.compose(getMovie, website, currentLanguage, defaultLanguage, req.params.hash);
            cacheService.getMaster(key, cacheOptions, composedFn, (err, movie) => {
                if (err) {
                    if (!err.redirect) {
                        logger.error(`[routes/movie] Error obtaining current movie (${req.params.hash})`, {
                            transactionId,
                            err
                        });
                    }

                    return cb(err);
                }
                res.data.context.movie = movie;

                // Langs alternativos para el meta
                if (movie.alternate_lang) {
                    res.data.context.site.alternate_lang = movie.alternate_lang;
                }

                return cb();
            });
        },
        // Obtener el listado de películas destacadas
        (cb) => {
            getFeaturedMovies(website, currentLanguage, defaultLanguage, (err, movies) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining featured movies', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_movies = movies;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining genre list', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        },
        // Obtener el genero destacado de la pagina con sus peliculas
        (cb) => {
            const hash = req.website.featured.genre;
            getFeaturedGenre(website, currentLanguage, defaultLanguage, hash, (err, result) => {
                if (err) {
                    logger.error('[routes/home] Error featured genre', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_genre = result;
                return cb(null);
            });
        },
    ], (err) => {
        if (err) {
            if (err.redirect && err.redirect_to) {
                return res.redirect(301, err.redirect_to);
            } else {
                return res.sendError(err);
            }
        }

        res.data.route = 'movie';
        res.data.path = req.amp ? '/amp/movies/movie' : '/movies/movie';
        
        // Reemplazar valores del titulo
        res.data.context.site.title = res.data.context.site.title.replace(/{{movie.title}}/g, res.data.context.movie.title);
        res.data.context.site.title = res.data.context.site.title.replace(/{{movie.release_year}}/g, res.data.context.movie.release_year);
        // Reemplazar valores del description
        res.data.context.site.description = res.data.context.site.description.replace(/{{movie.title}}/g, res.data.context.movie.title);
        res.data.context.site.description = res.data.context.site.description.replace(/{{movie.release_year}}/g, res.data.context.movie.release_year);
        res.data.context.site.description = res.data.context.site.description.replace(/{{movie.overview}}/g, res.data.context.movie.overview);
        // Truncar longitud de descripcion
        if (res.data.context.site.description.length > 250) {
            res.data.context.site.description = res.data.context.site.description.substr(0, 250) + '...';
        }

        const playerConfig = Object.assign({}, website.playerConfig);

        // Definir si puede o no mostrar anuncios y eliminar el config de la respuesta
        playerConfig.showPlayer = adsHelper.showAds(playerConfig.adCountries, req.location.country_code, req.isCrawler);
        delete playerConfig.adCountries;

        // Reemplazar valores del titulo en los enlaces de anuncios
        if (playerConfig.adLink && playerConfig.adLink.length) {
            playerConfig.adLink = playerConfig.adLink.map((link) => {
                return link.replace(/{title}/g, encodeURIComponent(res.data.context.movie.title));
            });
        }

        // Asignar configuracion del player
        res.data.context.playerConfig = playerConfig;

        return next();
    });
};

const getMovie = (website, currentLanguage, defaultLanguage, movieHash, callback) => {
    async.waterfall([
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Elegir pelicula en base al hash.
        (dbConnection, cb) => {
            const query = `
                SELECT
                    movie_id
                FROM
                    movie_hash
                WHERE
                    hash = ?
            `;

            dbConnection.query(query, [movieHash], (err, movie) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!movie[0]) {
                    return cb({
                        message: 'Movie not found',
                        code: 'NOT_FOUND'
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie[0].movie_id);
            });
        },
        // Obtiene los datos de la película en base al ID
        (dbConnection, movieId, cb) => {
            const query = `
                SELECT
                    m.id,
                    IFNULL(ml_1.title, IFNULL(ml_2.title, IFNULL(ml_3.title, m.title))) title,
                    IFNULL(a.content,
                        IFNULL(mlw_1.overview,
                            IFNULL(ml_1.overview,
                                IFNULL(mlw_2.overview,
                                    IFNULL(ml_2.overview,
                                        IFNULL(mlw_3.overview,
                                            IFNULL(ml_3.overview,
                                                m.overview))))))) overview,
                    m.original_title,
                    m.tagline,
                    m.popularity,
                    m.poster_path,
                    m.backdrop_path,
                    m.runtime,
                    m.budget,
                    m.revenue,
                    m.adult,
                    m.imdb_id,
                    m.facebook_id,
                    m.instagram_id,
                    m.twitter_id,
                    IFNULL(ml_1.homepage, IFNULL(ml_2.homepage, m.homepage)) homepage,
                    m.vote_count,
                    m.vote_average,
                    m.movie_status_id,
                    m.release_date,
                    YEAR(m.release_date) release_year,
                    MONTH(m.release_date) release_month,
                    DAY(m.release_date) release_day
                FROM
                    movie m
                LEFT JOIN
                    movie_language ml_1 ON (m.id=ml_1.movie_id AND ml_1.language_locale_id=?)
                LEFT JOIN
                    movie_language_website mlw_1 ON (ml_1.id=mlw_1.movie_language_id AND mlw_1.website_id=?)
                LEFT JOIN
                    movie_language ml_2 ON (m.id=ml_2.movie_id AND ml_2.language_id=?)
                LEFT JOIN
                    movie_language_website mlw_2 ON (ml_2.id=mlw_2.movie_language_id AND mlw_2.website_id=?)
                LEFT JOIN
                    movie_language ml_3 ON (m.id=ml_3.movie_id AND ml_3.language_id=?)
                LEFT JOIN
                    movie_language_website mlw_3 ON (ml_3.id=mlw_3.movie_language_id AND mlw_3.website_id=?)
                LEFT JOIN
                    article a ON (a.movie_id=m.id AND a.language_id=? AND a.website_id=?)
                WHERE
                    m.id = ?
                GROUP BY
                    m.id
            `;

            const params = [
                currentLanguage.language_locale_id,
                website.id,
                currentLanguage.language_id,
                website.id,
                defaultLanguage.language_id,
                website.id,
                currentLanguage.language_id,
                website.id,
                movieId
            ];

            dbConnection.query(query, params, (err, movie) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!movie[0]) {
                    return cb(true, dbConnection);
                }

                return cb(null, dbConnection, movie[0]);
            });
        },
        // Obtener listado de hashes de la película
        (dbConnection, movie, cb) => {
            const query = `
                SELECT
                    mh.hash,
                    mh.website_id,
                    l1.id language_id,
                    l2.id language_locale_id
                FROM
                    movie_hash mh
                JOIN
                    language l1 ON mh.language_id = l1.id
                LEFT JOIN
                    language l2 ON mh.language_locale_id = l2.id
                WHERE
                    mh.movie_id = ? AND
                    mh.active = 1 AND
                    (mh.website_id = ? OR mh.website_id IS NULL)
            `;

            dbConnection.query(query, [movie.id, website.id], (err, hashes) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                // Obtengo el hash mas optimo para el lenguaje del usuario
                let preferredHash = getBestHash(website, hashes, currentLanguage, defaultLanguage);

                if (website.language.multiLanguage) {
                    // Si el sitio es multilenguaje, el hash tiene que coincidir con el optimo
                    if (preferredHash.hash !== movieHash) {
                        const newLink = website.host + getMovieLink(website, currentLanguage, {
                            ...movie,
                            hash: preferredHash.hash,
                        });

                        return cb({
                            redirect: true,
                            redirect_to: newLink,
                        }, dbConnection);
                    }
                } else {
                    // Si el sitio no tiene multilenguaje, el hash tiene que existir.

                    // Pero primero tengo que controlar si el hash optimo es especifico para el website
                    // En este caso, es porque el hash fue baneado y hay que usar el nuevo
                    if (preferredHash.hash !== movieHash && preferredHash.website_id !== null) {
                        const newLink = website.host + getMovieLink(website, currentLanguage, {
                            ...movie,
                            hash: preferredHash.hash,
                        });

                        return cb({
                            redirect: true,
                            redirect_to: newLink,
                        }, dbConnection);
                    }

                    // Si no existe es porque tiene valid=0 y tiene que redigir a un hash mejor.
                    const found = hashes.find((h) => h.hash === movieHash);
                    if (!found) {
                        const newLink = website.host + getMovieLink(website, currentLanguage, {
                            ...movie,
                            hash: preferredHash.hash,
                        });

                        return cb({
                            redirect: true,
                            redirect_to: newLink,
                        }, dbConnection);
                    }
                }

                let alternateLangs = [];

                for (let language of website.language.available) {
                    let bestHash = getBestHash(website, hashes, language, defaultLanguage);
                    const langId = language.show_locale ? language.language_locale_id : language.language_id;

                    const hash = {
                        default: language.default ? true : false,
                        hreflang: languageHelper.getLanguage(langId).iso,
                        href: website.host + getMovieLink(website, language, {
                            ...movie,
                            hash: bestHash.hash,
                        }),
                        language_name: languageHelper.getLanguage(langId).name,
                    };

                    alternateLangs.push(hash);
                }

                // Ordeno primero el default
                alternateLangs = alternateLangs.sort((a, b) => {
                    return b.default - a.default;
                });

                movie.alternate_lang = alternateLangs;

                return cb(null, dbConnection, movie);
            });

        },
        // Obtener listado de generos de la película
        (dbConnection, movie, cb) => {
            const query = `
                SELECT
                    g.id,
                    IFNULL(gl_1.name, IFNULL(gl_2.name, gl_3.name)) name,
                    IFNULL(gl_1.hash, IFNULL(gl_2.hash, gl_3.hash)) hash
                FROM
                    genre g
                JOIN
                    movie_genre mg ON g.id=mg.genre_id
                LEFT JOIN
                    genre_language gl_1 ON (g.id = gl_1.genre_id AND gl_1.language_id = ?)
                LEFT JOIN
                    genre_language gl_2 ON (g.id = gl_2.genre_id AND gl_2.language_id = ?)
                LEFT JOIN
                    genre_language gl_3 ON (g.id = gl_3.genre_id AND gl_3.language_id = ?)
                WHERE
                    mg.movie_id = ?
                GROUP BY
                    g.id
                ORDER BY
                    g.name ASC
            `;

            const fallbackLanguageId = languageHelper.getId('en');
            const params = [
                currentLanguage.language_id,
                defaultLanguage.language_id,
                fallbackLanguageId,
                movie.id
            ];

            dbConnection.query(query, params, (err, genres) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                movie.genres = genres;

                // Generar los links
                for (let genre of genres) {
                    genre.link = getGenreLink(website, currentLanguage, genre);
                }

                return cb(null, dbConnection, movie);

            });
        },
        // Obtener listado de videos de la película
        (dbConnection, movie, cb) => {
            const query = `
                SELECT
                    l.name language_name,
                    l.iso language_iso,
                    vm.name,
                    vm.site,
                    vm.key,
                    vm.size,
                    vm.type
                FROM
                    movie_video mv
                JOIN
                    video_media vm ON mv.video_media_id=vm.id
                JOIN
                    language l ON vm.language_id=l.id
                WHERE
                    mv.movie_id = ?
            `;

            dbConnection.query(query, [movie.id], (err, videos) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!videos.length) {
                    videos.push({
                        language_name: 'English',
                        iso: 'en',
                        name: 'Trailer',
                        site: 'YouTube',
                        key: 'uYSo9vMrHjw',
                        size: 1080,
                        type: 'Trailer',
                    });
                }

                movie.videos = videos;

                return cb(null, dbConnection, movie);

            });
        },
        // Obtener actores
        (dbConnection, movie, cb) => {
            const query = `
                SELECT
                    p.id,
                    p.hash,
                    mp.\`character\`,
                    p.name,
                    p.profile_path,
                    p.gender_id,
                    mp.\`order\`
                FROM
                    movie_performer mp
                JOIN
                    person p ON mp.person_id = p.id
                WHERE
                    mp.movie_id=?
                ORDER BY
                    mp.\`order\` ASC
            `;

            dbConnection.query(query, [movie.id], (err, performers) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                for (const performer of performers) {
                    performer.link = getPersonLink(website, currentLanguage, performer);
                }

                movie.performers = performers;

                return cb(null, dbConnection, movie);

            });
        },
        // Obtener crew
        (dbConnection, movie, cb) => {
            const query = `
                SELECT
                    p.id,
                    p.name,
                    p.hash,
                    p.profile_path,
                    p.gender_id,
                    mc.job,
                    mc.department
                FROM
                    movie_crew mc
                JOIN
                    person p ON mc.person_id = p.id
                WHERE
                    mc.movie_id=?
            `;

            dbConnection.query(query, [movie.id], (err, crew) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                for (const c of crew) {
                    c.link = getPersonLink(website, currentLanguage, c);
                }

                movie.crew = crew.filter((c) => c.job !== 'Director');
                movie.directors = crew.filter((c) => c.job === 'Director');

                return cb(null, dbConnection, movie);

            });
        },
        // Obtener ids de peliculas relacionadas
        (dbConnection, movie, cb) => {
            if (!movie.genres || !movie.genres.length) {
                return cb(null, dbConnection, movie, null);
            }

            const query = `
                SELECT
                    m.id
                FROM
                    movie_genre mg
                JOIN
                    movie m ON mg.movie_id = m.id
                WHERE
                    mg.genre_id IN (?) AND
                    mg.movie_id != ? AND
                    m.gallery_popular_position IS NOT NULL
                GROUP BY
                    mg.movie_id
                ORDER BY
                    m.gallery_popular_position ASC
                LIMIT
                    10
            `;

            const genres = movie.genres.map((g) => g.id);

            dbConnection.query(query, [genres, movie.id], (err, result) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                const movieIds = result.map((m) => m.id);

                return cb(null, dbConnection, movie, movieIds);
            });
        },
        // Obtener peliculas relacionadas
        (dbConnection, movie, relatedMovieIds, cb) => {
            if (!relatedMovieIds || !relatedMovieIds.length) {
                movie.related_movies = [];
                return cb(null, dbConnection, movie);
            }

            const params = {
                movieIds: relatedMovieIds,
                galleryType: 'popular'
            };

            getMovieList(dbConnection, website, currentLanguage, defaultLanguage, params, (err, related) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                movie.related_movies = related;

                return cb(null, dbConnection, movie);
            });
        },
        // Obtener el review de la pelicula
        (dbConnection, movie, cb) => {
            const query = `
                SELECT
                    \`key\`,
                    created_at
                FROM
                    movie_review
                WHERE
                    movie_id = ?
            `;

            dbConnection.query(query, [movie.id], (err, reviews) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (reviews && reviews[0]) {
                    movie.review = reviews[0];
                }

                return cb(null, dbConnection, movie);
            });
        },
        // Obtener providers de la pelicula
        (dbConnection, movie, cb) => {
            const query = `
                        SELECT
                            mp.provider_id,
                            p.name provider_name,
                            mp.link,
                            mp.created_at
                        FROM
                            movie_provider mp
                        JOIN
                            provider p ON mp.provider_id = p.id
                        WHERE
                            movie_id = ?
                    `;

            dbConnection.query(query, [movie.id], (err, movieProviders) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                movie.movie_providers = movieProviders;

                return cb(null, dbConnection, movie);
            });
        },
    ], (err, dbConnection, movie) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, movie);
    });
};

const getBestHash = (website, hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;
    // Primero intento agarrar el locale por defecto de la pagina y website correcto
    bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id && h.website_id === website.id);

    // Si no existe, intento agarrar el locale por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina y website correcto
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id && h.website_id === website.id);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id);
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo utilizando el website
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id && h.website_id === website.id);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id && h.website_id === website.id);
        }
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id);
        }
    }

    // Si no existe, agarro ingles + website
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((h) => h.language_id === fallbackLangId && h.website_id === website.id);
    }

    // Si no existe, agarro ingles
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((h) => h.language_id === fallbackLangId);
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};