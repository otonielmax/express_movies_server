'use strict';
const async = require('async');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const paginationHelper = require('../../../../helpers/pagination');
const linkHelper = require('../../../../helpers/link');
const languageHelper = require('../../../../helpers/language');
const dbService = require('../../../../services/db');
const utils = require('../../../../helpers/utils');
const getAllGenres = require('../../../managers/movies/genre/getAll');
const getPage = require('../../../managers/movies/movie/getPage');
const getTotal = require('../../../managers/movies/movie/getTotal');
const getGenre = require('../../../managers/movies/genre/get');
const getFeaturedGenre = require('../../../managers/movies/genre/getFeaturedGenre');
const getFeaturedMovies = require('../../../managers/movies/genre/getFeaturedMovies');

/**
 * Utilizada para manejar la ruta de listar películas de un género
 */
module.exports = (req, res, next) => {
    const { transactionId } = req;
    // Informacion de paginacion
    const pageId = Number(req.params.page || 1);
    const pageLimit = req.website.genre.limit.page;
    // Hash
    const genre = req.params.hash;
    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;
    const currentLocaleId = currentLanguage.language_locale_id;
    // Informacion del website
    const website = req.website;
    const websiteId = website.id;

    const cacheOptions = {
        localExpireTime: 60,
        storageExpireTime: 60 * 15,
        stringify: true,
    };

    // ID de Genero
    let genreId;

    async.waterfall([
        // Obtener información del genero actual
        (cb) => {
            getGenre(website, currentLanguage, defaultLanguage, genre, (err, genreFound) => {
                if (err) {
                    logger.error(`[routes/genre] Error obtaining current genre (${genre})`, {
                        transactionId,
                        genre,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genre = genreFound;
                genreId = genreFound.id;

                return cb(null);
            });
        },
        // Obtener películas destacadas del genero actual
        (cb) => {
            getFeaturedMovies(website, currentLanguage, defaultLanguage, genreId, genre, (err, movies) => {
                if (err) {
                    logger.error(`[routes/genre] Error obtaining current genre featured movies (${genre})`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_movies = movies;

                return cb(null);
            });
        },
        // Obtener películas de la página actual
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_GENRE_${genre}_PAGE_${pageId}`;
            const composedFn = utils.compose(getPage, website, currentLanguage, defaultLanguage, {
                pageId,
                pageLimit,
                genreId,
                galleryType: 'popular',
            });

            cacheService.getMaster(key, cacheOptions, composedFn, (err, movies) => {
                if (err) {
                    logger.error(`[routes/genre] Error obtaining current genre page (${genre} - ${pageId})`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.page_movies = movies;

                return cb(null);
            });
        },
        // Obtener información de la paginación
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_GENRE_${genre}_PAGINATION`;
            const composedFn = utils.compose(getTotal, {
                genreId,
                galleryType: 'popular',
            });

            cacheService.getMaster(key, cacheOptions, composedFn, (err, items) => {
                if (err) {
                    logger.error(`[routes/genre] Error obtaining current genre total items (${genre})`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                const pagination = paginationHelper.get(website, 'genre', currentLanguage, pageId, items, pageLimit);

                // Reemplazar hash
                for (let page of pagination.pages) {
                    if (page.link) {
                        page.link = page.link.replace(/:hash/g, genre);
                    }
                }
                res.data.context.pagination = pagination;

                return cb(null);
            });
        },
        // Obtener links alternativos
        (cb) => {
            const key = `${websiteId}_GENRE_${genre}_PAGE_${pageId}_ALTERNATE_LANGS`;

            const composedFn = utils.compose(getAlternateLangs, website, currentLanguage, defaultLanguage, genreId, pageId);
            cacheService.getMaster(key, cacheOptions, composedFn, (err, alternateLangs) => {
                if (err) {
                    logger.error(`[routes/genre] Error obtaining alternative genre links ${genre}`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.site.alternate_lang = alternateLangs;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining genre list', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        },
        // Obtener el genero destacado de la pagina con sus peliculas
        (cb) => {
            const hash = req.website.featured.genre;
            getFeaturedGenre(website, currentLanguage, defaultLanguage, hash, (err, result) => {
                if (err) {
                    logger.error('[routes/home] Error featured genre', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_genre = result;

                return cb(null);
            });
        }
    ], (err) => {
        if (err) {
            return res.sendError(err);
        }

        res.data.route = 'category';
        res.data.path = '/movies/category';

        // Reemplazar valores del titulo
        res.data.context.site.title = res.data.context.site.title.replace(/{{genre.name}}/g, res.data.context.genre.name);
        // Reemplazar valores de la descripcion
        res.data.context.site.description = res.data.context.site.description.replace(/{{genre.name}}/g, res.data.context.genre.name);
        // Reemplazar valores del canonical
        res.data.context.site.canonical = res.data.context.site.canonical.replace(/{{genre.hash}}/g, res.data.context.genre.hash);

        return next();
    });
};

const getAlternateLangs = (website, currentLanguage, defaultLanguage, genreId, pageId, callback) => {
    async.waterfall([
        (cb) => {
            const query = `
                SELECT
                    gl.hash,
                    gl.language_id
                FROM
                    genre_language gl
                JOIN
                    genre g ON g.id=gl.genre_id
                WHERE
                    g.id = ?
            `;

            dbService.query(query, [genreId], (err, hashLang) => {
                if (err) {
                    return cb(err);
                }

                return cb(null, hashLang);
            });
        },
        (genreLangs, cb) => {
            const routeName = 'genre';
            /*
            * si el numero de pagina es igual a 1, se usa la ruta por defecto
            * de lo contrario se usa la ruta paginada
            */
            const route = pageId === 1
                ? website.routes.find((r) => r.name === routeName).values
                : website.routes.find((r) => r.name === routeName).paginationValues;

            let alternateLangs = [];
            for (const language of website.language.available) {
                const langId = language.show_locale ? language.language_locale_id : language.language_id;
                const langLocaleId = language.language_locale_id;
                const bestHash = getBestHash(genreLangs, language, defaultLanguage);

                let href = website.host + linkHelper.getLanguagePrefix(website, language) + (route[langLocaleId] || route.default);
                href = href.replace(':hash', bestHash.hash).replace(':page', pageId);
                alternateLangs.push({
                    default: language.default ? true : false,
                    hreflang: languageHelper.getLanguage(langId).iso,
                    href: href,
                    language_name: languageHelper.getLanguage(langId).name,
                });
            }

            // Ordeno primero el default
            alternateLangs = alternateLangs.sort((a, b) => {
                return b.default - a.default;
            });

            return cb(null, alternateLangs);
        }
    ], (err, alternateLangs) => {
        return callback(err, alternateLangs);
    });
};

const getBestHash = (hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;
    // Primero intento agarrar el idioma preferido
    bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id);

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id);
    }

    // Si no existe, agarro ingles
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((hash) => hash.language_id === fallbackLangId);
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};