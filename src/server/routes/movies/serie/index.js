'use strict';
const async = require('async');

const { logger } = require('../../../../logger');
const getFeaturedSeries = require('../../../managers/series/serie/getFeatured');
const getAllGenres = require('../../../managers/series/genre/getAll');
const getSerie = require('../../../managers/series/serie/get');
const getSerieLink = require('../../../managers/series/serie/getLink');
const shouldRedirect = require('../../../managers/series/version/shouldRedirect');

/**
 * Se utiliza para manejar la ruta de visualizar detalles de una serie
 */
module.exports = (req, res, next) => {
    const { transactionId } = req;

    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;

    // Informacion del website
    const website = req.website;

    async.waterfall([
        // Obtener la serie
        (cb) => {
            getSerie(website, currentLanguage, defaultLanguage, req.params.hash, (err, serie) => {
                if (err) {
                    if (!err.redirect) {
                        logger.error(`[routes/serie] Error obtaining current serie (${req.params.hash})`, {
                            transactionId,
                            err
                        });
                    }

                    return cb(err);
                }

                // Control de version
                if (shouldRedirect(serie.version, req.params.version)) {
                    const newLink = website.host + getSerieLink(website, currentLanguage, serie);

                    return cb({
                        redirect: true,
                        redirect_to: newLink,
                    });
                }

                res.data.context.serie = serie;

                // Langs alternativos para el meta
                if (serie.alternate_lang) {
                    res.data.context.site.alternate_lang = serie.alternate_lang;
                }

                return cb();
            });
        },
        // Obtener el listado de series destacadas
        (cb) => {
            getFeaturedSeries(website, currentLanguage, defaultLanguage, (err, series) => {
                if (err) {
                    logger.error('[routes/serie] Error obtaining featured series', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_series = series;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/serie] Error obtaining genre list', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        },
    ], (err) => {
        if (err) {
            if (err.redirect && err.redirect_to) {
                return res.redirect(301, err.redirect_to);
            } else {
                return res.sendError(err);
            }
        }

        res.data.route = 'serie';
        res.data.path = req.amp ? '/amp/series/serie' : '/series/serie';

        // Reemplazar valores del titulo
        res.data.context.site.title = res.data.context.site.title.replace(/{{serie.title}}/g, res.data.context.serie.title);

        // Reemplazar valores del description
        res.data.context.site.description = res.data.context.site.description.replace(/{{serie.title}}/g, res.data.context.serie.title);
        res.data.context.site.description = res.data.context.site.description.replace(/{{serie.overview}}/g, res.data.context.serie.overview);

        // Truncar longitud de descripcion
        if (res.data.context.site.description.length > 250) {
            res.data.context.site.description = res.data.context.site.description.substr(0, 250) + '...';
        }

        return next();
    });
};