'use strict';
const async = require('async');
const crypto = require('crypto');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const linkHelper = require('../../../../helpers/link');
const paginationHelper = require('../../../../helpers/pagination');
const utils = require('../../../../helpers/utils');
const getFeaturedMovies = require('../../../managers/movies/movie/getFeatured');
const getAllGenres = require('../../../managers/movies/genre/getAll');
const getPage = require('../../../managers/movies/movie/getPage');
const getTotal = require('../../../managers/movies/movie/getTotal');
const getFeaturedGenre = require('../../../managers/movies/genre/getFeaturedGenre');

module.exports = (req, res, next) => {
    const { transactionId } = req;
    // Informacion de paginacion
    const pageId = Number(req.params.page || 1);
    const pageLimit = req.website.search.limit.page;
    // Parametro de busqueda
    const query = req.query.s;
    if (!query || !query.length) {
        return next();
    }
    const queryHash = crypto.createHash('md5').update(query).digest('hex');
    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;
    const currentLocaleId = currentLanguage.language_locale_id;
    // Informacion del website
    const website = req.website;
    const websiteId = website.id;

    // Guardar los cache de busquedas poco tiempo
    const cacheOptions = {
        localExpireTime: 0,
        storageExpireTime: 60 * 2,
        stringify: true,
    };

    async.waterfall([
        // Obtener la pagina con el search query
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_SEARCH_${queryHash}_PAGE_${pageId}`;

            const composedFn = utils.compose(getPage, website, currentLanguage, defaultLanguage, {
                pageId,
                pageLimit,
                title: query,
                galleryType: ['popular', 'now_playing', 'top_rated', 'upcoming'],
            });
            cacheService.getMaster(key, cacheOptions, composedFn, (err, movies) => {
                if (err) {
                    logger.error(`[routes/search] Error obtaining search page movies (${pageId})`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.page_movies = movies;

                return cb(null);
            });
        },
        // Obtener informacion de la paginacion
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_SEARCH_${queryHash}_PAGINATION`;

            const composedFn = utils.compose(getTotal, {
                title: query,
                galleryType: ['popular', 'now_playing', 'top_rated', 'upcoming'],
            });
            cacheService.getMaster(key, cacheOptions, composedFn, (err, items) => {
                if (err) {
                    logger.error('[routes/search] Error obtaining pagination info', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                const pagination = paginationHelper.get(website, 'search', currentLanguage, pageId, items, pageLimit);

                // Agregar parametro de busqueda
                for (let page of pagination.pages) {
                    if (page.link) {
                        page.link = page.link += '?s=' + query;
                    }
                }

                res.data.context.pagination = pagination;

                return cb(null);
            });
        },
        // Obtener el listado de películas destacadas
        (cb) => {
            getFeaturedMovies(website, currentLanguage, defaultLanguage, (err, movies) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining featured movies', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_movies = movies;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining genre list', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        },
        // Obtener el genero destacado de la pagina con sus peliculas
        (cb) => {
            const hash = req.website.featured.genre;
            getFeaturedGenre(website, currentLanguage, defaultLanguage, hash, (err, result) => {
                if (err) {
                    logger.error('[routes/home] Error featured genre', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_genre = result;
                return cb(null);
            });
        },
    ], (err) => {
        if (err) {
            return res.sendError(err);
        }

        res.data.route = 'search';
        res.data.path = '/search';
        res.data.context.search = query;

        // Reemplazar valores del titulo
        res.data.context.site.title = res.data.context.site.title.replace(/{{search_query}}/g, query);
        // Reemplazar valores del description
        res.data.context.site.description = res.data.context.site.description.replace(/{{search_query}}/g, query);
        // Defino lenguajes alternativos
        const routeName = 'search';
        const alternateLang = linkHelper.getAlternateLang(website, routeName, pageId);
        for (const lang of alternateLang) {
            lang.href = lang.href.replace(':page', pageId);
        }
        res.data.context.site.alternate_lang = alternateLang;

        return next();
    });
};