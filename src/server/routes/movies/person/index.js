'use strict';
const async = require('async');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const paginationHelper = require('../../../../helpers/pagination');
const linkHelper = require('../../../../helpers/link');
const dbService = require('../../../../services/db');
const utils = require('../../../../helpers/utils');
const getAllGenres = require('../../../managers/movies/genre/getAll');
const getPage = require('../../../managers/movies/movie/getPage');
const getTotal = require('../../../managers/movies/movie/getTotal');
const getFeaturedMovies = require('../../../managers/movies/movie/getFeatured');
const getFeaturedGenre = require('../../../managers/movies/genre/getFeaturedGenre');

/**
 * Utilizada para manejar la ruta de listar películas de un género
 */
module.exports = (req, res, next) => {
    const { transactionId } = req;
    // Informacion de paginacion
    const pageId = Number(req.params.page || 1);
    const pageLimit = req.website.home.limit.page;
    // Hash
    const person = req.params.hash;
    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;
    const currentLocaleId = currentLanguage.language_locale_id;
    // Informacion del website
    const website = req.website;
    const websiteId = website.id;

    // Person ID
    let personId;

    const cacheOptions = {
        localExpireTime: 30,
        storageExpireTime: 60 * 15,
        stringify: true,
    };

    async.waterfall([
        // Obtener información de la persona actual
        (cb) => {
            const key = `${websiteId}_PERSON_${person}`;

            const composedFn = utils.compose(getPerson, person);
            cacheService.getMaster(key, cacheOptions, composedFn, (err, person) => {
                if (err) {
                    logger.error(`[routes/person] Error obtaining current person (${person})`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.person = person;
                personId = person.id;

                return cb(null);
            });
        },
        // Obtener películas de la página actual
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_PERSON_${person}_PAGE_${pageId}`;

            const composedFn = utils.compose(getPage, website, currentLanguage, defaultLanguage, {
                personId,
                pageId,
                pageLimit,
                galleryType: ['popular', 'now_playing', 'top_rated', 'upcoming'],
            });
            cacheService.getMaster(key, cacheOptions, composedFn, (err, movies) => {
                if (err) {
                    logger.error(`[routes/person] Error obtaining current person page (${person} - ${pageId})`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.page_movies = movies;

                return cb(null);
            });
        },
        // Obtener información de la paginación
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_PERSON_${person}_PAGINATION`;

            const composedFn = utils.compose(getTotal, {
                personId,
                galleryType: ['popular', 'now_playing', 'top_rated', 'upcoming'],
            });
            cacheService.getMaster(key, cacheOptions, composedFn, (err, items) => {
                if (err) {
                    logger.error(`[routes/person] Error obtaining current person total items (${person})`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                const pagination = paginationHelper.get(website, 'person', currentLanguage, pageId, items, pageLimit);

                // Reemplazar hash
                for (let page of pagination.pages) {
                    if (page.link) {
                        page.link = page.link.replace(/:hash/g, person);
                    }
                }
                res.data.context.pagination = pagination;

                return cb(null);
            });
        },
        // Obtener el listado de películas destacadas
        (cb) => {
            getFeaturedMovies(website, currentLanguage, defaultLanguage, (err, movies) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining featured movies', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_movies = movies;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining genre list', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        },
        // Obtener el genero destacado de la pagina con sus peliculas
        (cb) => {
            const hash = req.website.featured.genre;
            getFeaturedGenre(website, currentLanguage, defaultLanguage, hash, (err, result) => {
                if (err) {
                    logger.error('[routes/home] Error featured genre', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_genre = result;
                return cb(null);
            });
        },
    ], (err) => {
        if (err) {
            return res.sendError(err);
        }

        // Reemplazar valores del titulo
        res.data.context.site.title = res.data.context.site.title.replace(/{{person.name}}/g, res.data.context.person.name);
        // Reemplazar valores de la descripcion
        res.data.context.site.description = res.data.context.site.description.replace(/{{person.name}}/g, res.data.context.person.name);

        // Defino lenguajes alternativos
        const routeName = 'person';
        const alternateLang = linkHelper.getAlternateLang(website, routeName, pageId);
        for (const lang of alternateLang) {
            lang.href = lang.href.replace(':page', pageId).replace(':hash', person);
        }
        res.data.context.site.alternate_lang = alternateLang;


        res.data.route = 'person';
        res.data.path = '/person';

        return next();
    });
};

const getPerson = (personHash, callback) => {
    const query = `
        SELECT
            id,
            external_id,
            hash,
            name,
            profile_path,
            gender_id
        FROM
            person
        WHERE
            hash = ?
    `;
    dbService.query(query, [personHash], (err, person) => {
        if (err) {
            return callback(err);
        }

        if (!person[0]) {
            return callback({
                message: 'Person not found',
                code: 'NOT_FOUND'
            });
        }

        return callback(null, person[0]);
    });
};