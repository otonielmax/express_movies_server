'use strict';
const request = require('request');
const fs = require('fs');
const path = require('path');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const utils = require('../../../../helpers/utils');

const mimeTypes = {
    gif: 'image/gif',
    jpg: 'image/jpeg',
    jpeg: 'image/jpeg',
    png: 'image/png',
};

// Se busca la imagen por defecto
const defaultImagePath = path.join(__dirname, '../../../../assets/defaultImage.jpg');
const defaultImage = fs.readFileSync(defaultImagePath);
const defaultMimeType = 'image/jpeg';

module.exports = (req, res) => {
    const { transactionId } = req;
    const imageSize = req.params.size;
    const imagePath = req.params.path;

    const dotIndex = imagePath.indexOf('.');
    let mimeType;
    if (dotIndex !== -1) {
        const extension = imagePath.substr(dotIndex + 1, imagePath.length);
        mimeType = mimeTypes[extension];
    }

    // Controlar que el MimeType este OK.
    if (!mimeType) {
        res.status(500);
        return res.send();
    }

    const cacheOptions = {
        localExpireTime: 0, // No guardar imagenes en memoria
        storageExpireTime: 60 * 60 * 12, // 12 horas en Redis
        binary: true,
        stringify: true,
    };

    const key = `TMDB_IMAGE_${imageSize}_${imagePath}`;

    const composedFn = utils.compose(proxyImage, imageSize, imagePath);
    cacheService.getMaster(key, cacheOptions, composedFn, (err, image) => {
        if (err) {
            logger.error(`[routes/tmdb-image] Error obtaining image "${imageSize}/${imagePath}"`, {
                transactionId,
                err
            });

            res.type(defaultMimeType);
            return res.send(defaultImage);
        }

        // Extender cache para imagenes
        res.set('Cache-Control', 'public, max-age=86400, s-maxage=31536000');

        res.type(mimeType);
        res.send(image);
    });
};

const proxyImage = (imageSize, imagePath, callback) => {
    const options = {
        url: `https://image.tmdb.org/t/p/${imageSize}/${imagePath}`,
        encoding: null, // enconding en null para que retorne directamente el binario
        timeout: 4000
    };

    request(options, (err, response, buffer) => {
        if (err) {
            return callback(err);
        }

        if (!response || response.statusCode !== 200) {
            return callback(new Error(`Invalid response code from TMDB (${response.statusCode})`));
        }

        return callback(null, buffer);
    });
};
