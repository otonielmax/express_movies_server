'use strict';
const async = require('async');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const linkHelper = require('../../../../helpers/link');
const paginationHelper = require('../../../../helpers/pagination');
const utils = require('../../../../helpers/utils');
const getAllGenres = require('../../../managers/series/genre/getAll');
const getPage = require('../../../managers/series/serie/getPage');
const getTotal = require('../../../managers/series/serie/getTotal');
const getFeaturedSeries = require('../../../managers/series/serie/getFeatured');

/**
 * Ruta utilizada para manejar el listado de las series mas puntuadas de la aplicacion 
 */
module.exports = (req, res, next) => {
    const { transactionId } = req;

    // Informacion de paginacion
    const pageId = Number(req.params.page || 1);
    const pageLimit = req.website.home.limit.page;

    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;
    const currentLocaleId = currentLanguage.language_locale_id;

    // Informacion del website
    const website = req.website;
    const websiteId = website.id;

    const cacheOptions = {
        localExpireTime: 60,
        storageExpireTime: 60 * 60,
        stringify: true,
    };

    async.waterfall([
        // Obtener las series de la página actual
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_SERIES_TOP_RATED_PAGE_${pageId}`;
            const composedFn = utils.compose(getPage, website, currentLanguage, defaultLanguage, {
                pageId,
                pageLimit,
                galleryType: 'top_rated'
            });

            cacheService.getMaster(key, cacheOptions, composedFn, (err, series) => {
                if (err) {
                    logger.error(`[routes/series-top-rated] Error obtaining current page (${pageId})`, {
                        transactionId,
                        err
                    });

                    return cb(err);
                }

                res.data.context.page_series = series;

                return cb(null);
            });
        },
        // Obtiene la información de la paginación
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_SERIES_TOP_RATED_PAGINATION`;
            const composedFn = utils.compose(getTotal, {
                galleryType: 'top_rated'
            });

            cacheService.getMaster(key, cacheOptions, composedFn, (err, items) => {
                if (err) {
                    logger.error('[routes/series-top-rated] Error obtaining pagination info', {
                        transactionId,
                        err
                    });

                    return cb(err);
                }

                const pagination = paginationHelper.get(website, 'series_top_rated', currentLanguage, pageId, items, pageLimit);
                res.data.context.pagination = pagination;

                return cb(null);
            });
        },
        // Obtener el listado de series destacadas
        (cb) => {
            getFeaturedSeries(website, currentLanguage, defaultLanguage, (err, series) => {
                if (err) {
                    logger.error('[routes/series-top-rated] Error obtaining featured series', {
                        transactionId,
                        err
                    });

                    return cb(err);
                }

                res.data.context.featured_series = series;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/series-top-rated] Error obtaining genre list', {
                        transactionId,
                        err
                    });

                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        }
    ], (err) => {
        if (err) {
            return res.sendError(err);
        }

        // Defino lenguajes alternativos
        const routeName = 'series_top_rated';
        const alternateLang = linkHelper.getAlternateLang(website, routeName, pageId);

        for (const lang of alternateLang) {
            lang.href = lang.href.replace(':page', pageId);
        }
        
        res.data.context.site.alternate_lang = alternateLang;

        // Defino datos de la ruta
        res.data.route = 'series-top-rated';
        res.data.path = '/series/top_rated';

        return next();
    });
};