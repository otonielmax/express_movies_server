'use strict';
const async = require('async');

const { logger } = require('../../../../logger');
const cacheService = require('../../../../services/cache');
const linkHelper = require('../../../../helpers/link');
const paginationHelper = require('../../../../helpers/pagination');
const utils = require('../../../../helpers/utils');
const getAllGenres = require('../../../managers/movies/genre/getAll');
const getPage = require('../../../managers/movies/movie/getPage');
const getTotal = require('../../../managers/movies/movie/getTotal');
const getFeaturedMovies = require('../../../managers/movies/movie/getFeatured');
const getFeaturedGenre = require('../../../managers/movies/genre/getFeaturedGenre');

/**
 * Ruta utilizada para manejar el home de la aplicacion 
 */
module.exports = (req, res, next) => {
    const { transactionId } = req;
    // Informacion de paginacion y parametros
    const pageId = Number(req.params.page || 1);
    const pageLimit = req.website.home.limit.page;
    const alternativeSite = req.params.alternative_site;
    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;
    const currentLocaleId = currentLanguage.language_locale_id;
    // Informacion del website
    const website = req.website;
    const websiteId = website.id;

    const cacheOptions = {
        localExpireTime: 60,
        storageExpireTime: 60 * 60,
        stringify: true,
    };

    // Realizo todas las peticiones en paralelos
    async.waterfall([
        // Obtener las películas de la página actual
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_POPULAR_MOVIES_PAGE_${pageId}`;
            const composedFn = utils.compose(getPage, website, currentLanguage, defaultLanguage, {
                pageId,
                pageLimit,
                galleryType: 'popular',
            });

            cacheService.getMaster(key, cacheOptions, composedFn, (err, movies) => {
                if (err) {
                    logger.error(`[routes/home] Error obtaining current page movies (${pageId})`, {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.page_movies = movies;

                return cb(null);
            });
        },
        // Obtiene la información de la paginación
        (cb) => {
            const key = `${websiteId}_${currentLocaleId}_POPULAR_MOVIES_PAGINATION`;
            const composedFn = utils.compose(getTotal, {
                galleryType: 'popular',
            });

            cacheService.getMaster(key, cacheOptions, composedFn, (err, items) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining pagination info', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                const pagination = paginationHelper.get(website, 'home', currentLanguage, pageId, items, pageLimit);
                res.data.context.pagination = pagination;

                return cb(null);
            });
        },
        // Obtener el listado de películas destacadas
        (cb) => {
            getFeaturedMovies(website, currentLanguage, defaultLanguage, (err, movies) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining featured movies', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_movies = movies;

                return cb(null);
            });
        },
        // Obtiene listado de géneros completos
        (cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining genre list', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.genres = genres;

                return cb(null);
            });
        },
        // Obtener el genero destacado de la pagina con sus peliculas
        (cb) => {
            const hash = req.website.featured.genre;
            getFeaturedGenre(website, currentLanguage, defaultLanguage, hash, (err, result) => {
                if (err) {
                    logger.error('[routes/home] Error featured genre', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.featured_genre = result;
                return cb(null);
            });
        },
    ], (err) => {
        if (err) {
            return res.sendError(err);
        }

        // Defino nombre de sitio alternativo
        if (alternativeSite) {
            const altSiteName = alternativeSite.toUpperCase().replace(/-/g, ' ');
            res.data.context.alternative_site_name = altSiteName;
            res.data.context.site.title = res.data.context.site.title.replace(/{{alternative_site}}/g, altSiteName);
            res.data.context.site.description = res.data.context.site.description.replace(/{{alternative_site}}/g, altSiteName);
            res.data.context.alternative_site_hash = alternativeSite;
        }
        
        // Defino lenguajes alternativos
        const routeName = alternativeSite
            ? 'home_alt'
            : 'home';
        const alternateLang = linkHelper.getAlternateLang(website, routeName, pageId);
        for (const lang of alternateLang) {
            lang.href = lang.href.replace(':page', pageId);
            if (alternativeSite) {
                lang.href = lang.href.replace(/:alternative_site/g, alternativeSite);
            }
        }
        res.data.context.site.alternate_lang = alternateLang;

        // Defino datos de la ruta
        res.data.route = 'index';
        res.data.path = '/index';

        return next();
    });
};