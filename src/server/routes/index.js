'use strict';

module.exports = {
    common: require('./common'),
    movies: require('./movies'),
    sports: require('./sports')
};