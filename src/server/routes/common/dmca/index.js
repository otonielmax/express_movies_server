'use strict';

module.exports = (req, res, next) => {
    res.data.route = 'dmca';
    res.data.path = '../common/dmca';

    return next();
};