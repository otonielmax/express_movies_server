'use strict';

module.exports = (req, res, next) => {
    res.data.route = 'contact';
    res.data.path = '../common/contact';

    return next();
};