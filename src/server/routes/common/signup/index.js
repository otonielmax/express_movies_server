'use strict';
const async = require('async');
const config = require('config');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const dbService = require('../../../../services/db');
const utils = require('../../../../helpers/utils');

const SESSION_EXPIRATION = 60 * 60 * 24; // 1 dia en segundos

module.exports = (req, res) => {
    const website = req.website;

    async.waterfall([
        // Validaciones
        (cb) => {
            if (!req.body.email || !utils.isEmail(req.body.email)) {
                return cb({
                    code: 'invalid_input',
                    message: 'Invalid email'
                });
            }

            if (!req.body.name || !utils.isString(req.body.name) || req.body.name.length < 3) {
                return cb({
                    code: 'invalid_input',
                    message: 'Invalid name'
                });
            }

            if (!req.body.password || !utils.isString(req.body.password) || req.body.password.length < 6) {
                return cb({
                    code: 'invalid_input',
                    message: 'Invalid password'
                });
            }

            return cb();
        },
        // Obtener conexion con el Master
        (cb) => {
            dbService.getMasterConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Validar si el email existe
        (dbConnection, cb) => {
            const query = `
                SELECT
                    id
                FROM
                    user
                WHERE
                    email = ? AND
                    website_id = ?
            `;

            dbConnection.query(query, [req.body.email, req.website.id], (err, result) => {
                if (err) {
                    return cb({
                        code: 'internal_error',
                        message: 'Server error. Try again later.'
                    }, dbConnection);
                }

                if (result[0]) {
                    return cb({
                        code: 'user_already_exists',
                        message: 'Email already registered. Please choose a different one.'
                    }, dbConnection);
                }

                return cb(null, dbConnection);
            });
        },
        // Insertar usuario
        (dbConnection, cb) => {
            const query = `
                INSERT INTO
                    user
                SET
                    ?
            `;

            const password = crypto.createHash('md5').update(req.body.password).digest('hex');
            const user = {
                email: req.body.email,
                password: password,
                name: req.body.name,
                created_at: new Date(),
                updated_at: null,
                website_id: website.id,
            };

            dbConnection.query(query, [user], (err, result) => {
                if (err) {
                    return cb({
                        code: 'internal_error',
                        message: 'Server error. Try again later.'
                    }, dbConnection);
                }

                delete user.password;

                user.id = result.insertId;

                return cb(null, dbConnection, user);
            });
        },
        (dbConnection, user, cb) => {
            // Convierto a objeto plano
            const payload = {
                ...user
            };
            const token = jwt.sign(payload, config.SESSION_JWT_KEY, {
                expiresIn: SESSION_EXPIRATION,
            });

            return cb(null, dbConnection, token);
        },
    ], (err, dbConnection, token) => {
        if (dbConnection) {
            dbConnection.release();
        }

        if (err) {
            return res.status(500).send(err);
        }

        res.cookie('session', token, {
            maxAge: 1000 * SESSION_EXPIRATION, // convierto a milisegundos
        });
        return res.status(204).send();
    });
};