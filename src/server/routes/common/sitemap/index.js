'use strict';
const AWS = require('aws-sdk');
const config = require('config');

const { logger } = require('../../../../logger');

AWS.config.update({
    accessKeyId: config.DO_ACCESS_KEY_ID,
    secretAccessKey: config.DO_SECRET_ACCESS_KEY,
});
const s3 = new AWS.S3({
    endpoint: 'nyc3.digitaloceanspaces.com'
});

module.exports = (req, res) => {
    const { transactionId } = req;
    const pageId = req.params.page ? Number(req.params.page) : null;

    getSitemap(req.website, pageId, (err, sitemap) => {
        if (err) {
            logger.error(`[routes/sitemap] Error obtaining sitemap ${pageId} from DO Space`, {
                transactionId,
                pageId,
                err
            });
            return res.sendError({
                code: 'NOT_FOUND'
            });
        }

        res.type('application/xml');
        return res.send(sitemap);
    });
};

const getSitemap = (website, pageId, callback) => {
    const bucket = config.DO_MOVIES_BUCKET;
    const key = pageId
        ? `sitemaps/${website.id}/sitemap_${pageId}.xml`
        : `sitemaps/${website.id}/sitemap.xml`;

    s3.getObject({
        Bucket: bucket,
        Key: key
    }, (err, data) => {
        if (err) {
            return callback(err);
        }

        return callback(null, data.Body);
    });
};