'use strict';

module.exports = (req, res, next) => {
    res.data.route = 'ads';
    res.data.path = '../common/ads';

    res.type('text');

    return next();
};