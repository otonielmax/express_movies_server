'use strict';
const async = require('async');
const config = require('config');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const dbService = require('../../../../services/db');
const utils = require('../../../../helpers/utils');

const SESSION_EXPIRATION = 60 * 60 * 24; // 1 dia en segundos

module.exports = (req, res) => {
    async.waterfall([
        // Validaciones
        (cb) => {
            if (!req.body.email || !utils.isEmail(req.body.email)) {
                return cb({
                    code: 'invalid_input',
                    message: 'Invalid email'
                });
            }

            if (!req.body.password || !utils.isString(req.body.password) || req.body.password.length < 6) {
                return cb({
                    code: 'invalid_input',
                    message: 'Invalid password'
                });
            }

            return cb();
        },
        (cb) => {
            const query = `
                SELECT
                    id,
                    email,
                    name,
                    created_at,
                    updated_at,
                    website_id
                FROM
                    user
                WHERE
                    email = ? AND
                    password = ? AND
                    website_id = ?
            `;

            const password = crypto.createHash('md5').update(req.body.password).digest('hex');
            dbService.query(query, [req.body.email, password, req.website.id], (err, result) => {
                if (err) {
                    return cb({
                        code: 'internal_error',
                        message: 'Server error. Try again later.'
                    });
                }

                if (!result[0]) {
                    return cb({
                        code: 'user_not_found',
                        message: 'Email or password is wrong.'
                    });
                }

                return cb(null, result[0]);
            });
        },
        (user, cb) => {
            // Convierto a objeto plano
            const payload = {
                ...user
            };
            const token = jwt.sign(payload, config.SESSION_JWT_KEY, {
                expiresIn: SESSION_EXPIRATION,
            });

            return cb(null, token);
        },
    ], (err, token) => {
        if (err) {
            return res.status(500).send(err);
        }

        res.cookie('session', token, {
            maxAge: 1000 * SESSION_EXPIRATION, // convierto a milisegundos
        });
        return res.status(204).send();
    });
};