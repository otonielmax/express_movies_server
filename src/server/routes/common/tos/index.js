'use strict';

module.exports = (req, res, next) => {
    res.data.route = 'tos';
    res.data.path = '../common/tos';

    return next();
};