'use strict';

module.exports = (req, res, next) => {
    res.data.route = 'robots';
    res.data.path = '../common/robots';

    res.type('text');

    return next();
};