'use strict';

module.exports = {
    ads: require('./ads'),
    contact: require('./contact'),
    dmca: require('./dmca'),
    login: require('./login'),
    robots: require('./robots'),
    signup: require('./signup'),
    sitemap: require('./sitemap'),
    tos: require('./tos'),
};