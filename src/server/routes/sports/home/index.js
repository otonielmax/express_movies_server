'use strict';
const async = require('async');
const moment = require('moment');

const { logger } = require('../../../../logger');
const utils = require('../../../../helpers/utils');
const adsHelper = require('../../../../helpers/ads');
const cacheService = require('../../../../services/cache');
const linkHelper = require('../../../../helpers/link');
const dbService = require('../../../../services/db');

module.exports = (req, res, next) => {
    const { transactionId } = req;
    // Informacion del website
    const website = req.website;
    // Informacion de idioma
    const defaultLanguage = req.session.default_language;
    const currentLanguage = req.session.current_language;
    const currentLocaleId = currentLanguage.language_locale_id;
    // Informacion de paginacion
    const pageId = Number(req.params.page || 1);

    const cacheOptions = {
        localExpireTime: 60 * 1,
        storageExpireTime: 60 * 5,
        stringify: true
    };

    async.waterfall([
        // Partidos activos
        (cb) => {
            const key = `${currentLocaleId}_SPORTS_HOME_ACTIVE`;
            const composedFn = utils.compose(getActiveGames, currentLanguage, defaultLanguage);

            cacheService.getMaster(key, cacheOptions, composedFn, (err, games) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining active games', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.sports_games = games;

                return cb(null);
            });
        },
        // Partidos viejos
        (cb) => {
            const key = `${currentLocaleId}_SPORTS_HOME_OLD`;
            const composedFn = utils.compose(getOldGames, currentLanguage, defaultLanguage);

            cacheService.getMaster(key, cacheOptions, composedFn, (err, games) => {
                if (err) {
                    logger.error('[routes/home] Error obtaining old games', {
                        transactionId,
                        err
                    });
                    return cb(err);
                }

                res.data.context.old_sports_games = games;

                return cb(null);
            });
        }
    ], (err) => {
        if (err) {
            return res.sendError(err);
        }

        // Defino lenguajes alternativos
        res.data.context.site.alternate_lang = linkHelper.getAlternateLang(website, 'home', pageId);

        // Defino datos de la ruta
        res.data.route = 'index';
        res.data.path = '/index';

        const playerConfig = Object.assign({}, website.playerConfig);

        // Definir si puede o no mostrar anuncios y eliminar el config de la respuesta
        playerConfig.showPlayer = adsHelper.showAds(playerConfig.adCountries, req.location.country_code, req.isCrawler);
        delete playerConfig.adCountries;

        // Asignar configuracion del player
        res.data.context.playerConfig = playerConfig;

        return next();
    });
};


const getActiveGames = (currentLanguage, defaultLanguage, cb) => {
    const query = `
        SELECT
            sg.id,
            sg.sport_id,
            IFNULL(sl_1.name, sl_2.name) sport_name,
            sg.name,
            sg.league,
            sg.date game_date,
            HOUR(sg.date) game_hour,
            MINUTE(sg.date) game_minute
        FROM
            sport_game sg
        LEFT JOIN
            sport s ON (s.id=sg.sport_id)
        LEFT JOIN
            sport_language sl_1 ON (s.id=sl_1.sport_id AND sl_1.language_id=?)
        LEFT JOIN
            sport_language sl_2 ON (s.id=sl_2.sport_id AND sl_2.language_id=?)
        WHERE
            sg.active = ?
        GROUP BY
            sg.id
        ORDER BY
            sg.date ASC
    `;

    const params = [
        currentLanguage.language_id,
        defaultLanguage.language_id,
        1
    ];

    dbService.query(query, params, (err, games) => {
        if (err) {
            return cb(err);
        }

        // Convierte la fecha a formato GMT sino Node lo transforma a string inutilizable.
        if (games && games.length) {
            games = games.map((game) => {
                return {
                    ...game,
                    game_date: moment(game.game_date).utc().format()
                };
            });
        }

        return cb(null, games);
    });
};

const getOldGames = (currentLanguage, defaultLanguage, cb) => {
    const query = `
        SELECT
            sg.id,
            sg.sport_id,
            IFNULL(sl_1.name, sl_2.name) sport_name,
            sg.name,
            sg.league,
            sg.date game_date,
            HOUR(sg.date) game_hour,
            MINUTE(sg.date) game_minute
        FROM
            sport_game sg
        LEFT JOIN
            sport s ON (s.id=sg.sport_id)
        LEFT JOIN
            sport_language sl_1 ON (s.id=sl_1.sport_id AND sl_1.language_id=?)
        LEFT JOIN
            sport_language sl_2 ON (s.id=sl_2.sport_id AND sl_2.language_id=?)
        WHERE
            sg.active = ?
        GROUP BY
            sg.id
        ORDER BY
            sg.date DESC
        LIMIT
            20
    `;

    const params = [
        currentLanguage.language_id,
        defaultLanguage.language_id,
        0
    ];

    dbService.query(query, params, (err, games) => {
        if (err) {
            return cb(err);
        }

        // Convierte la fecha a formato GMT sino Node lo transforma a string inutilizable.
        if (games && games.length) {
            games = games.map((game) => {
                return {
                    ...game,
                    game_date: moment(game.game_date).utc().format()
                };
            });
        }

        return cb(null, games);
    });
};