'use strict';
const linkHelper = require('../../../../helpers/link');

/**
 * Genera el link de una persona
 * 
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    person              - Objecto de la persona
 */
module.exports = (website, currentLanguage, person) => {
    const route = website.routes.find((route) => route.name === 'person');

    if (!route) {
        return '';
    }

    const linkPrefix = linkHelper.getLanguagePrefix(website, currentLanguage);
    const defaultLink = route.values[currentLanguage.language_locale_id] || route.values.default;
    let link = defaultLink;

    for (let key in person) {
        link = link.replace(new RegExp(`:${key}`, 'g'), person[key]);
    }

    return linkPrefix + link;
};