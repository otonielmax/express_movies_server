'use strict';
const squel = require('squel');

const dbService = require('../../../../services/db');
const galleryHelper = require('../../../../helpers/gallery');

/**
 * Cuenta la cantidad de items total en la base de datos, para poder calcular las páginas totales
 * @param {Object} params - Objeto con parametros para filtrar la query
 * @param {String|Array} params.galleryType - Tipo de galeria: popular, now_playing, top_rated, upcoming
 * @param {Number} [params.genreId] - Genero a filtrar para obtener ids de peliculas
 * @param {Number} [params.personId] - Id de persona a filtrar, para obtener peliculas solo de esa persona
 * @param   {Function} callback - Funcion para controlar la respuesta
 */
module.exports = (params, callback) => {
    const columnNames = [];

    // Se puede enviar un array de gallerias a usar, o solo una en conreto.
    if (Array.isArray(params.galleryType)) {
        for (const gallery of params.galleryType) {
            columnNames.push(galleryHelper.getColumName(gallery));
        }
    } else {
        columnNames.push(galleryHelper.getColumName(params.galleryType));
    }

    const galleryWheres = squel.expr();
    const query = squel.select()
        .field('COUNT(DISTINCT m.id)', 'count')
        .from('movie', 'm');

    if (params.personId) {
        query
            .left_join('movie_performer', 'mp',
                squel.expr()
                    .and('mp.movie_id = m.id')
                    .and('mp.person_id = ?', params.personId)
            )
            .left_join('movie_crew', 'mc',
                squel.expr()
                    .and('mc.movie_id = m.id')
                    .or('mc.person_id = ?', params.personId)
            )
            .where(
                squel.expr()
                    .and('mp.person_id = ?', params.personId)
                    .and('mc.person_id = ?', params.personId)
            );
    }

    if (params.genreId) {
        query
            .join('movie_genre', 'mg',
                squel.expr()
                    .and('mg.movie_id = m.id')
                    .and('mg.genre_id = ?', params.genreId)
            );
    }

    if (params.title) {
        query
            .join('movie_language', 'ml', 'ml.movie_id = m.id')
            .where('ml.title LIKE ?', `%${params.title}%`);
    }

    // Arma los filtros "or" por cada columna: WHERE (gallery1 IS NOT NUL OR galleryN IS NOT NULL)
    for (const column of columnNames) {
        query.order(`m.${column}`, 'ASC');
        galleryWheres.or(`m.${column} IS NOT NULL`);
    }

    query.where(galleryWheres);

    const parsedQuery = query.toParam();

    dbService.query(parsedQuery.text, parsedQuery.values, (err, result) => {
        if (err) {
            return callback(err);
        }

        const count = result[0] ? result[0].count : 0;

        return callback(null, count);
    });
};