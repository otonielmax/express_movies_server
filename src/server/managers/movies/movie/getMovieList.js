'use strict';
const async = require('async');
const squel = require('squel');
const stripHtml = require('string-strip-html');

const languageHelper = require('../../../../helpers/language');
const galleryHelper = require('../../../../helpers/gallery');
const getAllGenres = require('../genre/getAll');
const getLink = require('./getLink');

/**
 * Obtiene un listado de peliculas en base a un listado de IDS
 * @param {Object} dbConnection - Objecto de conexion a la DB
 * @param {Object} website - Configuracion del sitio
 * @param {Object} currentLanguage - Lenguaje actual
 * @param {Object} defaultLanguage - Lenguaje por defecto del sitio
 * @param {Object} params - Objeto con parametros para filtrar la query
 * @param {Array} params.movieIds - Arreglo de IDs de peliculas a buscar
 * @param {String} params.galleryType - Tipo de galeria: popular, now_playing, top_rated, upcoming
 * @param {Function} callback 
 */
module.exports = (dbConnection, website, currentLanguage, defaultLanguage, params, callback) => {
    if (!params.movieIds || !params.movieIds.length) {
        return callback(null, []);
    }

    const columnNames = [];

    // Se puede enviar un array de gallerias a usar, o solo una en conreto.
    if (Array.isArray(params.galleryType)) {
        for (const gallery of params.galleryType) {
            columnNames.push(galleryHelper.getColumName(gallery));
        }
    } else {
        columnNames.push(galleryHelper.getColumName(params.galleryType));
    }
    
    async.waterfall([
        // Obtener informacion de las peliculas
        (cb) => {
            const galleryWheres = squel.expr();
            const fallbackLanguage = languageHelper.getId('en');
            const query = squel.select()
                .field('m.id', 'id')
                .field(`IFNULL(mh_1.hash,
                    IFNULL(mh_2.hash,
                        IFNULL(mh_3.hash,
                            IFNULL(mh_4.hash,
                                IFNULL(mh_5.hash,
                                    IFNULL(mh_6.hash,
                                        IFNULL(mh_7.hash,
                                            mh_8.hash
                                        )
                                    )
                                )
                            )
                        )
                    )
                )`, 'hash')
                .field(`IFNULL(ml_1.title, 
                    IFNULL(ml_2.title, 
                        IFNULL(ml_3.title, 
                            m.title
                        )
                    )
                )`, 'title')
                .field(`IFNULL(a.content,
                    IFNULL(mlw_1.overview,
                        IFNULL(ml_1.overview,
                            IFNULL(mlw_2.overview,
                                IFNULL(ml_2.overview,
                                    IFNULL(mlw_3.overview,
                                        IFNULL(ml_3.overview,
                                            m.overview
                                        )
                                    )
                                )
                            )
                        )
                    )
                )`, 'overview')
                .field('m.original_title', 'original_title')
                .field('m.tagline', 'tagline')
                .field('m.popularity', 'popularity')
                .field('m.poster_path', 'poster_path')
                .field('m.backdrop_path', 'backdrop_path')
                .field('m.runtime', 'runtime')
                .field('m.budget', 'budget')
                .field('m.revenue', 'revenue')
                .field('m.adult', 'adult')
                .field('m.imdb_id', 'imdb_id')
                .field('m.facebook_id', 'facebook_id')
                .field('m.instagram_id', 'instagram_id')
                .field('m.twitter_id', 'twitter_id')
                .field('m.vote_count', 'vote_count')
                .field('m.vote_average', 'vote_average')
                .field('m.movie_status_id', 'movie_status_id')
                .field('m.release_date', 'release_date')
                .field('YEAR(m.release_date)', 'release_year')
                .field('MONTH(m.release_date)', 'release_month')
                .field('DAY(m.release_date)', 'release_day')
                .from('movie', 'm')
                // Hash  
                .left_join('movie_hash', 'mh_1',
                    squel.expr()
                        .and('m.id = mh_1.movie_id')
                        .and('mh_1.language_locale_id = ?', currentLanguage.language_locale_id)
                        .and('mh_1.active = 1')
                        .and('mh_1.website_id = ?', website.id)
                )
                .left_join('movie_hash', 'mh_2',
                    squel.expr()
                        .and('m.id = mh_2.movie_id')
                        .and('mh_2.language_locale_id = ?', currentLanguage.language_locale_id)
                        .and('mh_2.active = 1')
                        .and('mh_2.website_id IS NULL')
                )
                .left_join('movie_hash', 'mh_3',
                    squel.expr()
                        .and('m.id = mh_3.movie_id')
                        .and('mh_3.language_id = ?', currentLanguage.language_id)
                        .and('mh_3.active = 1')
                        .and('mh_3.website_id = ?', website.id)
                )
                .left_join('movie_hash', 'mh_4',
                    squel.expr()
                        .and('m.id = mh_4.movie_id')
                        .and('mh_4.language_id = ?', currentLanguage.language_id)
                        .and('mh_4.active = 1')
                        .and('mh_4.website_id IS NULL')
                )
                .left_join('movie_hash', 'mh_5',
                    squel.expr()
                        .and('m.id = mh_5.movie_id')
                        .and('mh_5.language_id = ?', defaultLanguage.language_id)
                        .and('mh_5.active = 1')
                        .and('mh_5.website_id = ?', website.id)
                )
                .left_join('movie_hash', 'mh_6',
                    squel.expr()
                        .and('m.id = mh_6.movie_id')
                        .and('mh_6.language_id = ?', defaultLanguage.language_id)
                        .and('mh_6.active = 1')
                        .and('mh_6.website_id IS NULL')
                )
                .left_join('movie_hash', 'mh_7',
                    squel.expr()
                        .and('m.id = mh_7.movie_id')
                        .and('mh_7.language_id = ?', fallbackLanguage, website.id)
                        .and('mh_7.active = 1')
                        .and('mh_7.website_id = ?', website.id)
                )
                .left_join('movie_hash', 'mh_8',
                    squel.expr()
                        .and('m.id = mh_8.movie_id')
                        .and('mh_8.language_id = ?', fallbackLanguage)
                        .and('mh_8.active = 1')
                        .and('mh_8.website_id IS NULL')
                )
                // Languages
                .left_join('movie_language', 'ml_1',
                    squel.expr()
                        .and('m.id = ml_1.movie_id')
                        .and('ml_1.language_locale_id = ?', currentLanguage.language_locale_id)
                )
                .left_join('movie_language_website', 'mlw_1',
                    squel.expr()
                        .and('ml_1.id = mlw_1.movie_language_id')
                        .and('mlw_1.website_id = ?', website.id)
                )
                .left_join('movie_language', 'ml_2',
                    squel.expr()
                        .and('m.id = ml_2.movie_id')
                        .and('ml_2.language_id = ?', currentLanguage.language_id)
                )
                .left_join('movie_language_website', 'mlw_2',
                    squel.expr()
                        .and('ml_2.id = mlw_2.movie_language_id')
                        .and('mlw_2.website_id = ?', website.id)
                )
                .left_join('movie_language', 'ml_3',
                    squel.expr()
                        .and('m.id = ml_3.movie_id')
                        .and('ml_3.language_id = ?', defaultLanguage.language_id)
                )
                .left_join('movie_language_website', 'mlw_3',
                    squel.expr()
                        .and('ml_3.id = mlw_3.movie_language_id')
                        .and('mlw_3.website_id = ?', website.id)
                )
                .left_join('article', 'a',
                    squel.expr()
                        .and('a.movie_id = m.id')
                        .and('a.language_id = ?', currentLanguage.language_id)
                        .and('a.website_id = ?', website.id)
                )
                .where('m.id IN ?', params.movieIds)
                .group('m.id');

            // Arma los filtros "or" por cada columna: WHERE (gallery1 IS NOT NUL OR galleryN IS NOT NULL)
            for (const column of columnNames) {
                query.order(`m.${column}`, 'ASC');
                galleryWheres.or(`m.${column} IS NOT NULL`);
            }

            query.where(galleryWheres);

            const parsedQuery = query.toParam();

            dbConnection.query(parsedQuery.text, parsedQuery.values, (err, movies) => {
                if (err) {
                    return cb(err);
                }

                if (movies.length) {
                    // Asignar links a cada una de las peliculas
                    for (const movie of movies) {
                        movie.link = getLink(website, currentLanguage, movie);
                        movie.genres = [];
                        // Eliminar HTML (para los articulos)
                        if (movie.overview) {
                            movie.overview = stripHtml(movie.overview);
                        }
                    }
                }

                return cb(null, movies);
            });
        },
        // Obtener listado de generos
        (movies, cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                return cb(err, movies, genres);
            });
        },
        // Obtener los generos de todas las peliculas
        (movies, genres, cb) => {
            const query = `
                SELECT
                    genre_id,
                    movie_id
                FROM
                    movie_genre
                WHERE
                    movie_id IN (?)
            `;

            dbConnection.query(query, [params.movieIds], (err, movieGenres) => {
                if (err) {
                    return cb(err);
                }

                for (const movieGenre of movieGenres) {
                    const movie = movies.find((m) => m.id === movieGenre.movie_id);
                    if (movie) {
                        if (!movie.genres) {
                            movie.genres = [];
                        }
                        const genre = genres.find((g) => g.id === movieGenre.genre_id);
                        if (genre) {
                            movie.genres.push(genre);
                        }
                    }
                }

                return cb(null, movies);
            });
        },
    ], (err, movies) => {
        return callback(err, movies);
    });
};