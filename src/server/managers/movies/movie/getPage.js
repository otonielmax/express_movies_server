'use strict';
const squel = require('squel');
const async = require('async');

const getMovieList = require('./getMovieList');
const dbService = require('../../../../services/db');
const galleryHelper = require('../../../../helpers/gallery');

/**
 * Obtiene la página solicitada
 * @param {Object} website - Configuracion del sitio
 * @param {Object} currentLanguage - Lenguaje actual
 * @param {Object} defaultLanguage - Lenguaje por defecto del sitio
 * @param {Object} params - Objeto con parametros para filtrar la query
 * @param {Number} params.pageId - Número de página actual
 * @param {Number} params.pageLimit - Cantidad de items por página
 * @param {String|Array} params.galleryType - Tipo de galeria: popular, now_playing, top_rated, upcoming
 * @param {Number} [params.genreId] - Genero a filtrar para obtener ids de peliculas
 * @param {Number} [params.personId] - Id de persona a filtrar, para obtener peliculas solo de esa persona
 * @param {Function} callback 
 */
module.exports = (website, currentLanguage, defaultLanguage, params, callback) => {
    const offset = (params.pageLimit * (params.pageId - 1));
    const columnNames = [];

    // Se puede enviar un array de gallerias a usar, o solo una en conreto.
    if (Array.isArray(params.galleryType)) {
        for (const gallery of params.galleryType) {
            columnNames.push(galleryHelper.getColumName(gallery));
        }
    } else {
        columnNames.push(galleryHelper.getColumName(params.galleryType));
    }

    async.waterfall([
        // Obtener conexion
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Primero elegir los IDS de peliculas
        // Se obtienen los IDS ya que las query hacen muchos JOIN y es mas óptimo primer filtrar los IDS y luego obtener las películas por IDS
        (dbConnection, cb) => {
            const galleryWheres = squel.expr();
            const query = squel.select()
                .field('m.id', 'id')
                .from('movie', 'm')
                .limit(params.pageLimit)
                .offset(offset)
                .group('m.id');

            if (params.personId) {
                query
                    .left_join('movie_performer', 'mp',
                        squel.expr()
                            .and('mp.movie_id = m.id')
                            .and('mp.person_id = ?', params.personId)
                    )
                    .left_join('movie_crew', 'mc',
                        squel.expr()
                            .and('mc.movie_id = m.id')
                            .and('mc.person_id = ?', params.personId)
                    )
                    .where(
                        squel.expr()
                            .and('mp.person_id = ?', params.personId)
                            .or('mc.person_id = ?', params.personId)
                    );
            }

            if (params.genreId) {
                query
                    .join('movie_genre', 'mg',
                        squel.expr()
                            .and('mg.movie_id = m.id')
                            .and('mg.genre_id = ?', params.genreId)
                    );
            }

            if (params.title) {
                query
                    .join('movie_language', 'ml', 'ml.movie_id = m.id')
                    .where('ml.title LIKE ?', `%${params.title}%`);
            }

            // Arma los filtros "or" por cada columna: WHERE (gallery1 IS NOT NUL OR galleryN IS NOT NULL)
            for (const column of columnNames) {
                query.order(`m.${column}`, 'ASC');
                galleryWheres.or(`m.${column} IS NOT NULL`);
            }

            query.where(galleryWheres);

            const parsedQuery = query.toParam();

            dbService.query(parsedQuery.text, parsedQuery.values, (err, result) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                const movieIds = result.map((m) => m.id);

                return cb(null, dbConnection, movieIds);
            });
        },
        // Obtener peliculas en base a los IDs
        (dbConnection, movieIds, cb) => {
            if (!movieIds || !movieIds.length) {
                return cb(null, dbConnection, []);
            }

            // Agrega los ids obtenidos antes, para obtener info de estos ids
            params.movieIds = movieIds;

            getMovieList(dbConnection, website, currentLanguage, defaultLanguage, params, (err, movies) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                return cb(null, dbConnection, movies);
            });
        },
    ], (err, dbConnection, movies) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, movies);
    });
};