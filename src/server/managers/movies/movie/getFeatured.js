'use strict';
const async = require('async');

const utils = require('../../../../helpers/utils');
const cacheService = require('../../../../services/cache');
const dbService = require('../../../../services/db');
const getMovieList = require('./getMovieList');

/**
 * Obtiene las peliculas destacadas (mayor popularidad)
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 */
const getFeatured = (website, currentLanguage, defaultLanguage, callback) => {
    async.waterfall([
        // Obtener conexion
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Primero elegir los IDS de peliculas destacadas
        // Se obtienen los IDS ya que las query hacen muchos JOIN y es mas óptimo primer filtrar los IDS y luego obtener las películas por IDS
        (dbConnection, cb) => {
            // Ordena las películas por popularidad
            const query = `
                SELECT
                    id
                FROM
                    movie
                WHERE
                    gallery_popular_position IS NOT NULL
                ORDER BY
                    gallery_popular_position ASC
                LIMIT
                    ?
            `;
            const limit = website.home.limit.featured;

            dbConnection.query(query, [limit], (err, result) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                const movieIds = result.map((m) => m.id);

                return cb(null, dbConnection, movieIds);
            });
        },
        // Obtener peliculas en base a los ID
        (dbConnection, movieIds, cb) => {
            // Si no hay películas, sigue
            if (!movieIds || !movieIds.length) {
                return cb(null, dbConnection, []);
            }

            const params = {
                movieIds,
                galleryType: 'popular'
            };
            getMovieList(dbConnection, website, currentLanguage, defaultLanguage, params, (err, movies) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                return cb(null, dbConnection, movies);
            });
        },
    ], (err, dbConnection, movies) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, movies);
    });
};

/**
 * Obtiene las peliculas destacadas (mayor popularidad), utilizando el cache
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 */
module.exports = (website, currentLanguage, defaultLanguage, callback) => {
    const websiteId = website.id;
    const currentLocaleId = currentLanguage.language_locale_id;
    const key = `${websiteId}_${currentLocaleId}_FEATURED_MOVIES`;

    const cacheOptions = {
        localExpireTime: 60 * 1,
        storageExpireTime: 60 * 60,
        stringify: true,
    };
    const composedFn = utils.compose(getFeatured, website, currentLanguage, defaultLanguage);
    cacheService.getMaster(key, cacheOptions, composedFn, (err, movies) => {
        return callback(err, movies);
    });
};