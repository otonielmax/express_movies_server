'use strict';
const linkHelper = require('../../../../helpers/link');

/**
 * Genera el link de una pelicula
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    movie               - Objeto de la pelicula
 */
module.exports = (website, currentLanguage, movie) => {
    const route = website.routes.find((route) => route.name === 'movie');
    const linkPrefix = linkHelper.getLanguagePrefix(website, currentLanguage);
    const defaultLink = route.values[currentLanguage.language_locale_id] || route.values.default;
    let link = defaultLink;

    for (let key in movie) {
        link = link.replace(new RegExp(`:${key}`, 'g'), movie[key]);
    }

    return linkPrefix + link;
};