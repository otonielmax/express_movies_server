'use strict';
const async = require('async');

const utils = require('../../../../helpers/utils');
const cacheService = require('../../../../services/cache');
const dbService = require('../../../../services/db');
const getMovieList = require('../movie/getMovieList');

/**
 * Obtiene listo de peliculas destacadas de un genero
 * 
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {String}    genreId             - ID del genero
 */
const getFeaturedMovies = (website, currentLanguage, defaultLanguage, genreId, callback) => {
    async.waterfall([
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Primero elegir las peliculas
        (dbConnection, cb) => {
            const query = `
                SELECT
                    m.id
                FROM
                    movie m
                JOIN
                    movie_genre mg ON mg.movie_id=m.id 
                WHERE
                    gallery_popular_position IS NOT NULL AND
                    mg.genre_id = ?          
                ORDER BY
                    gallery_popular_position ASC
                LIMIT
                    ?
            `;
            const limit = website.genre.limit.featured;

            dbConnection.query(query, [genreId, limit], (err, result) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                const movieIds = result.map((m) => m.id);

                return cb(null, dbConnection, movieIds);
            });
        },
        // Obtener peliculas
        (dbConnection, movieIds, cb) => {
            const params = {
                movieIds,
                galleryType: 'popular'
            };
            getMovieList(dbConnection, website, currentLanguage, defaultLanguage, params, (err, movies) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                return cb(null, dbConnection, movies);
            });
        },
    ], (err, dbConnection, movies) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, movies);
    });
};

/**
 * Obtiene listo de peliculas destacadas de un genero, utilizando el cache
 * 
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {String}    genreId             - ID del genero
 */
module.exports = (website, currentLanguage, defaultLanguage, genreId, hash, callback) => {
    const websiteId = website.id;
    const currentLocaleId = currentLanguage.language_locale_id;
    const key = `${websiteId}_${currentLocaleId}_GENRE_${hash}_FEATURED`;

    const cacheOptions = {
        localExpireTime: 60 * 10,
        storageExpireTime: 60 * 60,
        stringify: true,
    };
    const composedFn = utils.compose(getFeaturedMovies, website, currentLanguage, defaultLanguage, genreId);
    cacheService.getMaster(key, cacheOptions, composedFn, (err, movies) => {
        if (err) {
            return callback(err);
        }

        return callback(err, movies);
    });
};