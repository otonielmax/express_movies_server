'use strict';
const languageHelper = require('../../../../helpers/language');
const utils = require('../../../../helpers/utils');
const cacheService = require('../../../../services/cache');
const dbService = require('../../../../services/db');
const getLink = require('./getLink');

/**
 * Obtiene un genero a traves del hash
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {String}    hash                - Hash a buscar 
 */
const getGenre = (website, currentLanguage, defaultLanguage, hash, callback) => {
    const query = `
        SELECT
            g.id,
            IFNULL(gl_1.name, IFNULL(gl_2.name, gl_3.name)) name,
            IFNULL(gl_1.hash, IFNULL(gl_2.hash, gl_3.hash)) hash
        FROM
            genre g
        LEFT JOIN
            genre_language gl_1 ON (g.id = gl_1.genre_id AND gl_1.language_id = ?)
        LEFT JOIN
            genre_language gl_2 ON (g.id = gl_2.genre_id AND gl_2.language_id = ?)
        LEFT JOIN
            genre_language gl_3 ON (g.id = gl_3.genre_id AND gl_3.language_id = ?)
        WHERE
            gl_1.hash = ? OR gl_2.hash = ? OR gl_3.hash = ?
        GROUP BY
            g.id
        ORDER BY
            name ASC
    `;

    const fallbackLanguageId = languageHelper.getId('en');
    const params = [
        currentLanguage.language_id,
        defaultLanguage.language_id,
        fallbackLanguageId,
        hash,
        hash,
        hash
    ];

    dbService.query(query, params, (err, genres) => {
        if (err) {
            return callback(err);
        }

        if (!genres || !genres[0]) {
            return callback({
                message: 'Genre not found',
                code: 'NOT_FOUND'
            });
        }

        const genre = genres[0];
        genre.link = getLink(website, currentLanguage, genre);

        return callback(null, genre);
    });
};

/**
 * Obtiene un genero a traves del hash, utilizando/guardando el cache
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {String}    hash                - Hash a buscar 
 */
module.exports = (website, currentLanguage, defaultLanguage, hash, callback) => {
    const websiteId = website.id;
    const currentLocaleId = currentLanguage.language_locale_id;
    const key = `${websiteId}_${currentLocaleId}_GENRE_${hash}`;

    const cacheOptions = {
        localExpireTime: 60 * 10,
        storageExpireTime: 60 * 60,
        stringify: true,
    };
    const composedFn = utils.compose(getGenre, website, currentLanguage, defaultLanguage, hash);
    cacheService.getMaster(key, cacheOptions, composedFn, (err, genre) => {
        return callback(err, genre);
    });
};