'use strict';
const async = require('async');

const getGenre = require('./get');
const getFeaturedMovies = require('./getFeaturedMovies');

/**
 * Obtiene el genero destacado de un sitio con sus peliculas destacadas
 * Utiliza el cache de otros modulos
 * 
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {String}    hash                - hash del genero
 */
module.exports = (website, currentLanguage, defaultLanguage, hash, callback) => {
    async.waterfall([
        (cb) => {
            getGenre(website, currentLanguage, defaultLanguage, hash, (err, genre) => {
                if (err) {
                    return cb(err);
                }

                if (!genre) {
                    return cb(new Error('Invalid genre'));
                }

                return cb(err, genre);
            });
        },
        (genre, cb) => {
            getFeaturedMovies(website, currentLanguage, defaultLanguage, genre.id, hash, (err, movies) => {
                const result = {
                    genre,
                    movies
                };

                return cb(err, result);
            });
        }
    ], (err, result) => {
        return callback(err, result);
    });
};