'use strict';
const async = require('async');

const utils = require('../../../../helpers/utils');
const languageHelper = require('../../../../helpers/language');
const hashHelper = require('../../../../helpers/hash');
const cacheService = require('../../../../services/cache');
const dbService = require('../../../../services/db');
const getPersonLink = require('../../movies/person/getLink');
const getEpisodeLink = require('../../series/episode/getLink');
const getLink = require('./getLink');

/**
 * Obtiene todos los datos basicos de una temporada en concreto de una serie
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {Object}    serie               - Objeto con datos de la serie
 * @param   {Number}    seasonNumber        - Numero se season a buscar
 * @param   {Function}  callback 
 */
module.exports = (website, currentLanguage, defaultLanguage, serie, seasonNumber, callback) => {
    const key = `${website.id}_${currentLanguage.language_locale_id}_SERIE_${serie.hash}_S_${seasonNumber}`.toUpperCase();
    const composedFn = utils.compose(getSeason, website, currentLanguage, defaultLanguage, serie, seasonNumber);
    const cacheOptions = {
        localExpireTime: 30,
        storageExpireTime: 60 * 60,
        stringify: true,
    };

    cacheService.getMaster(key, cacheOptions, composedFn, (err, season) => {
        if (err) {
            return callback(err);
        }

        return callback(null, season);
    });
};

const getSeason = (website, currentLanguage, defaultLanguage, serie, seasonNumber, callback) => {
    async.waterfall([
        // Obtener conexion a la base de datos
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Obtener datos basicos de la temporada
        (dbConnection, cb) => {
            const query = `
                SELECT
                    se.id,
                    se.title,
                    se.overview,
                    se.season_number,
                    se.poster_path,
                    se.air_date,
                    sv.version
                FROM
                    season se
                LEFT JOIN
                    serie_version sv ON (sv.season_id=se.id AND sv.website_id=?)
                WHERE
                    se.serie_id = ? AND
                    se.season_number = ?
                GROUP BY
                    se.id
            `;

            // Las season no tienen traducciones, asique solo agarramos el titulo y overview que tenga
            const params = [
                website.id,
                serie.id,
                seasonNumber
            ];

            dbConnection.query(query, params, (err, season) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!season[0]) {
                    return cb(true, dbConnection);
                }

                return cb(null, dbConnection, season[0]);
            });
        },
        // Obtener listado de hashes de la season
        (dbConnection, season, cb) => {
            const query = `
                SELECT
                    sh.hash,
                    l1.id language_id,
                    l2.id language_locale_id
                FROM
                    serie_hash sh
                JOIN
                    language l1 ON sh.language_id = l1.id
                LEFT JOIN
                    language l2 ON sh.language_locale_id = l2.id
                WHERE
                    sh.serie_id = ?
            `;

            dbConnection.query(query, [serie.id], (err, hashes) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                // Obtengo el hash mas optimo para el lenguaje del usuario
                let preferredHash = hashHelper.getBestHash(hashes, currentLanguage, defaultLanguage);

                if (website.language.multiLanguage) {
                    // Si el sitio es multilenguaje, el hash tiene que coincidir con el optimo
                    if (preferredHash.hash !== serie.hash) {
                        const newLink = website.host + getLink(website, currentLanguage, {
                            ...serie,
                            ...season,
                            hash: preferredHash.hash,
                        });

                        return cb({
                            redirect: true,
                            redirect_to: newLink,
                        }, dbConnection);
                    }
                }

                let alternateLangs = [];

                for (let language of website.language.available) {
                    let bestHash = hashHelper.getBestHash(hashes, language, defaultLanguage);
                    const langId = language.show_locale ? language.language_locale_id : language.language_id;

                    const hash = {
                        default: language.default ? true : false,
                        hreflang: languageHelper.getLanguage(langId).iso,
                        href: website.host + getLink(website, language, {
                            ...serie,
                            ...season,
                            hash: bestHash.hash,
                        }),
                        language_name: languageHelper.getLanguage(langId).name,
                    };

                    alternateLangs.push(hash);
                }

                // Ordeno primero el default
                alternateLangs = alternateLangs.sort((a, b) => {
                    return b.default - a.default;
                });

                season.alternate_lang = alternateLangs;

                return cb(null, dbConnection, season);
            });
        },
        // Obtener listado de episodios
        (dbConnection, season, cb) => {
            const query = `
                SELECT
                    e.id,
                    IFNULL(sl_1.title, 
                        IFNULL(sl_2.title, 
                            IFNULL(sl_3.title,
                                e.title
                            )
                        )
                    ) title,
                    IFNULL(sl_1.overview,
                        IFNULL(sl_2.overview,
                            IFNULL(sl_3.overview,
                                e.overview
                            )
                        )
                    ) overview,
                    e.episode_number,
                    e.still_path,
                    e.air_date,
                    e.vote_count,
                    e.vote_average,
                    sv.version
                FROM
                    serie s
                JOIN
                    episode e ON e.serie_id = s.id
                LEFT JOIN
                    serie_language sl_1 ON (e.id=sl_1.episode_id AND sl_1.language_locale_id=?)
                LEFT JOIN
                    serie_language sl_2 ON (e.id=sl_2.episode_id AND sl_2.language_id=?)
                LEFT JOIN
                    serie_language sl_3 ON (e.id=sl_3.episode_id AND sl_3.language_id=?)
                LEFT JOIN
                    serie_version sv ON (sv.episode_id=e.id AND sv.website_id=?)
                WHERE
                    e.serie_id = ? AND
                    e.season_id = ?
                GROUP BY
                    e.id
            `;

            const params = [
                currentLanguage.language_locale_id,
                currentLanguage.language_id,
                defaultLanguage.language_id,
                website.id,
                serie.id,
                season.id
            ];

            dbConnection.query(query, params, (err, episodes) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                // Asignar links a cada una de las temporadas
                for (const episode of episodes) {
                    episode.link = getEpisodeLink(website, currentLanguage, {
                        ...serie,
                        ...season,
                        ...episode
                    });
                    
                }

                season.episodes = episodes;

                return cb(null, dbConnection, season);
            });
        },
        // Obtener listado de actores de la temporada
        (dbConnection, season, cb) => {
            const query = `
                SELECT
                    p.id,
                    p.hash,
                    sp.\`character\`,
                    p.name,
                    p.profile_path,
                    p.gender_id,
                    sp.\`order\`,
                    sp.guest
                FROM
                    serie_performer sp
                JOIN
                    person p ON sp.person_id = p.id
                WHERE
                    sp.season_id=?
                GROUP BY
                    p.id
                ORDER BY
                    sp.\`order\` ASC
            `;

            dbConnection.query(query, [season.id], (err, results) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                for (const performer of results) {
                    performer.link = getPersonLink(website, currentLanguage, performer);
                }

                const performers = results.filter((p) => p.guest === 0);
                const guests = results.filter((p) => p.guest === 1);

                season.performers = performers;
                season.guests = guests;

                return cb(null, dbConnection, season);
            });
        },
        // Obtener crew de la temporada
        (dbConnection, season, cb) => {
            const query = `
                SELECT
                    p.id,
                    p.name,
                    p.hash,
                    p.profile_path,
                    p.gender_id,
                    sc.job,
                    sc.department
                FROM
                    serie_crew sc
                JOIN
                    person p ON sc.person_id = p.id
                WHERE
                    sc.season_id=?
                GROUP BY
                    p.id,
                    sc.department
            `;

            dbConnection.query(query, [season.id], (err, crew) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                for (const c of crew) {
                    c.link = getPersonLink(website, currentLanguage, c);
                }

                season.crew = crew.filter((c) => c.job !== 'Director');
                season.directors = crew.filter((c) => c.job === 'Director');

                return cb(null, dbConnection, season);
            });
        },
        // Obtener videos de la temporada
        (dbConnection, season, cb) => {
            const query = `
                SELECT
                    l.name language_name,
                    l.iso language_iso,
                    vm.name,
                    vm.site,
                    vm.key,
                    vm.size,
                    vm.type
                FROM
                    serie_video sv
                JOIN
                    video_media vm ON sv.video_media_id=vm.id
                JOIN
                    language l ON vm.language_id=l.id
                WHERE
                    sv.season_id = ?
            `;

            dbConnection.query(query, [season.id], (err, videos) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                let seasonVideos = videos;
                if (!seasonVideos.length) {
                    seasonVideos = serie.videos;
                }

                season.videos = seasonVideos;

                return cb(null, dbConnection, season);
            });
        },
        // Genera el link
        (dbConnection, season, cb) => {
            season.link = getLink(website, currentLanguage, {
                ...serie,
                ...season
            });

            return cb(null, dbConnection, season);
        },
    ], (err, dbConnection, season) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, season);
    });
};