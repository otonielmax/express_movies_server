'use strict';
const linkHelper = require('../../../../helpers/link');

/**
 * Genera el link de una season
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    data                - Objeto con la informacion a utilizar en los placeholder
 */
module.exports = (website, currentLanguage, data) => {
    const route = website.routes.find((route) => route.name === 'season');
    
    /**Si la ruta no está definida, devolvemos null */
    if (!route) {
        return null;
    }

    const linkPrefix = linkHelper.getLanguagePrefix(website, currentLanguage);

    const routeValues = data.version ? route.versionValues : route.values;
    const defaultLink = routeValues[currentLanguage.language_locale_id] || routeValues.default;

    let link = defaultLink;
    for (let key in data) {
        link = link.replace(new RegExp(`:${key}`, 'g'), data[key]);
    }

    return linkPrefix + link;
};