'use strict';
const linkHelper = require('../../../../helpers/link');

/**
 * @todo es lo mismo que el "getLink" de movies.. separar en una carpeta "common"
 * Genera el link de un genero
 * 
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    genre               - Objecto del genero
 */
module.exports = (website, currentLanguage, genre) => {
    const route = website.routes.find((route) => route.name === 'genre');
    const linkPrefix = linkHelper.getLanguagePrefix(website, currentLanguage);
    const defaultLink = route.values[currentLanguage.language_locale_id] || route.values.default;

    let link = defaultLink;
    for (let key in genre) {
        link = link.replace(new RegExp(`:${key}`, 'g'), genre[key]);
    }

    return linkPrefix + link;
};