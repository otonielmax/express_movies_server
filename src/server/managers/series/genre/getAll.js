'use strict';
const languageHelper = require('../../../../helpers/language');
const utils = require('../../../../helpers/utils');
const cacheService = require('../../../../services/cache');
const dbService = require('../../../../services/db');
const getLink = require('./getLink');

/**
 * @todo es lo mismo que el de movies, se puede separar en una carpeta "common"
 * Obtiene el listado de generos
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 */
const getAllGenres = (website, currentLanguage, defaultLanguage, callback) => {
    const query = `
        SELECT
            g.id,
            IFNULL(gl_1.name, IFNULL(gl_2.name, gl_3.name)) name,
            IFNULL(gl_1.hash, IFNULL(gl_2.hash, gl_3.hash)) hash
        FROM
            genre g
        LEFT JOIN
            genre_language gl_1 ON (g.id = gl_1.genre_id AND gl_1.language_id = ?)
        LEFT JOIN
            genre_language gl_2 ON (g.id = gl_2.genre_id AND gl_2.language_id = ?)
        LEFT JOIN
            genre_language gl_3 ON (g.id = gl_3.genre_id AND gl_3.language_id = ?)
        WHERE
            g.series = 1
        GROUP BY
            g.id
        ORDER BY
            name ASC
    `;
    const fallbackLanguageId = languageHelper.getId('en');
    const params = [
        currentLanguage.language_id,
        defaultLanguage.language_id,
        fallbackLanguageId
    ];

    dbService.query(query, params, (err, genres) => {
        if (err) {
            return callback(err);
        }

        if (genres.length) {
            // Asignar links a cada una de las categorias
            for (const genre of genres) {
                genre.link = getLink(website, currentLanguage, genre);
            }
        }

        return callback(null, genres);
    });
};

/**
 * Obtiene el listado de generos, utilizando el cache
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 */
module.exports = (website, currentLanguage, defaultLanguage, callback) => {
    const websiteId = website.id;
    const currentLocaleId = currentLanguage.language_locale_id;
    const key = `${websiteId}_${currentLocaleId}_SERIE_GENRE_LIST`;
    const composedFn = utils.compose(getAllGenres, website, currentLanguage, defaultLanguage);

    const cacheOptions = {
        localExpireTime: 60 * 10,
        storageExpireTime: 60 * 60 * 6, // Guardo 6 horas en Redis ya que esto no cambia nunca
        stringify: true,
    };
    cacheService.getMaster(key, cacheOptions, composedFn, (err, genres) => {
        return callback(err, genres);
    });
};