'use strict';
const async = require('async');
const squel = require('squel');

const getSerieList = require('./getSerieList');
const dbService = require('../../../../services/db');
const galleryHelper = require('../../../../helpers/gallery');

/**
 * Obtiene la página solicitada
 * @param   {Object}        website             - Configuracion del sitio
 * @param   {Object}        currentLanguage     - Lenguaje actual
 * @param   {Object}        defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {Object}        params              - Objeto con parametros para filtrar la query
 * @param   {Number}        params.pageId       - Número de página actual
 * @param   {Number}        params.pageLimit    - Cantidad de items por página
 * @param   {String|Array}  params.galleryType  - Tipo de galeria: popular, top_rated, on_air
 * @param   {Function}      callback 
 */
module.exports = (website, currentLanguage, defaultLanguage, params, callback) => {
    const offset = (params.pageLimit * (params.pageId - 1));
    const columnNames = [];

    // Se puede enviar un array de gallerias a usar, o solo una en conreto.
    if (Array.isArray(params.galleryType)) {
        for (const gallery of params.galleryType) {
            columnNames.push(galleryHelper.getColumName(gallery));
        }
    } else {
        columnNames.push(galleryHelper.getColumName(params.galleryType));
    }

    async.waterfall([
        // Obtener conexion
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Primero elegir los IDS de las series. Es mas óptimo primero filtrar los IDS y luego obtener los rows por IDS
        (dbConnection, cb) => {
            const galleryWheres = squel.expr();
            const query = squel.select()
                .field('s.id', 'id')
                .from('serie', 's')
                .limit(params.pageLimit)
                .offset(offset)
                .group('s.id');

            // Arma los filtros "or" por cada columna: WHERE (gallery1 IS NOT NUL OR galleryN IS NOT NULL)
            for (const column of columnNames) {
                query.order(`s.${column}`, 'ASC');
                galleryWheres.or(`s.${column} IS NOT NULL`);
            }

            query.where(galleryWheres);

            const parsedQuery = query.toParam();

            dbService.query(parsedQuery.text, parsedQuery.values, (err, result) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                const ids = result.map((obj) => obj.id);

                return cb(null, dbConnection, ids);
            });
        },
        // Obtener series en base a los IDs
        (dbConnection, ids, cb) => {
            if (!ids || !ids.length) {
                return cb(null, dbConnection, []);
            }

            // Agrega los ids obtenidos antes, para obtener info de estos ids
            params.serieIds = ids;

            getSerieList(dbConnection, website, currentLanguage, defaultLanguage, params, (err, series) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                return cb(null, dbConnection, series);
            });
        },
    ], (err, dbConnection, series) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, series);
    });
};