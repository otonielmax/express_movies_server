'use strict';
const linkHelper = require('../../../../helpers/link');

/**
 * @todo es lo mismo que el "getLink" de movies.. separar en una carpeta "common"
 * Genera el link de una serie
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    serie               - Objeto con informacion de la serie
 */
module.exports = (website, currentLanguage, serie) => {
    const route = website.routes.find((route) => route.name === 'serie');
    const linkPrefix = linkHelper.getLanguagePrefix(website, currentLanguage);

    const routeValues = serie.version ? route.versionValues : route.values;
    const defaultLink = routeValues[currentLanguage.language_locale_id] || routeValues.default;

    let link = defaultLink;
    for (let key in serie) {
        link = link.replace(new RegExp(`:${key}`, 'g'), serie[key]);
    }

    return linkPrefix + link;
};