'use strict';
const async = require('async');

const utils = require('../../../../helpers/utils');
const cacheService = require('../../../../services/cache');
const dbService = require('../../../../services/db');
const getSerieList = require('./getSerieList');

/**
 * @todo es lo mismo que el "getFeatured" de movies.. separar en una carpeta "common"
 * Obtiene las series destacadas (mayor popularidad)
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 */
const getFeatured = (website, currentLanguage, defaultLanguage, callback) => {
    async.waterfall([
        // Obtener conexion
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Primero elegir los IDS, es mas óptimo primer filtrar los IDS y luego obtener las películas por IDS
        (dbConnection, cb) => {
            // Ordena las películas por popularidad
            const query = `
                SELECT
                    id
                FROM
                    serie
                WHERE
                    gallery_popular_position IS NOT NULL
                ORDER BY
                    gallery_popular_position ASC
                LIMIT
                    ?
            `;
            const limit = website.home.limit.featured;

            dbConnection.query(query, [limit], (err, result) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                const ids = result.map((obj) => obj.id);

                return cb(null, dbConnection, ids);
            });
        },
        // Obtener series en base a los ID
        (dbConnection, ids, cb) => {
            // Si no hay series, sigue
            if (!ids || !ids.length) {
                return cb(null, dbConnection, []);
            }

            const params = {
                serieIds: ids,
                galleryType: 'popular'
            };

            getSerieList(dbConnection, website, currentLanguage, defaultLanguage, params, (err, series) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                return cb(null, dbConnection, series);
            });
        },
    ], (err, dbConnection, series) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, series);
    });
};

/**
 * Obtiene las series destacadas (mayor popularidad), utilizando el cache
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 */
module.exports = (website, currentLanguage, defaultLanguage, callback) => {
    const websiteId = website.id;
    const currentLocaleId = currentLanguage.language_locale_id;
    const key = `${websiteId}_${currentLocaleId}_FEATURED_SERIES`;

    const cacheOptions = {
        localExpireTime: 60 * 1,
        storageExpireTime: 60 * 60,
        stringify: true,
    };
    const composedFn = utils.compose(getFeatured, website, currentLanguage, defaultLanguage);

    cacheService.getMaster(key, cacheOptions, composedFn, (err, movies) => {
        return callback(err, movies);
    });
};