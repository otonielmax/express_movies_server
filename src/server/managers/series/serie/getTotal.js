'use strict';
const squel = require('squel');

const dbService = require('../../../../services/db');
const galleryHelper = require('../../../../helpers/gallery');

/**
 * Cuenta la cantidad de items total en la base de datos, para poder calcular las páginas totales
 * @param {Object} params - Objeto con parametros para filtrar la query
 * @param {String|Array} params.galleryType - Tipo de galeria: popular, top_rated, on_air
 * @param   {Function}  callback        - Funcion para controlar la respuesta
 */
module.exports = (params, callback) => {
    const columnNames = [];

    // Se puede enviar un array de gallerias a usar, o solo una en conreto.
    if (Array.isArray(params.galleryType)) {
        for (const gallery of params.galleryType) {
            columnNames.push(galleryHelper.getColumName(gallery));
        }
    } else {
        columnNames.push(galleryHelper.getColumName(params.galleryType));
    }

    const galleryWheres = squel.expr();
    const query = squel.select()
        .field('COUNT(DISTINCT s.id)', 'count')
        .from('serie', 's');

    // Arma los filtros "or" por cada columna: WHERE (gallery1 IS NOT NUL OR galleryN IS NOT NULL)
    for (const column of columnNames) {
        query.order(`s.${column}`, 'ASC');
        galleryWheres.or(`s.${column} IS NOT NULL`);
    }

    query.where(galleryWheres);

    const parsedQuery = query.toParam();

    dbService.query(parsedQuery.text, parsedQuery.values, (err, result) => {
        if (err) {
            return callback(err);
        }

        const count = result[0] ? result[0].count : 0;

        return callback(null, count);
    });
};