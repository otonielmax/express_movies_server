'use strict';
const async = require('async');
const squel = require('squel');

const languageHelper = require('../../../../helpers/language');
const galleryHelper = require('../../../../helpers/gallery');
const getAllGenres = require('../genre/getAll');
const getLink = require('./getLink');

/**
 * Obtiene un listado de series en base a un listado de IDS
 * @param   {Object}    dbConnection        - Objecto de conexion a la DB
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {Object}    params              - Objeto con parametros para filtrar la query
 * @param   {Array}     params.serieIds     - Arreglo de IDs de peliculas a buscar
 * @param   {String}    params.galleryType  - Tipo de galeria: popular, top_rated, on_air
 */
module.exports = (dbConnection, website, currentLanguage, defaultLanguage, params, callback) => {
    if (!params.serieIds || !params.serieIds.length) {
        return callback(null, []);
    }

    const columnNames = [];

    // Se puede enviar un array de gallerias a usar, o solo una en conreto.
    if (Array.isArray(params.galleryType)) {
        for (const gallery of params.galleryType) {
            columnNames.push(galleryHelper.getColumName(gallery));
        }
    } else {
        columnNames.push(galleryHelper.getColumName(params.galleryType));
    }
    
    async.waterfall([
        // Obtener informacion de las series
        (cb) => {
            const galleryWheres = squel.expr();
            const fallbackLanguage = languageHelper.getId('en');
            const query = squel.select()
                .field('s.id', 'id')
                .field(`IFNULL(sh_1.hash,
                    IFNULL(sh_2.hash,
                        IFNULL(sh_3.hash,
                            sh_4.hash
                        )
                    )
                )`, 'hash')
                .field(`IFNULL(sl_1.title, 
                    IFNULL(sl_2.title, 
                        IFNULL(sl_3.title, 
                            s.title
                        )
                    )
                )`, 'title')
                .field(`IFNULL(slw_1.overview,
                    IFNULL(sl_1.overview,
                        IFNULL(slw_2.overview,
                            IFNULL(sl_2.overview,
                                IFNULL(slw_3.overview,
                                    IFNULL(sl_3.overview,
                                        s.overview
                                    )
                                )
                            )
                        )
                    )
                )`, 'overview')
                .field('s.original_title', 'original_title')
                .field('s.popularity', 'popularity')
                .field('s.gallery_popular_position', 'gallery_popular_position')
                .field('s.poster_path', 'poster_path')
                .field('s.backdrop_path', 'backdrop_path')
                .field('s.imdb_id', 'imdb_id')
                .field('s.facebook_id', 'facebook_id')
                .field('s.instagram_id', 'instagram_id')
                .field('s.twitter_id', 'twitter_id')
                .field('s.vote_count', 'vote_count')
                .field('s.vote_average', 'vote_average')
                .field('s.serie_status_id', 'serie_status_id')
                .field('sv.version', 'version')
                .field('ss.name', 'serie_status_name')
                .field('s.serie_type_id', 'serie_type_id')
                .field('st.name', 'serie_type_name')
                .field('s.first_air_date', 'first_air_date')
                .field('YEAR(s.first_air_date)', 'first_air_date_year')
                .field('MONTH(s.first_air_date)', 'first_air_date_month')
                .field('DAY(s.first_air_date)', 'first_air_date_da')
                .from('serie', 's')
                .left_join('serie_type', 'st', 's.serie_type_id = st.id')
                .left_join('serie_status', 'ss', 's.serie_status_id = ss.id')
                // Hash
                .left_join('serie_hash', 'sh_1',
                    squel.expr()
                        .and('s.id = sh_1.serie_id')
                        .and('sh_1.language_locale_id = ?', currentLanguage.language_locale_id)
                )
                .left_join('serie_hash', 'sh_2',
                    squel.expr()
                        .and('s.id = sh_2.serie_id')
                        .and('sh_2.language_id = ?', currentLanguage.language_id)
                )
                .left_join('serie_hash', 'sh_3',
                    squel.expr()
                        .and('s.id = sh_3.serie_id')
                        .and('sh_3.language_id = ?', defaultLanguage.language_id)
                )
                .left_join('serie_hash', 'sh_4',
                    squel.expr()
                        .and('s.id = sh_4.serie_id')
                        .and('sh_4.language_id = ?', fallbackLanguage)
                )
                // Languages
                .left_join('serie_language', 'sl_1',
                    squel.expr()
                        .and('s.id = sl_1.serie_id')
                        .and('sl_1.language_locale_id = ?', currentLanguage.language_locale_id)
                )
                .left_join('serie_language_website', 'slw_1',
                    squel.expr()
                        .and('sl_1.id = slw_1.serie_language_id')
                        .and('slw_1.website_id = ?', website.id)
                )
                .left_join('serie_language', 'sl_2',
                    squel.expr()
                        .and('s.id = sl_2.serie_id')
                        .and('sl_2.language_id = ?', currentLanguage.language_id)
                )
                .left_join('serie_language_website', 'slw_2',
                    squel.expr()
                        .and('sl_2.id = slw_2.serie_language_id')
                        .and('slw_2.website_id = ?', website.id)
                )
                .left_join('serie_language', 'sl_3',
                    squel.expr()
                        .and('s.id = sl_3.serie_id')
                        .and('sl_3.language_id = ?', defaultLanguage.language_id)
                )
                .left_join('serie_language_website', 'slw_3',
                    squel.expr()
                        .and('sl_3.id = slw_3.serie_language_id')
                        .and('slw_3.website_id = ?', website.id)
                )
                // Version
                .left_join('serie_version', 'sv',
                    squel.expr()
                        .and('s.id = sv.serie_id')
                        .and('sv.website_id = ?', website.id)
                )
                .where('s.id IN ?', params.serieIds)
                .group('s.id');

            // Arma los filtros "or" por cada columna: WHERE (gallery1 IS NOT NUL OR galleryN IS NOT NULL)
            for (const column of columnNames) {
                query.order(`s.${column}`, 'ASC');
                galleryWheres.or(`s.${column} IS NOT NULL`);
            }

            query.where(galleryWheres);

            const parsedQuery = query.toParam();

            dbConnection.query(parsedQuery.text, parsedQuery.values, (err, result) => {
                if (err) {
                    return cb(err);
                }

                if (result.length) {
                    // Asignar links a cada una de las series
                    for (const row of result) {
                        row.link = getLink(website, currentLanguage, row);
                        row.genres = [];
                    }
                }

                return cb(null, result);
            });
        },
        // Obtener listado de generos
        (series, cb) => {
            getAllGenres(website, currentLanguage, defaultLanguage, (err, genres) => {
                return cb(err, series, genres);
            });
        },
        // Obtener los generos de todas las series
        (series, genres, cb) => {
            const query = `
                SELECT
                    genre_id,
                    serie_id
                FROM
                    serie_genre
                WHERE
                    serie_id IN (?)
            `;

            dbConnection.query(query, [params.serieIds], (err, serieGenres) => {
                if (err) {
                    return cb(err);
                }

                for (const serieGenre of serieGenres) {
                    const serie = series.find((m) => m.id === serieGenre.serie_id);

                    if (serie) {
                        if (!serie.genres) {
                            serie.genres = [];
                        }

                        const genre = genres.find((g) => g.id === serieGenre.genre_id);
                        if (genre) {
                            serie.genres.push(genre);
                        }
                    }
                }

                return cb(null, series);
            });
        },
    ], (err, series) => {
        return callback(err, series);
    });
};