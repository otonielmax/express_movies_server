'use strict';
const async = require('async');
const utils = require('../../../../helpers/utils');
const languageHelper = require('../../../../helpers/language');
const hashHelper = require('../../../../helpers/hash');
const cacheService = require('../../../../services/cache');
const dbService = require('../../../../services/db');
const getGenreLink = require('../genre/getLink');
const getPersonLink = require('../../movies/person/getLink');
const getSerieLink = require('./getLink');
const getSeasonLink = require('../season/getLink');
const getSerieList = require('./getSerieList');
const getLink = require('./getLink');
const getEpisodeLink = require('../episode/getLink');

/**
 * Obtiene todos los datos basicos de una serie en concreto como las temporadas
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 * @param   {String}    serieHash           - Hash de la serie
 * @param   {Function}  callback 
 */
module.exports = (website, currentLanguage, defaultLanguage, serieHash, callback) => {
    const key = `${website.id}_${currentLanguage.language_locale_id}_SERIE_${serieHash}`.toUpperCase();
    const composedFn = utils.compose(getSerie, website, currentLanguage, defaultLanguage, serieHash);
    const cacheOptions = {
        localExpireTime: 30,
        storageExpireTime: 60 * 60,
        stringify: true,
    };

    cacheService.getMaster(key, cacheOptions, composedFn, (err, serie) => {
        if (err) {
            return callback(err);
        }

        return callback(null, serie);
    });
};

const getSerie = (website, currentLanguage, defaultLanguage, serieHash, callback) => {
    async.waterfall([
        // Obtener conexion a la base de datos
        (cb) => {
            dbService.getConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        // Elegir serie en base al hash.
        (dbConnection, cb) => {
            const query = `
                SELECT
                    serie_id
                FROM
                    serie_hash
                WHERE
                    hash = ?
            `;

            dbConnection.query(query, [serieHash], (err, serie) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!serie[0]) {
                    return cb({
                        message: 'Serie not found',
                        code: 'NOT_FOUND'
                    }, dbConnection);
                }

                return cb(null, dbConnection, serie[0].serie_id);
            });
        },
        // Obtiene los datos basicos en base al ID
        (dbConnection, serieId, cb) => {
            const query = `
                SELECT
                    s.id,
                    IFNULL(sl_1.title, 
                        IFNULL(sl_2.title, 
                            IFNULL(sl_3.title, 
                                s.title
                            )
                        )
                    ) title,
                    IFNULL(slw_1.overview,
                        IFNULL(sl_1.overview,
                            IFNULL(slw_2.overview,
                                IFNULL(sl_2.overview,
                                    IFNULL(slw_3.overview,
                                        IFNULL(sl_3.overview,
                                            s.overview
                                        )
                                    )
                                )
                            )
                        )
                    ) overview,
                    s.original_title,
                    s.popularity,
                    s.poster_path,
                    s.backdrop_path,
                    s.imdb_id,
                    s.facebook_id,
                    s.instagram_id,
                    s.twitter_id,
                    s.vote_count,
                    s.vote_average,
                    s.serie_status_id,
                    ss.name serie_status_name,
                    s.serie_type_id,
                    st.name serie_type_name,
                    s.number_of_seasons,
                    s.number_of_episodes,
                    s.in_production,
                    sv.version,
                    IFNULL(sl_1.homepage, IFNULL(sl_2.homepage, s.homepage)) homepage,
                    s.first_air_date,
                    YEAR(s.first_air_date) first_air_date_year,
                    MONTH(s.first_air_date) first_air_date_month,
                    DAY(s.first_air_date) first_air_date_day
                FROM
                    serie s
                JOIN
                    serie_type st ON (s.serie_type_id = st.id)
                JOIN
                    serie_status ss ON (s.serie_status_id = ss.id)
                LEFT JOIN
                    serie_language sl_1 ON (s.id=sl_1.serie_id AND sl_1.language_locale_id=?)
                LEFT JOIN
                    serie_language_website slw_1 ON (sl_1.id=slw_1.serie_language_id AND slw_1.website_id=?)
                LEFT JOIN
                    serie_language sl_2 ON (s.id=sl_2.serie_id AND sl_2.language_id=?)
                LEFT JOIN
                    serie_language_website slw_2 ON (sl_2.id=slw_2.serie_language_id AND slw_2.website_id=?)
                LEFT JOIN
                    serie_language sl_3 ON (s.id=sl_3.serie_id AND sl_3.language_id=?)
                LEFT JOIN
                    serie_language_website slw_3 ON (sl_3.id=slw_3.serie_language_id AND slw_3.website_id=?)
                LEFT JOIN
                    serie_version sv ON (sv.serie_id=s.id AND sv.website_id = ?)
                WHERE
                    s.id = ?
                GROUP BY
                    s.id
            `;

            // @todo agregar articulos
            const params = [
                currentLanguage.language_locale_id,
                website.id,
                currentLanguage.language_id,
                website.id,
                defaultLanguage.language_id,
                website.id,
                website.id,
                serieId
            ];

            dbConnection.query(query, params, (err, serie) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!serie[0]) {
                    return cb(true, dbConnection);
                }

                // Guardo el hash que se uso, para reutilizarlo luego en los "getLink"
                serie[0].hash = serieHash;

                return cb(null, dbConnection, serie[0]);
            });
        },
        // Obtener listado de hashes de la serie
        (dbConnection, serie, cb) => {
            const query = `
                SELECT
                    sh.hash,
                    l1.id language_id,
                    l2.id language_locale_id
                FROM
                    serie_hash sh
                JOIN
                    language l1 ON sh.language_id = l1.id
                LEFT JOIN
                    language l2 ON sh.language_locale_id = l2.id
                WHERE
                    sh.serie_id = ?
            `;

            dbConnection.query(query, [serie.id], (err, hashes) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                // Obtengo el hash mas optimo para el lenguaje del usuario
                let preferredHash = hashHelper.getBestHash(hashes, currentLanguage, defaultLanguage);

                if (website.language.multiLanguage) {
                    // Si el sitio es multilenguaje, el hash tiene que coincidir con el optimo
                    if (preferredHash.hash !== serieHash) {
                        const newLink = website.host + getSerieLink(website, currentLanguage, {
                            ...serie,
                            hash: preferredHash.hash,
                        });

                        return cb({
                            redirect: true,
                            redirect_to: newLink,
                        }, dbConnection);
                    }
                }

                let alternateLangs = [];

                for (let language of website.language.available) {
                    let bestHash = hashHelper.getBestHash(hashes, language, defaultLanguage);
                    const langId = language.show_locale ? language.language_locale_id : language.language_id;

                    const hash = {
                        default: language.default ? true : false,
                        hreflang: languageHelper.getLanguage(langId).iso,
                        href: website.host + getSerieLink(website, language, {
                            ...serie,
                            hash: bestHash.hash,
                        }),
                        language_name: languageHelper.getLanguage(langId).name,
                    };

                    alternateLangs.push(hash);
                }

                // Ordeno primero el default
                alternateLangs = alternateLangs.sort((a, b) => {
                    return b.default - a.default;
                });

                serie.alternate_lang = alternateLangs;

                return cb(null, dbConnection, serie);
            });
        },
        // Obtener listado de generos de la serie
        (dbConnection, serie, cb) => {
            const query = `
                SELECT
                    g.id,
                    IFNULL(gl_1.name, IFNULL(gl_2.name, gl_3.name)) name,
                    IFNULL(gl_1.hash, IFNULL(gl_2.hash, gl_3.hash)) hash
                FROM
                    genre g
                JOIN
                    serie_genre sg ON g.id=sg.genre_id
                LEFT JOIN
                    genre_language gl_1 ON (g.id = gl_1.genre_id AND gl_1.language_id = ?)
                LEFT JOIN
                    genre_language gl_2 ON (g.id = gl_2.genre_id AND gl_2.language_id = ?)
                LEFT JOIN
                    genre_language gl_3 ON (g.id = gl_3.genre_id AND gl_3.language_id = ?)
                WHERE
                    sg.serie_id = ?
                GROUP BY
                    g.id
                ORDER BY
                    g.name ASC
            `;

            const fallbackLanguageId = languageHelper.getId('en');
            const params = [
                currentLanguage.language_id,
                defaultLanguage.language_id,
                fallbackLanguageId,
                serie.id
            ];

            dbConnection.query(query, params, (err, genres) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                serie.genres = genres;

                // Generar los links
                for (let genre of genres) {
                    genre.link = getGenreLink(website, currentLanguage, genre);
                }

                return cb(null, dbConnection, serie);
            });
        },
        // Obtener listado de temporadas de la serie
        (dbConnection, serie, cb) => {
            const query = `
                SELECT
                    se.id,
                    se.season_number,
                    se.title,
                    se.overview,
                    se.poster_path,
                    se.air_date,
                    sv.version
                FROM
                    season se
                LEFT JOIN
                    serie_version sv ON (sv.season_id=se.id AND sv.website_id=?)
                WHERE
                    se.serie_id = ?
                ORDER BY
                    se.season_number ASC
            `;

            dbConnection.query(query, [website.id, serie.id], (err, seasons) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                // Asignar links a cada una de las temporadas
                for (const season of seasons) {
                    season.link = getSeasonLink(website, currentLanguage, {
                        ...serie,
                        ...season
                    });
                }

                serie.seasons = seasons;

                return cb(null, dbConnection, serie);
            });
        },
        /*
        * Obtener arreglo minimalista de episodios dentro de de cada temporada
        * de la serie.
        */
        (dbConnection, serie, cb)=>{
            const query = `
                SELECT 
                    ep.episode_number,
                    ep.season_id,
                    sv.version
                FROM
                    episode ep
                LEFT JOIN serie_version sv
                    ON (ep.id = sv.episode_id AND sv.website_id=?)
                WHERE ep.serie_id = ?
                ORDER BY ep.episode_number ASC;
            `;
            dbConnection.query(query, [website.id, serie.id], (err, results) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                /**
                 * Hashtable con los índices de las temporadas atado al ID
                 * de las temporadas para poder referenciarlos de manera
                 * más rápida: {season_id: index_en_arreglo}
                 */
                const seasonsHash = {};
                for (const [index, season] of serie.seasons.entries()) {
                    seasonsHash[season.id] = index;
                }

                for (const episode of results) {
                    const index = seasonsHash[episode.season_id];
                    if (!serie.seasons[index].episodes) {
                        serie.seasons[index].episodes = [];
                    }
                    serie.seasons[index].episodes.push({
                        episode_number:episode.episode_number, 
                        link: getEpisodeLink(website, currentLanguage, {
                            ...serie,
                            ...serie.seasons[index],
                            ...episode
                        })
                    });
                }

                return cb(null, dbConnection, serie);
            });
        },
        // Obtener listado de actores de la serie
        (dbConnection, serie, cb) => {
            const query = `
                SELECT
                    p.id,
                    p.hash,
                    sp.\`character\`,
                    p.name,
                    p.profile_path,
                    p.gender_id,
                    sp.\`order\`,
                    sp.guest
                FROM
                    serie_performer sp
                JOIN
                    person p ON sp.person_id = p.id
                WHERE
                    sp.serie_id=?
                GROUP BY
                    p.id
                ORDER BY
                    sp.\`order\` ASC
            `;

            dbConnection.query(query, [serie.id], (err, results) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                for (const performer of results) {
                    performer.link = getPersonLink(website, currentLanguage, performer);
                }

                const performers = results.filter((p) => p.guest === 0);
                const guests = results.filter((p) => p.guest === 1);

                serie.performers = performers;
                serie.guests = guests;

                return cb(null, dbConnection, serie);
            });
        },
        // Obtener crew
        (dbConnection, serie, cb) => {
            const query = `
                SELECT
                    p.id,
                    p.name,
                    p.hash,
                    p.profile_path,
                    p.gender_id,
                    sc.job,
                    sc.department
                FROM
                    serie_crew sc
                JOIN
                    person p ON sc.person_id = p.id
                WHERE
                    sc.serie_id=?
                GROUP BY
                    p.id,
                    sc.department
            `;

            dbConnection.query(query, [serie.id], (err, crew) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                for (const c of crew) {
                    c.link = getPersonLink(website, currentLanguage, c);
                }

                serie.crew = crew.filter((c) => c.job !== 'Director');
                serie.directors = crew.filter((c) => c.job === 'Director');

                return cb(null, dbConnection, serie);

            });
        },
        // Obtener las keywords
        (dbConnection, serie, cb) => {
            const query = `
                SELECT
                    k.id,
                    k.name
                FROM
                    keyword k
                JOIN
                    serie_keyword sk ON sk.keyword_id = k.id
                WHERE
                    sk.serie_id=?
            `;

            dbConnection.query(query, [serie.id], (err, result) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                serie.keywords = result;

                return cb(null, dbConnection, serie);

            });
        },
        // Obtener videos de la serie
        (dbConnection, serie, cb) => {
            const query = `
                SELECT
                    l.name language_name,
                    l.iso language_iso,
                    vm.name,
                    vm.site,
                    vm.key,
                    vm.size,
                    vm.type
                FROM
                    serie_video sv
                JOIN
                    video_media vm ON sv.video_media_id=vm.id
                JOIN
                    language l ON vm.language_id=l.id
                WHERE
                    sv.serie_id = ?
            `;

            dbConnection.query(query, [serie.id], (err, videos) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!videos.length) {
                    videos.push({
                        language_name: 'English',
                        iso: 'en',
                        name: 'Trailer',
                        site: 'YouTube',
                        key: 'uYSo9vMrHjw',
                        size: 1080,
                        type: 'Trailer',
                    });
                }

                serie.videos = videos;

                return cb(null, dbConnection, serie);
            });
        },
        // Genera el link
        (dbConnection, serie, cb) => {
            serie.link = getLink(website, currentLanguage, serie);

            return cb(null, dbConnection, serie);
        },
        // Obtener IDs de series relacionadas
        (dbConnection, serie, cb) => {
            if (!serie.genres || !serie.genres.length) {
                return cb(null, dbConnection, serie, null);
            }

            const query = `
                SELECT
                    s.id
                FROM
                    serie_genre sg
                JOIN
                    serie s ON sg.serie_id = s.id
                WHERE
                    s.gallery_popular_position IS NOT NULL AND
                    sg.genre_id IN (?) AND
                    sg.serie_id != ?
                GROUP BY
                    sg.serie_id
                ORDER BY
                    s.gallery_popular_position ASC
                LIMIT
                    10
            `;

            const genres = serie.genres.map((obj) => obj.id);

            dbConnection.query(query, [genres, serie.id], (err, result) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                const relatedSeriesIds = result.map((obj) => obj.id);

                return cb(null, dbConnection, serie, relatedSeriesIds);
            });
        },
        // Obtener series relacionadas
        (dbConnection, serie, relatedSeriesIds, cb) => {
            if (!relatedSeriesIds || !relatedSeriesIds.length) {
                serie.related_series = [];
                return cb(null, dbConnection, serie);
            }

            const params = {
                serieIds: relatedSeriesIds,
                galleryType: 'popular'
            };

            getSerieList(dbConnection, website, currentLanguage, defaultLanguage, params, (err, related) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                serie.related_series = related;

                return cb(null, dbConnection, serie);
            });
        }
    ], (err, dbConnection, serie) => {
        if (dbConnection) {
            dbConnection.release();
        }

        return callback(err, serie);
    });
};