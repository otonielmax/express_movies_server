'use strict';

/**
 * Decide si la version actual de la URL es correcta o no
 * 
 * @param   {Object}    pathVersion             - Version en los parametros de la URL
 * @param   {Object}    itemVersion             - Version del item para un website en particular
 */
module.exports = (itemVersion, pathVersion) => {
    // Si tiene version y no hay en la URL
    if (itemVersion && !pathVersion) {
        return true;
    }
    // Si no tiene version y hay en la URL
    if (!itemVersion && pathVersion) {
        return true;
    }
    // Si lo que hay en la URL no es la version que deberia tener
    if (itemVersion != pathVersion) {
        return true;
    }

    return false;
};