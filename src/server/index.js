const async = require('async');
const compress = require('compression');
const config = require('config');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const languageParser = require('accept-language-parser');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const { v4 } = require('uuid');
const path = require('path');

const { logger } = require('../logger');
const routesControllers = require('./routes');
const languageHelper = require('../helpers/language');
const linkHelper = require('../helpers/link');
const utils = require('../helpers/utils');
const locationMiddleware = require('./middlewares/location');
const crawlerMiddleware = require('./middlewares/crawler');

const noLanguagePrefixRoutes = ['sitemap', 'rss', 'ads', 'robots', 'login', 'signup', 'tmdb-image', 'dmca'];

// AMP prefix
const ampPrefix = '/amp';

module.exports = (website, callback) => {
    const express = require('express');
    const app = express();
    const hbs = require('hbs');
    const hbsHelpers = require('handlebars-helpers')();

    // Handlebars
    const hbsInstance = hbs.create();
    // Registrar partials (filter not defined values).
    const partials = [website.paths.partials, website.paths.sharedPartials].filter((p) => !!p);
    partials.map((p) => hbsInstance.registerPartials(p));
    // Registrar helpers de handlebars
    for (let key in hbsHelpers) {
        hbsInstance.registerHelper(key, hbsHelpers[key]);
    }
    // Registrar helpers custom
    hbsInstance.registerHelper('arrSlice', function (array, start, end) {
        if (!array || !Array.isArray(array)) {
            return [];
        }
        return array.slice(start, end);
    });
    hbsInstance.registerHelper('assign', function (varName, varValue, options) {
        if (!options.data.root) {
            options.data.root = {};
        }
        options.data.root[varName] = varValue;
    });
    // Solucion temporal para encontrar que sitio hace crashear el forEach
    hbsInstance.registerHelper('forEach', (array, options) => {
        const data = {
            ...options,
            _parent: options
        };
        let buffer = '';

        if (!array || !Array.isArray(array)) {
            const websiteName = website.name;
            logger.error(`#forEach mal utilizado en "${websiteName}"`, {
                website: websiteName,
                path: options.data.root.site.canonical
            });
            return buffer;
        }

        let len = array.length;
        let i = -1;
        while (++i < len) {
            let item = array[i];
            data.index = i;
            item.index = i + 1;
            item.total = len;
            item.isFirst = i === 0;
            item.isLast = i === (len - 1);
            buffer += options.fn(item, { data: data });
        }

        return buffer;
    });

    // Setear engine
    app.engine('hbs', hbsInstance.__express);
    app.set('view engine', 'hbs');

    // Setear cache control (importante para Cloudflare)
    app.use((_req, res, next) => {
        res.set('Cache-Control', 'public, max-age=3600, s-maxage=86400');
        return next();
    });

    // Sistema de cache de rutas. Solo en produccion
    if (config.ENVIRONMENT === 'production') {
        app.enable('view cache');
    }

    // Middleware de compresion
    app.use(compress());

    // Archivos estáticos
    app.use('/static/', express.static(website.paths.static));
    if (website.paths.sharedStatic) {
        app.use('/static-cdn/', express.static(website.paths.sharedStatic));
    }

    // Si es un archivo estático y no hiteo express, corta.
    app.use((req, res, next) => {
        const staticRegex = /^\/static(-cdn)?\//g;
        if (staticRegex.test(req.path)) {
            return res.status(404).send();
        }

        return next();
    });

    // Middleware de cookies
    app.use(cookieParser());

    // Middleware de Transaction ID
    app.use((req, res, next) => {
        req.transactionId = req.headers['x-transaction-id'] || v4();

        // Agregar al TransactionID el nombre del sitio, para facilitar debug
        const websiteString = `${website.name}#`;
        if (req.transactionId.indexOf(websiteString) !== 0) {
            req.transactionId = `${websiteString}${req.transactionId}`;
        }

        return next();
    });

    // eslint-disable-next-line no-unused-vars
    app.use((err, req, res, next) => {
        const { path, transactionId } = req;
        logger.error('Route crashed', { transactionId, path, err });
        res.status(500).send();
    });

    // Middleware de targeting
    app.use(locationMiddleware);

    // Middleware de comprobacion de crawlers
    app.use(crawlerMiddleware);

    // Log request y crear funcion de enviar error
    app.use((req, res, next) => {
        const { body, headers, params, path, query, transactionId } = req;
        logger.debug(`< [${req.method}] ${path}`, { transactionId, query, path, params, body, headers });

        res.sendError = (err, statusCode) => {
            logger.error(`< [${req.method}] ${path} `, { transactionId, err });
            let status;
            if (statusCode) {
                status = statusCode;
            } else if (err && err.code && err.code === 'NOT_FOUND') {
                status = 404;
            } else {
                // Por defecto usamos 502, para que Cloudflare active el Always Online
                status = 502;
            }
            return res.status(status).send();
        };

        return next();
    });

    app.use((req, res, next) => {
        // --------------------------------------------------------------------------
        // Agregar info adicional al request
        // --------------------------------------------------------------------------
        req.website = website;
        // Lugar donde guardar cosas para compilar al final
        res.data = {
            route: null,
            path: null,
            context: {
                location: req.location,
                is_crawler: req.isCrawler
            },
        };
        // --------------------------------------------------------------------------

        // --------------------------------------------------------------------------
        // Deteccion del idioma del usuario
        // --------------------------------------------------------------------------
        const defaultLanguage = website.language.available.find((l) => l.default);
        let languages = languageParser.parse(req.headers['Accept-Language'] || req.headers['accept-language']);
        let userLanguage = languages[0];
        let userLanguageId;
        let userLocaleId;
        if (userLanguage && userLanguage.code) {
            // Obtengo LanguageId
            userLanguage.code = userLanguage.code.toLowerCase();
            userLanguageId = languageHelper.getId(userLanguage.code);

            // Obtengo LocaleId
            if (userLanguage.region) {
                userLanguage.region = userLanguage.region.toUpperCase();
                let locale = userLanguage.region;

                // Para español solo tenemos es-ES y es-MX. Todo lo que no sea es-ES lo agrupo en latinoamerica.
                if (userLanguage.code === 'es') {
                    if (userLanguage.region !== 'ES') {
                        locale = 'MX';
                    }
                }

                // Obtengo el locale del usuario
                const iso = `${userLanguage.code}-${locale}`;
                userLocaleId = languageHelper.getId(iso);
            }
        }

        // Si no se pudo calcular alguno, utiliza el por defecto
        if (!userLanguageId) {
            userLanguageId = defaultLanguage.language_id;
            userLocaleId = defaultLanguage.language_locale_id;
        }
        // --------------------------------------------------------------------------

        // --------------------------------------------------------------------------
        // Deteccion del idioma optimo
        // --------------------------------------------------------------------------
        let preferredLanguage;

        // Primero intento agarrar el mismo locale que el usuario
        if (userLocaleId) {
            preferredLanguage = website.language.available.find((l) => l.language_locale_id && l.language_locale_id === userLocaleId);
        }

        // Si no existe, intento obtener el del idioma
        if (!preferredLanguage) {
            preferredLanguage = website.language.available.find((l) => l.language_id === userLanguageId);
        }

        // Como ultima opcion, agarra el por defecto de la pagina
        if (!preferredLanguage) {
            preferredLanguage = defaultLanguage;
        }
        // --------------------------------------------------------------------------

        // --------------------------------------------------------------------------
        // Agregar info adicional al contexto
        // --------------------------------------------------------------------------
        req.session = {};
        req.session.user_language = {
            language_id: userLanguageId,
            language_locale_id: userLocaleId,
            language_name: userLocaleId ? languageHelper.getLanguage(userLocaleId).name : 'Unknown',
        };
        req.session.default_language = defaultLanguage;
        req.session.preferred_language = preferredLanguage;

        return next();
    });

    // Control de sesion
    app.use((req, res, next) => {
        if (!req.cookies.session) {
            return next();
        }

        jwt.verify(req.cookies.session, config.SESSION_JWT_KEY, (err, decoded) => {
            if (err) {
                res.clearCookie('session');
                return next();
            }

            // Si la session no es de este sitio
            if (decoded.website_id !== website.id) {
                res.clearCookie('session');
                return next();
            }

            req.session.user = decoded;

            return next();
        });
    });

    /**
     * Middleware general de los sitios. Es necesario realizar todo aca ya que se debe conocer la ruta y sus parametros
     * @param {Object} routeVariants - Informacion de la ruta y sus variantes segun idioma
     */
    const checkRoute = (route) => {
        return (req, res, next) => {
            const { transactionId } = req;
            const userLanguageId = req.session.user_language.language_id;
            const userLocaleId = req.session.user_language.language_locale_id;
            const preferredLanguage = req.session.preferred_language;

            // --------------------------------------------------------------------------
            // Deteccion del idioma actual
            // --------------------------------------------------------------------------
            let currentLanguage;

            // Solo controla en sitios multilenguaje
            if (website.language.multiLanguage) {
                const languageIso = req.params.language_iso;
                const languageId = languageHelper.getId(languageIso);

                // Controlo que el lenguaje exista y este activo en la pagina
                if (languageId) {
                    // Busca si el idioma esta activo en la pagina
                    currentLanguage = website.language.available.find((l) => {
                        if (l.show_locale) {
                            // Si tiene locale visible, controlo directo contra el locale.
                            // Si no coincide es porque no es este idioma
                            return l.language_locale_id === languageId;
                        }
                        // Si no tiene locale activo, tiene que coincidir por el id del lenguaje.
                        return l.language_id === languageId;
                    });
                }
            } else {
                // De lo contrario el lenguaje es siempre el mas optimo para el usuario
                currentLanguage = preferredLanguage;
            }
            req.session.current_language = currentLanguage;

            // --------------------------------------------------------------------------

            // Valido si el numero de pagina es tipo number
            if (req.params.page && !utils.isPositive(req.params.page)) {
                return res.status(404).send();
            }

            // --------------------------------------------------------------------------
            // Control de rutas viejas.
            // --------------------------------------------------------------------------
            // Se realiza primero para buscar las rutas viejas y redirigir si es necesario.
            // Por compatibilidad previa al sistema multi-lenguaje.
            if (route.oldValues && route.oldValues.length && route.oldValues.includes(req.route.path)) {
                let desiredRoute = route.values[userLocaleId] || route.values[userLanguageId] || route.values.default;

                if (req.params.page && req.params.page !== 1) {
                    desiredRoute = route.paginationValues[userLocaleId] || route.paginationValues[userLanguageId] || route.paginationValues.default;
                } else if (req.params.version) {
                    desiredRoute = route.versionValues[userLocaleId] || route.versionValues[userLanguageId] || route.versionValues.default;
                }

                const params = req.params;
                const newPath = linkRewrite(website, preferredLanguage, desiredRoute, params);
                logger.debug('[server/index.js] Redireccionando a ruta nueva', {
                    transactionId,
                    newPath
                });
                return res.redirect(301, newPath);
            }
            // --------------------------------------------------------------------------

            // --------------------------------------------------------------------------
            // Control de lenguaje actual
            // --------------------------------------------------------------------------
            // Si no hay current lang, implica que el idioma elegido no es valido (o no se eligio ningun idioma).
            if (!route.no_language_prefix && !noLanguagePrefixRoutes.includes(route.controller) && !currentLanguage) {
                let desiredRoute = route.values[userLocaleId] || route.values[userLanguageId] || route.values.default;

                if (req.params.page && req.params.page !== 1) {
                    desiredRoute = route.paginationValues[userLocaleId] || route.paginationValues[userLanguageId] || route.paginationValues.default;
                } else if (req.params.version) {
                    desiredRoute = route.versionValues[userLocaleId] || route.versionValues[userLanguageId] || route.versionValues.default;
                }

                const params = req.params;
                const newPath = linkRewrite(website, preferredLanguage, desiredRoute, params);
                logger.debug('[server/index.js] Redireccionando a idioma correcto', {
                    transactionId,
                    newPath
                });

                return res.redirect(301, newPath);
            }
            // --------------------------------------------------------------------------

            // --------------------------------------------------------------------------
            // Site context
            // --------------------------------------------------------------------------
            const localeId = currentLanguage
                ? (currentLanguage.language_locale_id || currentLanguage.language_id)
                : (preferredLanguage.language_locale_id || preferredLanguage.language_id);

            // Agarro el prefijo para todos los links
            const linksPrefix = linkHelper.getLanguagePrefix(website, currentLanguage || preferredLanguage);

            // Header, footer y sidebar links
            const headerLinks = req.website.headerLinks ? req.website.headerLinks[localeId] || req.website.headerLinks.default : [];
            const footerLinks = req.website.footerLinks ? req.website.footerLinks[localeId] || req.website.footerLinks.default : [];
            const sidebarLinks = req.website.sidebarLinks ? req.website.sidebarLinks[localeId] || req.website.sidebarLinks.default : [];
            const parsedHeaderLinks = headerLinks.map((l) => parseConfigurableLinks(l, linksPrefix));
            const parsedFooterLinks = footerLinks.map((l) => parseConfigurableLinks(l, linksPrefix));
            const parsedSidebarLinks = sidebarLinks.map((l) => parseConfigurableLinks(l, linksPrefix));

            // Canonical URL
            const canonicalUrl = route.canonicals
                ? linksPrefix + (route.canonicals[localeId] || route.canonicals.default)
                : req.path;
            
            // FB Pixel por lenguaje
            const fbPixel = website.fbPixel
                ? [website.fbPixel.default, website.fbPixel[localeId]]
                : [];
            const cleanFbPixel = fbPixel.filter((elem) => !!elem); // Eliminar valores vacios

            // Procesar enlaces de rutas
            const routes = [
                'home', 'movies_top_rated', 'movies_upcoming', 'movies_now_playing',
                'series_popular', 'series_top_rated', 'series_on_air', 'series_airing_today',
                'search', 'login', 'signup',
                'dmca'
            ];
            const availableLinks = {};
            for (const routeName of routes) {
                const route = req.website.routes.find((r) => r.name === routeName);
                if (route) {
                    const prefix = !route.no_language_prefix && !noLanguagePrefixRoutes.includes(route.controller)
                        ? linksPrefix
                        : '';
                    availableLinks[routeName] = prefix + (route.values[localeId] || route.values.default);
                }
            }

            // Agregar homes alternativos posibles
            const altHomesRoute = req.website.routes.find((r) => r.name === 'home_alt');
            let altHomesLinks;
            if (altHomesRoute && altHomesRoute.pathParameters && altHomesRoute.pathParameters.alternative_site) {
                const path = altHomesRoute.values[localeId] || altHomesRoute.values.default;
                const prefix = !altHomesRoute.no_language_prefix && !noLanguagePrefixRoutes.includes(altHomesRoute.controller)
                    ? linksPrefix
                    : '';
                altHomesLinks = altHomesRoute.pathParameters.alternative_site.map((altSite) => {
                    return {
                        link: prefix + path.replace(/:alternative_site/g, altSite),
                        name: altSite.toUpperCase().replace(/-/g, ' ')
                    };
                });
            }

            // Evaluar AMP
            const ampEnabled = !!route.amp; // esta activado si la ruta lo tiene activado
            // En el caso de estar activado, se genera la URL para poder agregarlo como Meta en el sitio
            const currentRoute = route.values[userLocaleId] || route.values[userLanguageId] || route.values.default;
            const ampUrl = ampEnabled
                ? website.host + linkRewrite(website, preferredLanguage, currentRoute, req.params, ampEnabled)
                : null;
            
            // Agregar al REQ info si estamos actualmente en una ruta amp o no
            if (ampEnabled && req.path.includes(ampPrefix + '/')) {
                req.amp = true;
            }

            res.data.context.site = {
                id: website.id,
                host: website.host,
                name: website.name,
                multi_language: website.language.multiLanguage || false,
                title: route.titles ? route.titles[localeId] || route.titles.default : '',
                description: route.descriptions ? route.descriptions[localeId] || route.descriptions.default : '',
                canonical: website.host + canonicalUrl,
                links: availableLinks,
                analytics: website.analytics,
                adsense: website.adsense,
                fb_pixel: cleanFbPixel.length ? cleanFbPixel : null,
                facebook: website.facebook,
                twitter: website.twitter,
                dmca_id: website.dmca_id,
                head_custom: website.customHead,
                header_links: parsedHeaderLinks,
                footer_links: parsedFooterLinks,
                sidebar_links: parsedSidebarLinks,
                alternative_home_links: altHomesLinks,
                stress_test_mode: config.STRESS_TEST_MODE === 'true',
                is_amp: req.amp,
                alternate_amp: ampUrl
            };

            res.data.context.request = {
                hostname: req.hostname,
                cyphered_hostname: Buffer.from(req.hostname).toString('base64'),
                original_url: req.originalUrl,
                path: req.path,
                protocol: req.protocol,
                method: req.method,
                headers: req.headers,
                cookies: req.cookies
            };

            // --------------------------------------------------------------------------
            // Keywords
            // --------------------------------------------------------------------------
            // Recorre las keywords de la configuracion y las setea en el contexto
            res.data.context.site_keywords = {};
            for (const keyword of website.keywords) {
                res.data.context.site_keywords[keyword.name] = keyword.values[localeId] || keyword.values.default;
            }

            // Recorre las keywords de la ruta, si es que hay
            res.data.context.route_keywords = {};
            if (route.keywords) {
                for (const keyword of route.keywords) {
                    res.data.context.route_keywords[keyword.name] = keyword.values[localeId] || keyword.values.default;
                }
            }

            // --------------------------------------------------------------------------

            // --------------------------------------------------------------------------
            // Guardar language en el contexto
            // --------------------------------------------------------------------------
            const l = languageHelper.getLanguage(localeId);
            res.data.context.language_name = l.name;
            res.data.context.language_iso = l.iso;

            return next();
        };
    };

    // Defino rutas dinámicamente
    for (const route of website.routes) {
        const currentRouteController = route.controller;

        // Buscar el route controller
        const websiteType = website.type;
        let controller;
        if (routesControllers[websiteType] && routesControllers[websiteType][currentRouteController]) {
            controller = routesControllers[websiteType][currentRouteController];
        } else if (routesControllers.common && routesControllers.common[currentRouteController]) {
            controller = routesControllers.common[currentRouteController];
        } else {
            logger.error('[server/index.js] Ruta configurada invalida', {
                websiteId: website.id,
                route
            });
        }

        // Verifico que exista el controller
        if (controller) {
            // Recorro todas las alternativas de idiomas y armo un set de paths unicos
            const routeVariants = route.values;

            // Ruta paginada
            const paginationRouteVariants = route.paginationValues ? route.paginationValues : null;
            // Ruta versionada
            const versionRouteVariants = route.versionValues ? route.versionValues : null;
            // Ruta amp
            const ampEnabled = !!route.amp;

            let paths = new Set();

            // Si el sitio tiene multiples idiomas, recorro los idiomas disponibles y por cada uno defino las rutas
            if (website.language.multiLanguage) {
                for (const lang of website.language.available) {
                    // Defino la ruta correcta. Si existe para ese ID de Lenguaje y sino el default. 
                    // Agrego el prefijo con el ISO correcto. Si la ruta tiene configurada locale usa el ISO completo.
                    const langId = lang.language_locale_id || lang.language_id;
                    const path = routeVariants[langId] || routeVariants.default;

                    // Ruta paginada
                    const paginationPath = !paginationRouteVariants ? null : paginationRouteVariants[langId] || paginationRouteVariants.default;

                    // Ruta versionada
                    const versionPath = !versionRouteVariants ? null : versionRouteVariants[langId] || versionRouteVariants.default;

                    if (!route.no_language_prefix && !noLanguagePrefixRoutes.includes(route.controller)) {
                        let prefix = '/:language_iso(([a-z]{2}|[a-z]{2}-[A-Z]{2}))';

                        paths.add(prefix + path);
                        // Ruta paginada
                        if (paginationPath) {
                            paths.add(prefix + paginationPath);
                        }
                        // Ruta versionada
                        if (versionPath) {
                            paths.add(prefix + versionPath);
                        }
                        // Ruta amp
                        if (ampEnabled) {
                            paths.add(prefix + ampPrefix + path);
                        }
                    }

                    // Agrega el path con y sin prefijo. Si alguien entra a la ruta sin idioma, redirige a con idioma.
                    paths.add(path);

                    // Ruta paginada
                    if (paginationPath) {
                        paths.add(paginationPath);
                    }
                    // Ruta versionada
                    if (versionPath) {
                        paths.add(versionPath);
                    }
                    // Ruta amp
                    if (ampEnabled) {
                        paths.add(ampPrefix + path);
                    }
                }
            } else {
                for (const languageId in routeVariants) {
                    paths.add(routeVariants[languageId]);
                }
                // Ruta paginada
                if (paginationRouteVariants) {
                    for (const languageId in paginationRouteVariants) {
                        paths.add(paginationRouteVariants[languageId]);
                    }
                }
                // Ruta versionada
                if (versionRouteVariants) {
                    for (const languageId in versionRouteVariants) {
                        paths.add(versionRouteVariants[languageId]);
                    }
                }
                // Ruta amp
                if (ampEnabled) {
                    for (const languageId in routeVariants) {
                        paths.add(ampPrefix + routeVariants[languageId]);
                    }
                }
            }

            // Defino ademas las rutas viejas por compatibilidad
            if (route.oldValues && route.oldValues.length) {
                for (const path of route.oldValues) {
                    paths.add(path);
                }
            }

            paths = Array.from(paths);

            // Por cada uno de los paths encontrados, defino la ruta
            for (const path of paths) {
                if (route.method === 'POST') {
                    // Para las peticiones POST tenemos que agregar el bodyparser
                    app.post(path, bodyParser.json({
                        limit: '50mb'
                    }), bodyParser.urlencoded({
                        limit: '50mb',
                        extended: true
                    }), checkRoute(route), controller);
                } else {
                    app.get(path, checkRoute(route), controller);
                }
            }
        }
    }

    // Si no se definio el path, es porque no hiteo ninguna ruta. Redirijo al home
    app.use((req, res, next) => {
        const { transactionId } = req;
        if (!res.data || !res.data.path) {
            const route = website.routes.find((r) => r.name === 'home').values;
            const linkPrefix = linkHelper.getLanguagePrefix(website, req.session.preferred_language);
            const link = route[req.session.preferred_language.language_locale_id] || route.default;

            logger.debug('[server/index.js] Ingreso a una ruta inexistente', {
                transactionId,
                path: req.path
            });

            return res.redirect(301, linkPrefix + link);
        }

        return next();
    });

    // Load translations
    let Translations;
    const getTranslations = (callback) => {
        if (Translations) {
            return callback(null, Translations);
        }

        Translations = {};

        // El orden es importante ya que estan ordenados por prioridad ascendente.
        // Las últimas traducciones van a pisar a las primeras.
        const paths = [website.paths.sharedTranslations, website.paths.translations];

        async.mapSeries(paths, (path, cb) => {
            if (!path) {
                return cb();
            }

            let parsedPath = path;
            if (parsedPath[parsedPath.length - 1] !== '/') {
                parsedPath += '/';
            }

            fs.readdir(parsedPath, (err, files) => {
                if (err) {
                    return cb();
                }

                for (const file of files) {
                    // Si hay algo que no es .json, lo ignora
                    if (file.indexOf('.json') !== -1) {
                        const languageName = file.split('.')[0];
                        const currentTranslations = require(parsedPath + file);
                        // Extender traducciones.
                        Translations[languageName] = Object.assign({}, Translations[languageName], currentTranslations);
                    }
                }

                return cb();
            });
        }, () => {
            return callback(null, Translations);
        });
    };

    app.use((req, res, next) => {
        const currentLanguage = req.session.current_language;
        if (!currentLanguage) {
            res.data.context.translations = {};
            return next();
        }

        getTranslations((err, translations) => {
            const currentLanguage = req.session.current_language;
            const languageId = currentLanguage.language_id;
            const languageIso = languageHelper.getLanguage(languageId).iso;
            const languageLocaleId = currentLanguage.show_locale ? currentLanguage.language_locale_id : currentLanguage.language_id;
            const languageLocaleIso = languageHelper.getLanguage(languageLocaleId).iso;

            // Cuando el sitio tiene locale activado, se usa el idioma original como base y se sobreescribe con las especificas del locale
            if (languageIso !== languageLocaleIso) {
                res.data.context.translations = Object.assign({}, translations[languageIso], translations[languageLocaleIso]);
            } else {
                res.data.context.translations = translations[languageLocaleIso] || {};
            }

            return next();
        });
    });

    // Compile and send
    app.use((req, res) => {
        const { transactionId } = req;
        const finalPath = path.join(website.paths.website, res.data.path);

        res.data.context.current_path = req.path;

        // Modo debug
        if (req.query.debug && req.query.pass === 'sta9090') {
            return res.send(res.data.context);
        }

        // Agrego la session
        if (req.session && req.session.user) {
            res.data.context.session = req.session.user;
        }

        // Compilo el codigo y respondo
        res.render(finalPath, res.data.context, (err, page) => {
            if (err) {
                const url = `${req.hostname}${req.path}`;
                logger.error(`[server/index.js] Error renderizando vista URL "${url}"`, {
                    transactionId,
                    url,
                    finalPath,
                    err
                });
                return res.status(500).send();
            } else {
                logger.debug(`> [${req.method}] ${req.path}`, { transactionId });
                return res.send(page);
            }
        });
    });

    // Automatically assign a port
    const listener = app.listen(0, () => {
        const port = listener.address().port;
        logger.info(`${website.name} listening on port ${port}`);
        return callback(null, {
            port
        });
    });
};

const linkRewrite = (website, preferredLanguage, desiredRoute, params, ampEnabled = false) => {
    // Arma el nuevo path. El nuevo path depende de los parametros.
    const splittedRoute = desiredRoute.split('/');
    const finalRoute = [];
    // Desarmo la ruta para poder extraer los parametros
    for (let part of splittedRoute) {
        let isParam = part.indexOf(':') !== -1;
        // Si la parte es un parametro levanto el parametro del params.
        if (isParam) {
            let paramName = part.substr(1);
            finalRoute.push(params[paramName]);
        } else {
            // Sino simplemente utilizo la parte
            finalRoute.push(part);
        }
    }

    const r = finalRoute.join('/');
    // Si tiene multilenguaje, agrega el prefijo del idioma.
    const prefix = linkHelper.getLanguagePrefix(website, preferredLanguage);
    // Si tiene amp, se agrega el prefijo.
    const link = !ampEnabled
        ? `${prefix}${r}`
        : `${prefix}${ampPrefix}${r}`;
    return link;
};

const parseConfigurableLinks = (linkObj, linksPrefix) => {
    // Si es una ruta relativa y no es un archivo estatico agrego el prefijo
    if (linkObj.link[linkObj.link.length - 1] === '/' && linkObj.link.indexOf('/static/') !== 0) {
        return {
            ...linkObj,
            link: linksPrefix + linkObj.link
        };
    }
    return linkObj;
};