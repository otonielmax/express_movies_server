'use strict';

/**
 * Middleware que se encarga de obtener informacion de la conexion usando la IP e IP2Location
 * Guarda la informacion en req.location
 */
module.exports = (req, res, next) => {
    // Primero tratar de agarrar la IP que nos da Cloudflare
    let ip = req.get('CF-Connecting-IP');
    // Si no hay IP, tratar de agarrar la primera IP de X-Forwarded-For
    if (!ip) {
        // X-Forwarded-For puede ser un listado de IPs separados por coma. Agarro el primero
        let ipForwarded = req.get('X-Forwarded-For');
        if (ipForwarded) {
            ip = ipForwarded.split(',')[0];
        }
    }
    // Si aun no hay ip, intento agarrar el que da Express (no funciona con Cloudflare)
    if (!ip) {
        ip = req.ip;
    }
    // Como ultima alternativa pongo 127.0.0.1
    if (!ip) {
        ip = '127.0.0.1';
    }
    // Eliminar caracteres 
    ip = ip.replace('::ffff:', '');

    // Cloudflare country
    const cloudFlareCountry = req.get('CF-IPCountry');

    req.location = {
        ip,
        country_code: cloudFlareCountry
    };

    return next();
};