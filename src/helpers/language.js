'use strict';
const dbService = require('../services/db');

const Languages = {};
const LanguagesIds = {};

/**
 * Carga el listado de lenguajes de la base de datos
 */
exports.retrieve = (callback) => {
    const query = `
        SELECT
            id,
            name,
            iso,
            locale
        FROM
            language
        ORDER BY
            iso ASC
    `;

    dbService.query(query, [], (err, languages) => {
        if (err) {
            return callback(err);
        }

        for (let lang of languages) {
            // Si tiene locale, guardo referencia al idioma original
            if (lang.locale) {
                let baseLang = lang.iso.substr(0, lang.iso.indexOf('-'));
                lang.language_id = languages.find((l) => l.iso == baseLang).id;
            }

            Languages[lang.iso] = lang;
            LanguagesIds[lang.id] = lang;
        }

        return callback();
    });
};

/**
 * Obtiene el ID de un ISO especifico
 * 
 * @param   {String}    iso     - ISO code del lenguaje
 * @returns {Number}    ID del lenguaje. `null` si el lenguaje no existe 
 */
exports.getId = (iso) => {
    return Languages[iso] ? Languages[iso].id : null;
};

/**
 * Obtiene un lenguaje utilizando el ID
 * 
 * @param   {Number}    id      - ID del lenguaje
 * @returns {Object}    Objeto del lenguaje
 */
exports.getLanguage = (id) => {
    return LanguagesIds[id];
};

/**
 * Busca lenguaje
 * 
 * @param   {String}    searchString    - Parte inicial del texto
 * @returns {Array}     Arreglo de lenguajes que coinciden con la busqueda
 */
exports.getAll = (searchString) => {
    const result = [];

    for (const key in Languages) {
        if (key.indexOf(searchString) === 0) {
            result.push(Languages[key]);
        }
    }

    return result;
};