'use strict';

const languageHelper = require('./language');

/**
 * Obtiene el mejor Hash disponible para un recurso (serie, season, episode)
 * 
 * @param   {Array}     hashes              - Arreglo de hashes obtenidos de DB
 * @param   {Object}    preferredLanguage   - Lenguaje de preferencia
 * @param   {Object}    defaultLanguage     - Lenguaje por defecto del sitio
 */
exports.getBestHash = (hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;
    // Primero intento agarrar el locale por defecto de la pagina
    bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id);

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id);
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id);
        }
    }

    // Si no existe, agarro ingles
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((h) => h.language_id === fallbackLangId);
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};