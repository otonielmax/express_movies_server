const config = require('config');
const fs = require('fs');
const path = require('path');

const getWebsitesConfig = () => {
    // Procesar websites
    const websites = [];
    const p = path.join(__dirname, '../config/websites/');
    const files = fs.readdirSync(p);
    for (const file of files) {
        // Si hay algo que no es .js, lo ignora
        if (file.indexOf('.js') !== -1) {
            websites.push(require(p + file));
        }
    }

    return websites.filter((w) => {
        if (config.ENVIRONMENT !== 'production') {
            return true;
        } 
        // Si el entorno es production, solo retornamos los sitios activos
        return w.active === true;
    });
};

module.exports = getWebsitesConfig;