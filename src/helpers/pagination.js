'use strict';
const linkHelper = require('./link');
const { logger } = require('../logger');

/**
 * Parsea la informacion de paginacion a la manera que es utilizado en todos los sitios
 * 
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {String}    routeName           - Nombre de la ruta actual    
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @param   {Number}    currentPage         - Pagina actual
 * @param   {Number}    itemsTotal          - Total de items
 * @param   {Number}    itemsLimit          - Items por pagina
 * @returns {Object}    Objeto con toda la informacion de la paginacion
 */
exports.get = (website, routeName, currentLanguage, currentPage, itemsTotal, itemsLimit) => {
    let totalPages = Math.ceil(itemsTotal / itemsLimit);
    let pages;

    if (totalPages === 0) {
        totalPages = 1;
        pages = [1];
    } else {
        pages = [currentPage - 2, currentPage - 1, currentPage, currentPage + 1, currentPage + 2].filter((p) => p >= 1 && p <= totalPages);

        const first = pages[0];
        const last = pages[pages.length - 1];
        const missing = 5 - pages.length;

        if (missing) {
            if (first === 1) {
                // Si la primera pagina es 1, indica que faltan al final

                // Agrego al final los que necesite
                for (let i = 1; i <= missing; i++) {
                    if (last + 1 <= totalPages) {
                        pages.push(last + i);
                    }
                }
            } else if (last === totalPages) {
                // Si la ultima pagina es el maximo, faltan al principio

                // Agrego al principio los que necesite
                for (let i = 1; i <= missing; i++) {
                    if ((first - i) >= 1) {
                        pages.unshift(first - i);
                    }
                }
            } else {
                // Cae aca cuando pone una pagina invalida (mas de las que existen)
                // Para que no crashee porque no existe la pagina, agrego un link a la ultima pagina
                pages.push(totalPages);
            }
        }
    }

    const pagesLink = pages.map((page) => {
        return {
            is_current: page === currentPage,
            page: page,
            type: 'page',
        };
    });

    if (!pagesLink[0]) {
        logger.error(`[pagination] Caso raro. CP: ${currentPage}, IT: ${itemsTotal}, IL: ${itemsLimit}, Pages ${pages}`);
    }

    // Si no tiene visible la primera pagina, la agrego y agrego el ellipsis
    if (pagesLink[0].page !== 1) {
        pagesLink.unshift({
            type: 'ellipsis',
        });
        pagesLink.unshift({
            page: 1,
            type: 'page',
        });
    }

    if (currentPage !== 1) {
        pagesLink.unshift({
            page: currentPage - 1,
            type: 'prev',
        });
    }

    // Si no tiene visible la ultima pagina, la agrego y agrego el ellipsis
    if (pagesLink[pagesLink.length - 1].page !== totalPages) {
        pagesLink.push({
            type: 'ellipsis',
        });
        pagesLink.push({
            page: totalPages,
            type: 'page',
        });
    }

    if (currentPage !== totalPages) {
        pagesLink.push({
            page: currentPage + 1,
            type: 'next',
        });
    }

    const linkPrefix = linkHelper.getLanguagePrefix(website, currentLanguage);
    for (const pageLink of pagesLink) {
        if (pageLink && pageLink.page) {
            const route = !website || !website.routes ? null : website.routes.find((route) => route && (route.name === routeName));
            // in case route is null
            if (!route) {
                continue;
            }
            const defaultLink = (pageLink.page === 1) ?
                route.values[currentLanguage.language_locale_id] || route.values.default :
                route.paginationValues[currentLanguage.language_locale_id] || route.paginationValues.default;
            pageLink.link = linkPrefix + defaultLink.replace(/:page/g, pageLink.page);
        }
    }

    const result = {
        pages: pagesLink,
        current_page: currentPage,
        total_items: itemsTotal,
        total_pages: totalPages,
    };

    return result;
};