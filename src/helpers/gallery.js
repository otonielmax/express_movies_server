'use strict';

/**
 * En base a un nombre de galeria, retorna el tipo de columna que se debe usar en una query
 * 
 * @param {String} galleryType - Tipo de galeria: popular, top_rated, on_air, etc
 */
exports.getColumName = (galleryType) => {
    return `gallery_${galleryType}_position`;
};