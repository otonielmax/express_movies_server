'use strict';
const dbService = require('../services/db');

/**
 * Comprueba que la conexion a la base de datos esta activa
 * 
 * @param {Object}  res - Objecto response del request
 */
exports.checkStatus = (res) => {
    dbService.query('SELECT 1+1', [], (err) => {
        let response;
        if (err) {
            response = {
                statusCode: 500,
                message: err ? err.toString() : '',
            };
        } else {
            response = {
                statusCode: 200,
                message: 'ok',
            };
        }

        res.writeHead(response.statusCode, {});
        res.write(JSON.stringify(response));

        return res.end();
    });
};