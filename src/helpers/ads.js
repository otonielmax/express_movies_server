'use strict';

/**
 * Determina si puede o no mostrar anuncios en el player en base al pais y si es un crawler o no
 * @param {Array}   adCountries - Arreglo de paises para mostrar anuncios en el player
 * @param {Object}  countryCode - Codigo de pais
 * @param {Boolean} isCrawler - Boolean si es un crawler o no
 */
exports.showAds = (adCountries, countryCode, isCrawler) => {
    if (isCrawler) {
        return false;
    }

    if (countryCode && adCountries && adCountries.length) {
        return adCountries.includes(countryCode);
    } else {
        return false;
    }
};