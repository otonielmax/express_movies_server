'use strict';

exports.compose = (fn, ...args) => {
    return (callback) => {
        fn(...args, callback);
    };
};

/**
 * Verify if an value is an object.
 * 
 * @name isObject
 * @function
 * @param {*} obj - Value to analyze.
 * @return {Boolean} - Returns true if the value is an object, false if it is not. 
 */
exports.isObject = (obj) => {
    return obj !== undefined && obj === Object(obj) && !Array.isArray(obj);
};

/**
 * Verify if an value is an array.
 * 
 * @name isArray
 * @function
 * @param {*} arr - Value to analyze.
 * @return {Boolean} - Returns true if the value is an array, false if it is not. 
 */
exports.isArray = (arr) => {
    return arr !== undefined && Array.isArray(arr);
};

/**
 * Verify if an value is a number.
 * 
 * @name isNumber
 * @function 
 * @param {*} num - Value to analyze.
 * @return {Boolean} - Returns true if the value is a number, false if it is not.
 */
exports.isNumber = (num) => {
    return num !== undefined && !isNaN(parseFloat(num)) && isFinite(num);
};

/**
 * Verify if an value is an integer.
 * 
 * @name isInteger
 * @function 
 * @param {*} num - Value to analyze.
 * @return {Boolean} - Returns true if the value is an integer, false if it is not.
 */
exports.isInteger = (num) => {
    return exports.isNumber(num) && (parseFloat(num) % 1) === 0;
};

/**
 * Verify if an value is a positive number.
 * 
 * @name isPositive
 * @function 
 * @param {*} num - Value to analyze.
 * @return {Boolean} - Returns true if the value is a positive number, false if it is not.
 */
exports.isPositive = (num) => {
    return exports.isInteger(num) && num > 0;
};

/**
 * Verify if an value is a string.
 * 
 * @name isString
 * @function 
 * @param {*} str - Value to analyze
 * @return {Boolean} - Returns true if the value is a string, false if it is not.
 */
exports.isString = (str) => {
    return str !== undefined && str.constructor === String;
};

/**
 * Generates a random string of a certain length.
 * 
 * @name generateRandomString
 * @function
 * @param {Integer} length - Length of the string to generate.
 * @param {String} [possibleChars] - Which characters can be used at the generation of the string.
 * @return {String} - String randomly generated.
 */
exports.generateRandomString = (length, possibleChars) => {
    let randomString = '';
    let possible = possibleChars ? possibleChars : 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
        randomString += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return randomString;
};

/**
 * @function getCreditCardBrand
 * @desc Valida el numero de tarjeta de credito para detectar la marca
 * @param cardNumber Numero de la tarjeta de credito a validar
 * @return {String} Nombre de la marca de la tarjeta de credito
 */
exports.getCreditCardBrand = (cardNumber) => {
    if (!cardNumber) {
        return false;
    }

    let creditCardRules = {
        amex: /^3[47][0-9]{13}$/,
        aura: /^5[0-9]{18}$/,
        elo: /^((50670[7-8])|(506715)|(50671[7-9])|(50672[0-1])|(50672[4-9])|(50673[0-3])|(506739)|(50674[0-8])|(50675[0-3])|(50677[4-8])|(50900[0-9])|(50901[3-9])|(50902[0-9])|(50903[1-9])|(50904[0-9])|(50905[0-9])|(50906[0-4])|(50906[6-9])|(50907[0-2])|(50907[4-5])|(636368)|(636297)|(504175)|(438935)|(40117[8-9])|(45763[1-2])|(457393)|(431274)|(50907[6-9])|(50908[0-9])|(627780))/,
        diners: /^(36[0-9]{12}|3(?:0[0-5]|[68][0-9])[0-9]{11})$/,
        discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
        hipercard: /^((606282|637095|637568)[0-9]{10}|38[0-9]{14,17})$/,
        mastercard: /^5[0-5][0-9]{14}$|2[2-6][0-9]{14}$|271[0-9]{13}$|2720[0-9]{12}/,
        visa: /^4[0-9]{15}$/,
        carnet: /^(286900|502275|506(199|2(0[1-6]|1[2-578]|2[289]|3[67]|4[579]|5[01345789]|6[12359]|7[02-9]|8[0-47]|9[479])|3(0[0-79]|1[1-49]|2[039]|3[02-79]|4[0-49]|5[0-79]|6[014-79]|7[0-49]|8[023467]|9[124])|402)|606333|636379|639(388|484|559)|588772(02|66|67|68|74|84)|6046220[34])/
    };

    /* Valida que el tipo de tarjeta sea valido */
    for (let rule in creditCardRules) {
        /* Busco algun regex que cumpla */
        if (creditCardRules[rule].test(cardNumber)) {
            return rule;
        }
    }

    return false;
};

exports.isEmail = (str) => {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return exports.isString(str) && regex.test(str);
};

/**
 * Encodes a string in Base64
 * @param {String} str - String to encode
 * @returns {String} - Encoded string 
 */
exports.encodeBase64 = (str) => {
    return Buffer.from(str).toString('base64');
};

/**
 * Decodes a  Base64 string to plain string.
 * @param {String} str - String to encode
 * @returns {String} - Encoded string 
 */
exports.decodeBase64 = (str) => {
    return Buffer.from(str, 'base64').toString();
};

exports.addParamsToURL = (url, params) => {
    let newParams = [];
    for (let key in params) {
        newParams.push(`${key}=${encodeURIComponent(params[key])}`);
    }
    newParams = newParams.join('&');

    const hasParams = url.indexOf('?') !== -1;
    if (hasParams) {
        url += '&';
    } else {
        url += '?';
    }

    return url + newParams;
};