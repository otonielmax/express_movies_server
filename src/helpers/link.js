'use strict';
const languageHelper = require('./language');

/**
 * Obtiene el prefijo del lenguaje para la URL a traves de la configuracion del sitio
 * 
 * @param   {Object}    website             - Configuracion del sitio
 * @param   {Object}    currentLanguage     - Lenguaje actual
 * @returns {String}    Prefijo del lenguaje para la URL
 */
exports.getLanguagePrefix = (website, currentLanguage) => {
    let prefix = '';

    if (website.language.multiLanguage) {
        let languageId = currentLanguage.show_locale ? currentLanguage.language_locale_id : currentLanguage.language_id;
        const languageIso = languageHelper.getLanguage(languageId).iso;
        prefix = '/' + languageIso;
    }

    return prefix;
};

/**
 * Genera los links alternativos para una ruta
 * 
 * @param   {Object}    website       - Configuracion del sitio
 * @param   {String}    routeName     - Nombre de la ruta actual    
 * @param   {Number}    pageNumber        - Numero de la pagina actual
 */
exports.getAlternateLang = (website, routeName, pageNumber) => {
    /* 
     * si el numero de pagina es igual a 1, se usa la ruta por defecto 
     * de lo contrario se usa la ruta paginada
     */
    const route = pageNumber === 1
        ? website.routes.find((r) => r.name === routeName).values
        : website.routes.find((r) => r.name === routeName).paginationValues;

    let alternateLangs = [];

    // Por cada lenguaje activo en el sitio, lo agrega al listado de lenguajes alternativos disponibles
    for (const language of website.language.available) {
        const langId = language.show_locale ? language.language_locale_id : language.language_id;
        alternateLangs.push({
            default: language.default ? true : false,
            hreflang: languageHelper.getLanguage(langId).iso,
            href: website.host + exports.getLanguagePrefix(website, language) + (route[langId] || route.default),
            language_name: languageHelper.getLanguage(langId).name,
        });
    }

    // Ordeno primero el default
    alternateLangs = alternateLangs.sort((a, b) => {
        return b.default - a.default;
    });

    return alternateLangs;
};