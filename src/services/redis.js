'use strict';
const config = require('config');
const Redis = require('ioredis');

const { logger } = require('../logger');
const util = require('../helpers/utils');

const transactionId = 'redis';
let slaveClient;
let masterClient;

if (config.REDIS_DISABLED !== 'true') {
    slaveClient = new Redis(config.REDIS_SLAVE_URL, {
        retry_strategy: () => Number(config.REDIS_SLAVE_RETRY_STRATEGY)
    });
    masterClient = new Redis(config.REDIS_MASTER_URL, {
        retry_strategy: () => Number(config.REDIS_MASTER_RETRY_STRATEGY)
    });
    
    /**
     * Escucha el evento de conexion
     */
    slaveClient.on('connect', () => {
        logger.info('Conexion "SLAVE" establecida', { transactionId });
    });
    masterClient.on('connect', () => {
        logger.info('Conexion "MASTER" establecida', { transactionId });
    });
    /**
     * Escucha el evento de error
     */
    slaveClient.on('error', (err) => {
        logger.error('Error de conexion "SLAVE":', { transactionId, err });
    });
    masterClient.on('error', (err) => {
        logger.error('Error de conexion "MASTER"', { transactionId, err });
    });
}

/**
 * Obtiene informacion a partir de una key usando el cliente local
 * 
 * @param {String} key - Clave que se usa para obtener la informacion
 * @param {Function} callback - Funcion para manejar la respuesta
 */
exports.getData = (key, callback) => {
    if (!key || !util.isString(key)) {
        return callback(new Error('La clave es invalida'));
    }

    slaveClient.get(key, (err, data) => {
        /* Si hubo error corta */
        if (err) {
            return callback(err);
        }

        /* Si no estaba en redis corta */
        if (!data) {
            return callback(null, null);
        }

        return callback(null, data);
    });
};

/**
 * Guarda informacion con una key usando el cliente remoto
 * 
 * @param {String} key - Clave que se usa para guardar la informacion
 * @param {String} data - Informacion a guardar
 * @param {Integer} expireTime - Tipo de expiracion para la informacion
 * @param {Function} callback - Funcion para manejar la respuesta
 */
exports.setData = (key, data, expireTime, callback) => {
    if (!key || !data) {
        return callback(new Error('Falta información para poder guardar el dato'));
    }

    if (!util.isString(key) || !util.isString(data)) {
        return callback(new Error('La clave y el dato tienen que ser strings'));
    }

    if (expireTime) {
        masterClient.set(key, data, 'EX', expireTime, (err) => {
            if (err) {
                return callback(err);
            } else {
                return callback(null);
            }
        });
    } else {
        masterClient.set(key, data, (err) => {
            if (err) {
                return callback(err);
            } else {
                return callback(null);
            }
        });
    }
};

/**
 * Elimina una key usando el cliente remoto
 * 
 * @param {String} key - Clave a eliminar
 * @param {Function} callback - Funcion para manejar la respuesta
 */
exports.removeData = (key, callback) => {
    if (!key || !util.isString(key)) {
        return callback(new Error('La clave es invalida'));
    }

    masterClient.del(key, (err) => {
        if (err) {
            return callback(err);
        } else {
            return callback(null);
        }
    });
};