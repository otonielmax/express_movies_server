'use strict';
const config = require('config');
const mysql = require('mysql2');

const { logger } = require('../logger');

const transactionId = 'db-service';
const clusterConfig = JSON.parse(config.MYSQL_CLUSTER);

// Create MySQL Pool Cluster and set up nodes
const poolCluster = mysql.createPoolCluster({
    removeNodeErrorCount: 9999999999,
    restoreNodeTimeout: 5000
});

for (const mysqlConfig of clusterConfig) {
    const currentConfig = Object.assign({}, mysqlConfig);
    delete currentConfig.name;
    poolCluster.add(mysqlConfig.name, currentConfig);
}

poolCluster.on('remove', (nodeId) => {
    logger.error(`Removed MySQL Node "${nodeId}"`, { transactionId });
});

exports.getMasterConnection = (callback) => {
    poolCluster.getConnection('MASTER', (err, conn) => {
        if (err) {
            return callback(err);
        }

        return callback(null, new dbObject(conn));
    });
};

exports.getConnection = (callback) => {
    poolCluster.getConnection('SLAVE*', (err, conn) => {
        if (err) {
            return callback(err);
        }

        return callback(null, new dbObject(conn));
    });
};

exports.query = (query, values, callback) => {
    if (!query) {
        return callback('Falta query');
    }

    if (!values) {
        return callback('Falta values');
    }

    exports.getConnection((err, dbConnection) => {
        if (err) {
            return callback(`Error obteniendo conexion: ${err}`);
        }

        dbConnection.query(query, values, (err, result, fields) => {
            dbConnection.release();
            return callback(err, result, fields);
        });
    });
};

exports.endPool = (callback) => {
    poolCluster.end(() => {
        if (callback) {
            callback();
        }
    });
};

class dbObject {
    constructor(connection) {
        this.connection = connection;
    }
    query(query, params, callback, timeout = 2500) {
        const ts = +(new Date());
        const sql = this.connection.query({
            sql: query,
            timeout: timeout,
            values: params
        }, (err, results, fields) => {
            const timeSpent = +(new Date()) - ts;
            const formattedQuery = sql.sql.split('\n').join(' ').replace(/( {2,}|\t)/g, ' ').trim();
            if (err) {
                logger.error('[db.query] Error ejecutando query', {
                    err,
                    query: formattedQuery,
                    timeSpent
                });
            } else {
                logger.debug('[db.query] Query exitosa', {
                    query: formattedQuery,
                    timeSpent
                });
            }
            return callback(err, results, fields);
        });
    }
    release() {
        /* Dentro de un try-catch ya que si la conexion ya fue liberada, tira error */
        try {
            this.connection.release();
        } catch (e) {
            // logger.log('warn', 'Se intento liberar una conexion a DB que ya estaba liberada.');
        }
    }
    beginTransaction(callback) {
        this.connection.beginTransaction((err) => {
            return callback(err);
        });
    }
    rollback(callback) {
        this.connection.rollback(() => {
            return callback();
        });
    }
    commit(callback) {
        let that = this;
        this.connection.commit(function (err) {
            if (err) {
                that.rollback(function () {
                    return callback(err);
                });
            }

            return callback();
        });
    }
}