'use strict';
const async = require('async');
const config = require('config');
const cron = require('node-cron');
const moment = require('moment');

const { logger } = require('../logger');
const redisService = require('./redis');
const utils = require('../helpers/utils');

// Objeto de Cache
const Cache = {};

// Cron para remover objetos del cache
cron.schedule('*/10 * * * * *', () => {
    let now = moment().unix();
    // Recorre los cache que estan en memoria
    for (let i in Cache) {
        // Verifica si esta vencido o no y lo borra
        if (Cache[i].expires <= now) {
            delete Cache[i];
        }
    }
});

exports.get = (key, options, callback) => {
    /* Valida que la clave recibida sea valida */
    if (!key || !key.length) {
        logger.error('[cache.get] Error de codigo: no se envio clave para obtener el cache', {
            key,
            options
        });
        return callback({
            message: 'Clave invalida'
        });
    }

    if (!options || options.localExpireTime === undefined) {
        logger.error('[cache.get] Error de codigo: no se envio tiempo de expiracion local', {
            key,
            options
        });
        return callback({
            message: 'Falta el tiempo de expiracion para cache local'
        });
    }

    /* Busca el rating en el cache en memoria de la API */
    let localCache = Cache[key];

    if (localCache !== undefined) {        
        let data = localCache.data;

        // Convierto de nuevo a binario si es necesario
        if (options.binary) {
            data = Buffer.from(data, 'base64');
        }

        logger.silly(`[cache] Clave "${key}" obtenida de memoria`, {
            key,
            data
        });

        return callback(null, data);
    }

    /* Si esta deshabilitado Redis, corta */
    if (config.REDIS_DISABLED === 'true') {
        return callback(null, null);
    }

    redisService.getData(key, (err, data) => {
        if (err) {
            logger.error('[cache.get] Error obteniendo cache de Redis', {
                key,
                err
            });

            return callback(err);
        }

        if (!data) {
            logger.silly(`[cache] Clave "${key}" no encontrada en Redis`);
            return callback(null, null);
        }

        if (options.stringify) {
            try {
                data = JSON.parse(data).data;
            } catch (err) {
                logger.error('[cache.get] Error al hacer JSON.parse', {
                    key,
                    data,
                    err
                });
                return callback(null, null);
            }
        }

        // Convierto de nuevo a binario si es necesario
        if (options.binary) {
            data = Buffer.from(data, 'base64');
        }

        logger.silly(`[cache] Clave "${key}" obtenida de Redis`, {
            key,
            data
        });

        // Guarda el cache en memoria solo si lo pide
        if (options.localExpireTime > 0) {
            let cacheToSave = {
                key: key,
                data: data,
                expires: moment().add(options.localExpireTime, 'seconds').unix()
            };

            logger.silly(`[cache] Almacenando clave "${key}" en memoria`, {
                key,
                cacheToSave
            });

            /* Guarda el cache en memoria por si se consulta luego */
            Cache[key] = cacheToSave;
        }

        return callback(null, data);
    });
};

exports.save = (key, data, options, callback) => {
    if (!key || !data) {
        logger.error('[cache.save] Error de codigo: no se envio clave para guardar el cache', {
            key,
            options
        });

        return callback({
            message: 'Falta información para poder guardar el dato'
        });
    }

    if (!utils.isObject(options) || !options.storageExpireTime) {
        logger.error('[cache.save] Error de codigo: no se envio storageExpireTime', {
            key,
            options
        });

        return callback({
            message: 'Falta el tiempo de expiracion para storage'
        });
    }

    if (!utils.isObject(options) || options.localExpireTime === undefined) {
        logger.error('[cache.save] Error de codigo: no se envio localExpireTime', {
            key,
            options
        });

        return callback({
            message: 'Falta el tiempo de expiracion para cache local'
        });
    }

    // Si es necesario, convierto el dato a string
    if (options.binary) {
        data = Buffer.from(data, 'binary').toString('base64');
    }

    // Clono el objeto para no modificarlo
    let _data = {
        data: data
    };

    if (options.stringify) {
        try {
            _data = JSON.stringify(_data);
        } catch (err) {
            logger.error('[cache.save] Error al hacer JSON.stringify', {
                key,
                data: _data,
                err
            });

            return callback({
                message: 'Error al realizar JSON stringify.'
            });
        }
    }

    if (config.REDIS_DISABLED === 'true') {
        // Guarda el cache en memoria solo si lo pide
        if (options.localExpireTime > 0) {
            let cacheToSave = {
                key: key,
                data: data, // Al guardarlo guarda la version original
                expires: moment().add(options.localExpireTime, 'seconds').unix()
            };

            /* Guarda el rating en memoria por si se consulta luego */
            Cache[key] = cacheToSave;
        }

        return callback(null);
    } else {
        redisService.setData(key, _data, options.storageExpireTime, (err) => {
            if (err) {
                logger.error('[cache.save] Error al almacenar informacion en Redis', {
                    key,
                    options,
                    err
                });

                return callback(err);
            }

            // Guarda el cache en memoria solo si lo pide
            if (options.localExpireTime > 0) {
                let cacheToSave = {
                    key: key,
                    data: data, // Al guardarlo guarda la version original
                    expires: moment().add(options.localExpireTime, 'seconds').unix()
                };

                /* Guarda el rating en memoria por si se consulta luego */
                Cache[key] = cacheToSave;
            }

            return callback(null);
        });
    }
};

exports.getMaster = (key, options, fallback, callback) => {
    let cacheHit = false;

    async.waterfall([
        // Consulta cache
        (cb) => {
            exports.get(key, options, (err, result) => {
                if (err) {
                    return cb(null, null);
                }

                if (!result) {
                    return cb(null, null);
                }

                cacheHit = true;

                return cb(null, result);
            });

        },
        // Ejecuta la funcion callback
        (item, cb) => {
            if (item) {
                return cb(null, item);
            }

            fallback((err, item) => {
                if (err) {
                    return cb(err);
                }

                return cb(null, item);
            });
        },
        (item, cb) => {
            if (cacheHit || !item) {
                return cb(null, item);
            }

            exports.save(key, item, options, (err) => {
                if (err) {
                    return cb(null, item);
                }

                return cb(null, item);
            });
        }
    ], (err, item) => {
        return callback(err, item);
    });
};