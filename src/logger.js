const logger = require('@smartadtags/logger');
const config = require('config');

logger.config({
    environment: config.LOGGER_ENV,
    service: config.LOGGER_SERVICE || 'movies-webserver',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});

module.exports.logger = logger;