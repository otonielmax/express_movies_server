'use strict';
const async = require('async');
const config = require('config');
const http = require('http');
const httpProxy = require('http-proxy');
const path = require('path');

const { logger } = require('../logger');
const server = require('../server');
const getWebsitesConfig = require('../helpers/getWebsitesConfig');
const languageHelper = require('../helpers/language');
const statusHelper = require('../helpers/status');

const transactionId = 'master-server';

module.exports = () => {
    async.waterfall([
        (cb) => {
            try {
                const websites = getWebsitesConfig();
                for (let website of websites) {
                    // Acomodar los path
                    website.paths.website = path.join(__dirname, '../', website.paths.website);
                    website.paths.static = path.join(__dirname, '../', website.paths.static);
                    website.paths.partials = path.join(__dirname, '../', website.paths.partials);
                    website.paths.translations = website.paths.translations ? path.join(__dirname, '../', website.paths.translations) : null;
                    // Generales   
                    website.paths.sharedStatic = config.PATH_STATIC ? path.join(__dirname, '../', config.PATH_STATIC) : null;
                    website.paths.sharedTranslations = config.PATH_TRANSLATIONS ? path.join(__dirname, '../', config.PATH_TRANSLATIONS) : null;
                    website.paths.sharedPartials = config.PATH_PARTIALS ? path.join(__dirname, '../', config.PATH_PARTIALS) : null;
                }
                return cb(null, websites);
            } catch (err) {
                logger.error('[worker] Error al cargar configuraciones de sitios', { transactionId, err });
                return cb(err);
            }
        },
        (websites, cb) => {
            languageHelper.retrieve((err) => {
                if (err) {
                    logger.error('[worker] Error retrieving languages', {
                        transactionId,
                        err
                    });

                    return cb(err);
                }

                return cb(null, websites);
            });
        },
        // Iniciar servidores
        (websites, cb) => {
            async.mapSeries(websites, (website, cb) => {
                server(website, (err, result) => {
                    if (err) {
                        return cb(err);
                    }
                    
                    // Asignacion de puerto dinamica
                    website.port = result.port;
                    return cb();
                });
            }, (err) => cb(err, websites));
        }
    ], (err, websites) => {
        if (err) {
            return process.exit(1);
        }

        let proxy = httpProxy.createProxyServer({});
        let listener = (req, res) => {
            /* Check status */
            if (req.url === '/status') {
                return statusHelper.checkStatus(res);
            }

            let port = null;
            let host = req.headers.host ? req.headers.host.split(':')[0] : null;

            if (!host) {
                res.writeHead(404, {
                    'Content-Type': 'text/plain'
                });
                res.write('404');
                return res.end();
            }

            /* Busca entra los sitios alguno que responda al dominio del request */
            for (const website of websites) {
                for (let domain of website.domains) {
                    if (domain.indexOf('*') === -1) {
                        // Dominio comun
                        if (domain === host) {
                            port = website.port;
                            break;
                        }
                    } else {
                        // Es un wildcard
                        // Separo el dominio por el wildcard
                        let splitted = domain.split('*');
                        let lastIndex = -1;
                        let aux;
                        let valid = true;

                        // Recorro las partes del dominio.
                        // Busco que, por cada parte, la encuentre en el host y que la ubicacion sea mas adelante de la ultima coincidencia.
                        for (let part of splitted) {
                            aux = lastIndex;
                            lastIndex = host.indexOf(part);
                            if (lastIndex === -1 || aux >= lastIndex) {
                                valid = false;
                                break;
                            }
                        }

                        if (valid) {
                            port = website.port;
                            break;
                        }
                    }
                }

                // Si ya encontro, corta
                if (port) {
                    break;
                }
            }

            /* Si no hay ningun sitio que responda a ese dominio, le envia un error */
            if (!port) {
                res.writeHead(404, {
                    'Content-Type': 'text/plain'
                });
                res.write('404');
                return res.end();
            }

            proxy.web(req, res, {
                target: `http://127.0.0.1:${port}${req.url}`,
                prependPath: false,
                followRedirects: false,
                changeOrigin: false,
                autoRewrite: true,
                xfwd: true
            }, (err) => {
                logger.error('Error proxying request to child server', {
                    transactionId,
                    url: host + req.url,
                    port,
                    err
                });
            });
        };
        const masterServer = http.createServer(listener).listen(Number(config.PORT));
        masterServer.on('error', (err) => {
            // El codigo EADDRINUSE hace referencia a que el puerto seleccionado ya esta en uso
            if (err && err.code && err.code === 'EADDRINUSE') {
                logger.error(`[server] Port ${err.port} in use`, {
                    transactionId,
                    err
                });

                return process.exit(1);
            }

            // Si el error fue otro, mata el proceso
            logger.error('[worker] Critical error on proxy server', {
                transactionId,
                err
            });

            return process.exit(1);
        });

        // Graceful shutdown
        const gracefulShutdown = (signal) => {
            logger.info(`Signal received: ${signal}. Shutting down server`, { transactionId });
            masterServer.close(() => {
                logger.info('Server stopped', { transactionId });
                process.exit(0);
            });
        };
        process.on('SIGINT', gracefulShutdown);
        process.on('SIGTERM', gracefulShutdown);
    });
};