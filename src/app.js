'use strict';

const { logger } = require('./logger');
const worker = require('./cluster/worker');

const transactionId = 'app';

/**
 * En el caso de que ocurra una excepcion no controlada
 * imprime el mensaje y mata el proceso con estado 1 para
 * que se vuelva a lanzar.
 * Esto no tendria que pasar, pero pasa.. asique es solo momentaneo
 */
process.on('uncaughtException', function (err) {
    logger.error('[app] uncaughtException', {
        transactionId,
        err
    });
    
    process.exit(1);
});

logger.info('Loading configs and starting worker', { transactionId });

// Iniciar worker
worker();