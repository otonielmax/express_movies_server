'use strict';
const async = require('async');

const { logger } = require('../../src/logger');
const getWebsitesConfig = require('../../src/helpers/getWebsitesConfig');
const dbService = require('../../src/services/db');

const transactionId = 'websites-db-script';

const websites = getWebsitesConfig();


const run = (callback) => {
    // Buscar duplicados para prevenir errores en la DB
    const duplicates = websites.filter((website, index) => {
        return index !== websites.findIndex((w) => w.id === website.id)
            || index !== websites.findIndex((w) => w.name === website.name);
    });

    if (duplicates && duplicates.length) {
        logger.error('There are duplicates', { transactionId, duplicates });
        return callback(new Error('There are duplicates'));
    }

    const query = `
        INSERT INTO
            website
        SET
            ?
        ON DUPLICATE KEY UPDATE
            ?
    `;

    async.waterfall([
        (cb) => {
            dbService.getMasterConnection((err, connection) => {
                return cb(err, connection);
            });
        },
        (dbConnection, cb) => {
            dbConnection.beginTransaction((err) => {
                return cb(err, dbConnection);
            });
        },
        (dbConnection, cb) => {
            async.mapSeries(websites, (website, cb) => {
                logger.info(`Creating/updating "${website.name}"`, { transactionId });

                const updateData = {
                    name: website.name,
                    url: website.host,
                    active: !!website.active,
                    wp_active: !!website.wpActive
                };
                const insertData = {
                    ...updateData,
                    id: website.id
                };

                dbConnection.query(query, [insertData, updateData], (err) => {
                    if (err) {
                        logger.error(`Error updating site "${website.name}"`, { transactionId, err });
                        return cb(err);
                    }

                    logger.info(`Website "${website.name}" updated`, { transactionId });
                    return cb();
                });
            }, (err) => cb(err, dbConnection));
        }
    ], (err, dbConnection) => {
        if (err) {
            if (!dbConnection) {
                return callback(err);
            }

            dbConnection.rollback(() => {
                dbConnection.release();
                return callback(err);
            });
        } else {
            dbConnection.commit((err) => {
                dbConnection.release();
                return callback(err);
            });
        }
    });
};

run((err) => {
    if (err) {
        logger.error('Error executing cronjob', { transactionId, err });
        // Cuando hay falla matamos el proceso para que Buddy detecte el error
        process.exit(1);
    } else {
        logger.info('Job executed successfully', { transactionId });
        dbService.endPool();
    }
});