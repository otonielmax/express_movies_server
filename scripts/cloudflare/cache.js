'use strict';
const { logger } = require('../../src/logger');
const CloudflareApi = require('./api');
const getWebsitesConfig = require('../../src/helpers/getWebsitesConfig');

const transactionId = 'cloudflare-dns-script';

const processCache = async (website) => {
    const { name, cloudflare: { zoneId, email, apiKey } } = website;
    
    logger.info(`Processing website "${name}"`, { transactionId });

    const cloudflareApi = new CloudflareApi(email, apiKey);

    logger.info('Cleaning cache', { transactionId });

    try {
        await cloudflareApi.clearCache(zoneId, { purge_everything: true });
    } catch (err) {
        logger.error(`Error cleaning cache website "${name}"`, { transactionId, err });
        throw err;
    }
};

const processWebsites = async () => {
    try {
        const websites = getWebsitesConfig();
        for (const website of websites) {
            if (website.cloudflare && website.cloudflare.email) {
                await processCache(website);
            } else {
                logger.warn(`Skipping website "${website.name}", no CF config found`, { transactionId });
            }
        }
    } catch (err) {
        logger.error('Error processing websites', { transactionId, err });
        process.exit(1);
    }
};

processWebsites();