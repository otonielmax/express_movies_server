'use strict';

const request = require('request-promise');

class CloudflareApi {
    constructor (email, apiKey) {
        this.email = email;
        this.apiKey = apiKey;
    }

    async listDns (zoneId, qs) {
        return this._request(`/zones/${zoneId}/dns_records`, 'GET', null, qs);
    }

    async updateDns (zoneId, dnsRecordId, body) {
        return this._request(`/zones/${zoneId}/dns_records/${dnsRecordId}`, 'PUT', body);
    }

    async createDns (zoneId, body) {
        return this._request(`/zones/${zoneId}/dns_records`, 'POST', body);
    }

    async listPageRule (zoneId, qs) {
        return this._request(`/zones/${zoneId}/pagerules`, 'GET', null, qs);
    }

    async updatePageRule (zoneId, pageRuleId, body) {
        return this._request(`/zones/${zoneId}/pagerules/${pageRuleId}`, 'PUT', body);
    }

    async createPageRule (zoneId, body) {
        return this._request(`/zones/${zoneId}/pagerules`, 'POST', body);
    }

    async deletePageRule (zoneId, pageRuleId) {
        return this._request(`/zones/${zoneId}/pagerules/${pageRuleId}`, 'DELETE');
    }

    async clearCache (zoneId, body) {
        return this._request(`/zones/${zoneId}/purge_cache`, 'POST', body);
    }

    async updateSetting (zoneId, setting, body) {
        return this._request(`/zones/${zoneId}/settings/${setting}`, 'PATCH', body);
    }

    async _request (path, method, body, qs) {
        const options = {
            url: `https://api.cloudflare.com/client/v4${path}`,
            method,
            qs,
            json: body || true,
            headers: this._getHeaders()
        };
        return request(options);
    }

    _getHeaders () {
        return {
            'X-Auth-Key': this.apiKey,
            'X-Auth-Email': this.email
        };
    }
}

module.exports = CloudflareApi;