'use strict';
const parseDomain = require('parse-domain');

const { logger } = require('../../src/logger');
const CloudflareApi = require('./api');
const getWebsitesConfig = require('../../src/helpers/getWebsitesConfig');

const transactionId = 'cloudflare-dns-script';

const processDns = async (website) => {
    const { name, cloudflare: { zoneId, email, apiKey, dnsRecords } } = website;
    
    logger.info(`Processing website "${name}"`, { transactionId });

    const cloudflareApi = new CloudflareApi(email, apiKey);

    logger.info('Retrieving DNS resource list', { transactionId });

    // Obtener listado actual de registros DNS
    const dnsResponse = await cloudflareApi.listDns(zoneId, { per_page: 100 });
    const dnsList = dnsResponse.result;

    logger.info('DNS Resource list retrieved', { transactionId, dnsList });

    // Mapear los que necesitamos actualizar o crear
    const mappedDns = dnsRecords.map((currentDns) => {
        const foundDns = dnsList.find((d) => d.name === currentDns.name);
        if (foundDns) {
            currentDns.id = foundDns.id;
        }
        return currentDns;
    });

    // Crear/Actualizar registros DNS
    for (const dns of mappedDns) {
        const { id, type, name, content, proxied } = dns;

        logger.info(`Processing "${name}" record`, { transactionId, dns });

        const request = {
            type,
            name,
            content,
            proxied
        };

        try {
            if (id) {
                // Actualizarlo si ya existe
                logger.info(`Updating "${name}" record`, { transactionId, request });
                await cloudflareApi.updateDns(zoneId, id, request);
            } else {
                // Crear uno nuevo ya que no tiene ID
                logger.info(`Creating "${name}" record`, { transactionId, request });
                await cloudflareApi.createDns(zoneId, request);
            }
        } catch (err) {
            logger.error(`Error processing "${name}" record`, { transactionId, err });
            throw err;
        }
    }

    // Setear configuracion de SSL
    const sslMode = 'flexible';
    logger.info(`Setting SSL Mode to "${sslMode}"`, { transactionId });
    await cloudflareApi.updateSetting(zoneId, 'ssl', { value: sslMode });
    // Setear Always Use SSL
    const alwaysUseHttpsMode = 'on';
    logger.info(`Setting "Always Use HTTPS" to "${alwaysUseHttpsMode}"`, { transactionId });
    await cloudflareApi.updateSetting(zoneId, 'always_use_https', { value: alwaysUseHttpsMode });
    // Setear minificado automatico
    logger.info('Enabling files minification', { transactionId });
    await cloudflareApi.updateSetting(zoneId, 'minify', {
        value: {
            js: 'on',
            css: 'on',
            html: 'on'
        }
    });

    // Controlar page rules
    const pageRules = [];
    
    // Agregar redireccion de root a subdominio principal si es necesario
    const url = parseDomain(website.host);
    if (url.domain) {
        logger.info(`El sitio tiene un subdominio en prod: "${url.subdomain}". Agregando redireccion`, { transactionId });
        pageRules.push({
            targets: [{
                target: 'url',
                constraint: {
                    operator: 'matches',
                    value: `https://${url.domain}.${url.tld}/*` // redirige todo lo que no tiene el subdominio (por lo general www)
                }
            }],
            actions: [{
                id: 'forwarding_url',
                value: {
                    url: website.host + '/$1',
                    status_code: 301
                }
            }],
            status: 'active'
        });

        logger.info('Eliminando Page Rule archivos .jpg', { transactionId });
        pageRules.push({
            delete: true,
            targets: [{
                target: 'url',
                constraint: {
                    operator: 'matches',
                    value: `https://*.${url.domain}.${url.tld}/*.jpg` // todos los .jpg de todos los subdominios
                }
            }],
            actions: [{
                id: 'browser_cache_ttl', // 1 mes de cache en los clientes
                value: 2678400
            }, {
                id: 'edge_cache_ttl', // 1 semana de cache en el CDN
                value: 604800
            }],
            status: 'active'
        });

        logger.info('Agregando Page Rule Cache Origin-Control', { transactionId });
        pageRules.push({
            targets: [{
                target: 'url',
                constraint: {
                    operator: 'matches',
                    value: `https://*.${url.domain}.${url.tld}/*` // el cache lo maneja el webserver en todo el sitio
                }
            }],
            actions: [{
                id: 'explicit_cache_control',
                value: 'on'
            }],
            status: 'active'
        });
    }

    if (pageRules.length) {
        for (const pageRule of pageRules) {
            // Buscar si el page rule ya existe
            const pageRulesResponse = await cloudflareApi.listPageRule(zoneId);
            const currentPageRules = pageRulesResponse.result;
            const found = currentPageRules.find((pr) => {
                return JSON.stringify(pageRule.targets) === JSON.stringify(pr.targets);
            });
            
            // Eliminar Page Rules que ya no se usan
            if (pageRule.delete && found) {
                logger.info('Deleting page rule', {  transactionId, found });
                await cloudflareApi.deletePageRule(zoneId, found.id);
            } else if (!pageRule.delete && found) {
                logger.info('Updating page rule', { transactionId, pageRule });
                await cloudflareApi.updatePageRule(zoneId, found.id, pageRule);
            } else if (!pageRule.delete && !found) {
                logger.info('Creating page rule', { transactionId });
                await cloudflareApi.createPageRule(zoneId, pageRule);
            }
        }
    }
};

const processWebsites = async () => {
    try {
        const websites = getWebsitesConfig();
        for (const website of websites) {
            if (website.cloudflare && website.cloudflare.email) {
                await processDns(website);
            } else {
                logger.warn(`Skipping website "${website.name}", no CF config found`, { transactionId });
            }
        }
    } catch (err) {
        logger.error('Error processing websites', { transactionId, err });
        process.exit(1);
    }
};

processWebsites();